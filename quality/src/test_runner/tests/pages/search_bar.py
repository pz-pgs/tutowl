"""
Module represents header SearchBar
"""


class SearchBar:

    SEARCH_BAR_INPUT = '#search-bar-input'
    FOUND_ROOM_XPATH = "//*[@id='root']/div/div/div[2]/div[1]/div/ul/li[3]/ul/a/li/div[2]/p[1]"

    def __init__(self, browser):
        self.browser = browser
    
    def search_bar_input(self):
        return self.browser.wait().for_clickable_element_by_css_selector(self.SEARCH_BAR_INPUT)

    def search_with_query(self, query):
        search_bar_element = self.search_bar_input()

        search_bar_element.click()
        search_bar_element.send_keys(query)

    def found_room_result(self):
        return self.browser.wait().for_clickable_element_by_xpath(self.FOUND_ROOM_XPATH)