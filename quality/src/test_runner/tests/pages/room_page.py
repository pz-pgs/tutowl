"""
Module represents room page
"""


class RoomPage:

    ROOM_NAME_ELEMENT_XPATH = "//*[@id='root']/div/div/div[4]/div/div[1]/div[1]/div[1]"

    def __init__(self, browser):
        self.browser = browser
    
    def verify_room_name(self):
        return self.browser.wait().for_visibility_of_element_by_xpath(self.ROOM_NAME_ELEMENT_XPATH)
        