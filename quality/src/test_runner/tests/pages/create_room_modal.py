"""
Module represents create room modal
"""


class CreateRoomModal:

    CREATE_ROOM_MODAL_ELEMENTS = ['.create-room-modal', '.modal-title']
    ROOM_NAME_INPUT = 'roomName'
    ROOM_DESCRIPTION_INPUT = '.text-area-component'
    CREATE_ROOM_BUTTON = '.send-button'
    SUCCESS_ICON_CSS = '.success-checkmark'
    CLOSE_ICON_CSS = '.close-badge-button'

    def __init__(self, browser):
        self.browser = browser

    def is_page_visible(self):
        for selector in self.CREATE_ROOM_MODAL_ELEMENTS:
            return self.browser.wait().for_visibility_of_element_by_css_selector(selector)

    def room_name_field(self):
        return self.browser.wait().for_clickable_element_by_id(self.ROOM_NAME_INPUT)

    def room_description_field(self):
        return self.browser.wait().for_clickable_element_by_css_selector(self.ROOM_DESCRIPTION_INPUT)

    def create_room_button(self):
        return self.browser.wait().for_clickable_element_by_css_selector(self.CREATE_ROOM_BUTTON)

    def wait_for_create_success_checkmark(self):
        return self.browser.wait().for_visibility_of_element_by_css_selector(self.SUCCESS_ICON_CSS)

    def close_icon(self):
        return self.browser.wait().for_visibility_of_element_by_css_selector(self.CLOSE_ICON_CSS)

    def create_new_room(self, room_name, room_desc):
        room_name_element = self.room_name_field()
        room_desc_element = self.room_description_field()
        
        room_name_element.click()
        room_name_element.send_keys(room_name)
        room_desc_element.click()
        room_desc_element.send_keys(room_desc)
        self.create_room_button().click()

