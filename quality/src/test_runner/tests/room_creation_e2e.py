"""
Tests for room creation
"""

from runner.testcase import TestCase
from tests.pages.login import Login
from tests.pages.register import Register
from tests.pages.main_page import MainPage
from tests.pages.create_room_modal import CreateRoomModal
from tests.pages.search_bar import SearchBar
from tests.pages.room_page import RoomPage
from tests.utils.string_utils import StringUtils


class RoomCreation(TestCase):

    ROOM_NAME = 'example_room_name_{}'.format(StringUtils.randomString())
    ROOM_NAME_WITH_PREFIX = 'Room: ' + ROOM_NAME
    ROOM_DESCRPTION = 'example_room_desc'

    @classmethod
    def set_up_class(cls):
        cls.login = Login(cls.browser)
        cls.main_page = MainPage(cls.browser)
        cls.create_room_modal = CreateRoomModal(cls.browser)
        cls.search_bar = SearchBar(cls.browser)
        cls.room_page = RoomPage(cls.browser)

    def test_should_create_new_room_and_check_if_visible_on_classes_page(self):
        self.login.is_page_visible()
        self.login.login_to_app()
        self.main_page.is_page_visible()
        self.main_page.create_room_button().click()
        self.create_room_modal.is_page_visible()
        self.create_room_modal.create_new_room(self.ROOM_NAME, self.ROOM_DESCRPTION)
        self.create_room_modal.wait_for_create_success_checkmark()
        self.create_room_modal.close_icon().click()
        self.main_page.is_page_visible()
        self.search_bar.search_with_query(self.ROOM_NAME)
        self.search_bar.found_room_result().click()
        self.assertEquals(self.room_page.verify_room_name().text, self.ROOM_NAME_WITH_PREFIX)
        
