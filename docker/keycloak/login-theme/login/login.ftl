<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo; section>
    <#if section = "title">
        ${msg("loginTitle",(realm.displayName!''))}
    <#elseif section = "header">
        ${msg("loginTitleHtml",(realm.displayNameHtml!''))?no_esc}
    <#elseif section = "form">
        <h3 class="sub-login-text">Login with your data, that your entered during your registration.</h3>
        <#if realm.password>
            <form id="kc-form-login" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">
                <div class="${properties.kcFormGroupClass!}">
                    <div class="${properties.kcLabelWrapperClass!}">
                        <label for="username" class="${properties.kcLabelClass!}"><#if !realm.loginWithEmailAllowed>${msg("username")}<#elseif !realm.registrationEmailAsUsername>${msg("usernameOrEmail")}<#else>${msg("email")}</#if></label>
                    </div>

                    <div class="${properties.kcInputWrapperClass!}">
                        <#if usernameEditDisabled??>
                            <input id="username" class="${properties.kcInputClass!} username-login" name="username" value="${(login.username!'')}" type="text" disabled />
                        <#else>
                            <input id="username" class="${properties.kcInputClass!} username-login" name="username" value="${(login.username!'')}" type="text" autofocus autocomplete="off" />
                        </#if>
                    </div>
                </div>

                <div class="${properties.kcFormGroupClass!}">
                    <div class="${properties.kcLabelWrapperClass!}">
                        <label for="password" class="${properties.kcLabelClass!}">${msg("password")}</label>
                    </div>

                    <div class="${properties.kcInputWrapperClass!}">
                        <input id="password" class="${properties.kcInputClass!} password-login" name="password" type="password" autocomplete="off" />
                    </div>
                </div>


                    <div id="kc-form-options" class="${properties.kcFormOptionsClass!} remember-me-block">
                        <#if realm.rememberMe && !usernameEditDisabled??>
                            <div class="checkbox ${properties.kcFormGroupClass}">
                                <div class="multiple-choice">
                                    <#if login.rememberMe??>
                                        <input id="rememberMe" name="rememberMe" type="checkbox" tabindex="3" checked>
                                        <label for="rememberMe" class="${properties.kcCheckboxLabelClass} remember-me-label">${msg("rememberMe")}</label>
                                    <#else>
                                        <input id="rememberMe" name="rememberMe" type="checkbox" tabindex="3">
                                        <label for="rememberMe" class="${properties.kcCheckboxLabelClass} remember-me-label">${msg("rememberMe")}</label>
                                    </#if>
                                </div>
                            </div>
                        </#if>
                    </div>

                    <div id="forgot-block" class="${properties.kcFormOptionsWrapperClass!} ${properties.kcFormGroupClass}">
                            <#if realm.resetPasswordAllowed>
                                <p id="forgot-text"><a id="forgot-link" href="${url.loginResetCredentialsUrl}">${msg("doForgotPassword")}</a></p>
                            </#if>
                    </div>

                    <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!} ${properties.kcFormGroupClass}">
                        <div class="${properties.kcFormButtonsWrapperClass!}">
                            <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="login" id="kc-login" class="login-button" type="submit" value="${msg("doLogIn")}"/>
                        </div>
                    </div>

                    <#if realm.password && realm.registrationAllowed && !usernameEditDisabled??>
                        <div id="kc-registration">
                            <p id="register-text"> Don't have an account ? </p>
                            <a id="register-button" href="${url.registrationUrl}">${msg("registerLink")}</a>
                        </div>
                    </#if>
            </form>
        </#if>
    <#elseif section = "info" >
        <#if realm.password && social.providers??>
            <#-- This section of the theme has not yet been well styled. Non-trivial user research, interaction design and content design work is required to develop a solution for login using 3rd-party identity providers. -->
            <div id="kc-social-providers">
                <h2 class="heading-medium">${msg("socialProviders")}</h2>
                <ul class="list">
                    <#list social.providers as p>
                        <li><a href="${p.loginUrl}" id="zocial-${p.alias}" class="button zocial ${p.providerId}">${p.displayName}</a></li>
                    </#list>
                </ul>
            </div>
        </#if>
    </#if>
</@layout.registrationLayout>
