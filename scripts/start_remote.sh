#!/bin/bash

set -e

SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function app_start() (
    echo "Pulling newest backend image..."

    cd $SOURCE_DIR/../

    docker pull registry.gitlab.com/pz-pgs/backend:latest

    echo "Starting backend..."

    docker-compose -f docker/docker-compose.yml up -d

    echo "Backend started."
)

app_start
