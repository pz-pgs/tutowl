#!/bin/bash

set -e

SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function app_start() (
    echo "Building local docker image..."

    cd $SOURCE_DIR/../application/backend/

    ./gradlew jibDockerBuild

    echo "Docker image successfully built."

    cd ../../

    echo "Starting Tutowl..."

    docker-compose -f docker/docker-compose.yml up

    echo "Tutowl started."
)

app_start