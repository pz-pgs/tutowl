import express from 'express';
import morgan from 'morgan';
import path from 'path';
import https from 'https';
import fs from 'fs';
import compression from 'compression';

const app = express();

const PORT = process.env.PORT || 8081;
const PUBLIC_PATH = process.env.PUBLIC_PATH || '/';

const HTTPS_OPTIONS = {
  key: fs.readFileSync(path.resolve(__dirname, 'keys/key.pem'), 'utf8'),
  cert: fs.readFileSync(path.resolve(__dirname, 'keys/cert.pem'), 'utf8'),
};

app.use(morgan('combined'));
app.use(compression());
app.use(PUBLIC_PATH, express.static(path.resolve(__dirname, 'dist')));

app.get('/*', (request, response) => {
  response.sendFile(path.resolve(__dirname, 'dist', 'index.html'));
});

https
  .createServer(HTTPS_OPTIONS, app)
  .listen(PORT, () => console.log(`Application listening on port ${PORT}`));
