import React, { Component } from 'react';
import styled from 'styled-components';
import Colors from 'common/colors';
import userIcon from 'images/icons/user_full.svg';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { removeRoom } from 'room-page/actions/roomDetailsActions';
import DeleteRoomDialog from 'room-page/components/DeleteRoomDialog';

const StreamBoxTopbarWrapper = styled.div.attrs({ className: 'stream-box-topbar' })`
  display: grid;
  grid-template-columns: 1fr 3fr;
  grid-template-areas:
    'bar-left-area'
    'bar-right-area';

  height: 100%;
  background: ${Colors.WHITE};
  padding: 20px;
  border-bottom: 1px solid ${Colors.GALLERY};
  height: 14px;
  line-height: 14px;
`;

const TopBarLeft = styled.div.attrs({ className: 'top-bar-left' })`
  grid: bar-left-area;
  font-size: 14px;
  font-weight: bold;
  font-family: 'Titillium Web', sans-serif;
`;

const TopBarRight = styled.div.attrs({ className: 'top-bar-right' })`
  grid: bar-right-area;
  text-align: right;
`;

const DeleteRoomButton = styled.button.attrs({ className: 'delete-room-button' })`
  cursor: pointer;
  transition: 0.2s;
  padding: 10px 20px;
  color: #552526;
  border: 1px solid #f0f0f0;
  border-radius: 4px;
  background: #fce7e6;
  font-weight: bold;
  margin: -10px 20px 0 0;

  &:hover {
    opacity: 0.7;
  }

  @media only screen and (max-width: 880px) {
    display: none;
  }
`;

const BoldText = styled.span.attrs({ className: 'bold-text' })`
  font-weight: bold;
  font-family: 'Sen', sans-serif;
`;

const ActiveUsersCount = styled.p.attrs({ className: 'active-users-count' })`
  font-size: 16px;
  margin: 0 25px 0 0;
  display: inline-block;
  font-family: 'Titillium Web', sans-serif;
  color: #880719;
`;

const ActiveUsersIcon = styled.img.attrs({
  className: 'active-users-icon',
  alt: 'active-users-icon',
})`
  margin: 0;
  display: inline-block;
  width: 14px;
  height: 14px;
  margin-right: 5px;
  filter: invert(27%) sepia(51%) saturate(2878%) hue-rotate(346deg) brightness(104%) contrast(97%);
`;

const LiveCount = styled.span.attrs({ className: 'live-count' })`
  background: #fff;
  padding: 5px 10px;
  text-align: center;
  font-size: 12px;
  border: 1px solid ${Colors.SCIENCE_BLUE};
  border-top-left-radius: 3px;
  border-bottom-left-radius: 3px;
`;

const LiveName = styled.span.attrs({ className: 'live-count' })`
  background: #2d4564;
  padding: 5px 15px;
  text-transform: uppercase;
  text-align: center;
  font-size: 12px;
  border: 1px solid #2d4564;
  color: #fff;
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
`;

class StreamBoxTopbar extends Component {
  state = {
    showDeleteDialog: false,
  };

  onDeleteClick = () => {
    this.setState({
      showDeleteDialog: !this.state.showDeleteDialog,
    });
  };

  showDeleteButton = () => this.props.username === this.props.owner;

  render() {
    const { roomName, removeRoomFunc, roomId } = this.props;
    const { showDeleteDialog } = this.state;
    return (
      <StreamBoxTopbarWrapper>
        <TopBarLeft>
          Room: <BoldText>{roomName}</BoldText>
        </TopBarLeft>
        <TopBarRight>
          {this.showDeleteButton() ? (
            <DeleteRoomButton onClick={this.onDeleteClick}>Delete room</DeleteRoomButton>
          ) : null}
          <ActiveUsersCount>
            <ActiveUsersIcon src={userIcon} />
            23
          </ActiveUsersCount>
          <LiveCount>00:18:29</LiveCount>
          <LiveName>LIVE</LiveName>
        </TopBarRight>
        {showDeleteDialog && (
          <DeleteRoomDialog
            onClose={this.onDeleteClick}
            onDelete={removeRoomFunc}
            roomId={roomId}
            roomName={roomName}
          />
        )}
      </StreamBoxTopbarWrapper>
    );
  }
}

StreamBoxTopbar.propTypes = {
  roomName: PropTypes.string.isRequired,
  roomId: PropTypes.string.isRequired,
  owner: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  removeRoomFunc: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  roomName: state.session.roomDetails.roomData.name,
  roomId: state.session.roomDetails.roomId,
  owner: state.session.roomDetails.roomData.owner,
  username: state.common.authUser.keycloakInfo.userInfo.preferred_username,
});

const mapDispatchToProps = dispatch => ({
  removeRoomFunc: roomId => dispatch(removeRoom(roomId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(StreamBoxTopbar);
