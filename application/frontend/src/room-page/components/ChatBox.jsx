import React from 'react';
import styled from 'styled-components';
import Colors from 'common/colors';
import PropTypes from 'prop-types';
import ChatInput from 'room-page/components/ChatInput';
import ChatPosts from 'room-page/components/ChatPosts';

const ChatBoxWrapper = styled.div.attrs({ className: 'chat-box-wrapper' })`
  background: ${Colors.BACKGROUND_COLOR};
  border-left: 3px solid ${Colors.GALLERY};
  position: relative;
`;

const ChatBox = ({ messages, sendChatMessage, isSession }) => (
  <ChatBoxWrapper>
    <ChatPosts messages={messages} />
    <ChatInput onMessageSend={sendChatMessage} isSession={isSession} />
  </ChatBoxWrapper>
);

ChatBox.propTypes = {
  messages: PropTypes.instanceOf(Array).isRequired,
  sendChatMessage: PropTypes.func.isRequired,
  isSession: PropTypes.bool.isRequired,
};

export default ChatBox;
