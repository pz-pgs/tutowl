import React, { Component } from 'react';
import GenericModal from 'common/components/GenericModal';
import { connect } from 'react-redux';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import warningImage from 'images/warning.svg';
import ButtonProgressIndicator from 'common/components/ButtonProgressIndicator';
import { redirectToMainPage } from 'room-page/actions/roomDetailsActions';

const StyledWrapper = styled.div.attrs({ className: 'styled-wrapper' })`
  font-size: 12px;
  font-weight: 100;
  padding: 10px;
`;

const RemoveErrorBlock = styled.div.attrs({ className: 'remove-error-block' })`
  padding: 10px;
  margin: 5px 0;
  text-align: center;
  background: #fce7e6;
  font-size: 12px;
  color: #552526;
`;

const BoldText = styled.span.attrs({ className: 'bold-text' })`
  font-weight: bold;
`;

const WarningSign = styled.img.attrs({ className: 'warning-sign', alt: 'warning-sign' })`
  display: block;
  margin: 0 auto;
  height: 50px;
`;

const WarningText = styled.p.attrs({ className: 'warning-text' })``;

const ButtonsWrapper = styled.div.attrs({ className: 'buttons-wrapper' })`
  margin-top: 30px;
`;

const DeleteButton = styled.button.attrs({ className: 'delete-button' })`
  cursor: pointer;
  transition: 0.2s;
  padding: 10px 20px;
  color: #552526;
  border: 1px solid #f0f0f0;
  border-radius: 4px;
  background: #fce7e6;
  font-weight: bold;
  float: right;
  width: 48%;

  &:hover {
    opacity: 0.7;
  }
`;

const CancelButton = styled.button.attrs({ className: 'cancel-button' })`
  cursor: pointer;
  transition: 0.2s;
  padding: 10px 20px;
  color: #000000;
  border: 1px solid #f0f0f0;
  border-radius: 4px;
  background: #f0f0f0;
  font-weight: bold;
  display: inline-block;
  width: 48%;

  &:hover {
    opacity: 0.7;
  }
`;

const RemoveRoomSuccessBox = styled.div.attrs({ className: 'remove-room-success-box' })`
  padding: 25px 25px 15px 25px;
`;

const RemoveSuccessTitle = styled.p.attrs({ className: 'remove-success-title' })`
  display: block;
  margin: 0 0 30px 0;
  font-size: 15px;
  text-align: center;
  line-height: 20px;
  font-family: 'Roboto', sans-serif !important;
  font-weight: 100;
`;

class DeleteRoomDialog extends Component {
  onDeleteButton = () => {
    this.props.onDelete(this.props.roomId);
  };

  onCloseMethod = () => {
    if (this.props.isRemoveSuccess) {
      this.props.redirectToMainPageFunc();
    } else {
      this.props.onClose();
    }
  };

  render() {
    const { onClose, roomName, isRemoveLoading, isRemoveError, isRemoveSuccess } = this.props;
    return (
      <GenericModal onClose={this.onCloseMethod}>
        {isRemoveSuccess ? (
          <RemoveRoomSuccessBox>
            <RemoveSuccessTitle>Room removed succesfully!</RemoveSuccessTitle>
            <div className="success-checkmark">
              <div
                style={
                  !isRemoveSuccess
                    ? {
                        display: 'none',
                      }
                    : {}
                }
                className="check-icon"
              >
                <span className="icon-line line-tip" />
                <span className="icon-line line-long" />
                <div className="icon-circle" />
                <div className="icon-fix" />
              </div>
            </div>
          </RemoveRoomSuccessBox>
        ) : (
          <StyledWrapper>
            {isRemoveError ? (
              <RemoveErrorBlock>An error occurred while removing.</RemoveErrorBlock>
            ) : null}
            <WarningSign src={warningImage} />
            <WarningText>
              Are you sure you want to delete room named <BoldText>{roomName}</BoldText>?
            </WarningText>
            <ButtonsWrapper>
              <CancelButton onClick={onClose}>Cancel</CancelButton>
              <DeleteButton onClick={this.onDeleteButton}>
                {isRemoveLoading ? <ButtonProgressIndicator /> : 'Delete'}
              </DeleteButton>
            </ButtonsWrapper>
          </StyledWrapper>
        )}
      </GenericModal>
    );
  }
}

DeleteRoomDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  roomName: PropTypes.string.isRequired,
  roomId: PropTypes.string.isRequired,
  isRemoveLoading: PropTypes.bool.isRequired,
  isRemoveError: PropTypes.bool.isRequired,
  isRemoveSuccess: PropTypes.bool.isRequired,
  redirectToMainPageFunc: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isRemoveLoading: state.session.roomDetails.isRemoveLoading,
  isRemoveError: state.session.roomDetails.isRemoveError,
  isRemoveSuccess: state.session.roomDetails.isRemoveSuccess,
});

const mapDispatchToProps = dispatch => ({
  redirectToMainPageFunc: () => dispatch(redirectToMainPage()),
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleteRoomDialog);
