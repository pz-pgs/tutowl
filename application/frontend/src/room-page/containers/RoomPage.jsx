import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import shortid from 'shortid';
import Colors from 'common/colors';
import ChatBox from 'room-page/components/ChatBox';
import { connect } from 'react-redux';
import axios from 'axios';
import { OpenVidu } from 'openvidu-browser';
import ProtectedRoom from 'room-page/containers/ProtectedRoom';
import { getRoomDetails } from 'room-page/actions/roomDetailsActions';
import NoRoomFoundPlaceholder from 'common/components/NoRoomFoundPlaceholder';
import withLoading, { ProgIndSize } from 'common/utils/withLoading';
import StreamBox from 'room-page/components/StreamBox';

const RoomPageWrapper = styled.div.attrs({ className: 'room-page-wrapper' })`
  display: grid;
  grid-template-columns: 3fr 1fr;
  grid-template-areas: 'live-box-area chat-box-area';
  height: 100%;
`;

const RoomPageWrapperWithLoading = withLoading(RoomPageWrapper, ProgIndSize.XX_LARGE);

const JSON_CONTENT_TYPE = 'application/json';
const SPEECH_DETECTION_VALUE = 10;
const SPEAKING_THRESHOLD_VALUE = 50;
const RESOLUTION = '950x484';
const PUBLISHER_ROLE = 'PUBLISHER';

class RoomPage extends Component {
  state = {
    initialVideo: true,
    initialAudio: true,
    session: undefined,
    mainStreamManager: undefined,
    publisher: undefined,
    subscribers: [],
    publishVideo: true,
    publishAudio: true,
    fullScreenMode: false,
    isSpeaking: false,
    isSharingScreen: false,
    sessionStarted: false,
    roomId: '',
    messages: [],
  };

  componentWillMount() {
    const {
      match: {
        params: { roomId },
      },
    } = this.props;

    this.setState({ roomId });

    this.props.getRoomDetailsFunc(roomId);
  }

  componentDidMount() {
    document.addEventListener('fullscreenchange', this.exitHandler);
    document.addEventListener('webkitfullscreenchange', this.exitHandler);
    document.addEventListener('mozfullscreenchange', this.exitHandler);
    document.addEventListener('MSFullscreenChange', this.exitHandler);
  }

  componentWillUnmount() {
    if (this.state.session) this.removeUserFromSession();
    this.leaveSession();
  }

  onCheckboxChange = event => {
    const value = event.target.checked;

    this.setState({
      [event.target.name]: value,
    });
  };

  getToken = sessionRequest => {
    const headers = {
      'Content-Type': JSON_CONTENT_TYPE,
      'X-Requested-With': 'XMLHttpRequest',
      Authorization: `Bearer ${this.props.token}`,
    };

    return axios.post(`/api/session/join`, sessionRequest, {
      headers,
    });
  };

  getVideoClassName = () => (this.state.fullScreenMode ? 'full-sc-video' : 'XD');

  handleMainVideoStream = stream => {
    if (this.state.mainStreamManager !== stream) {
      this.setState({
        mainStreamManager: stream,
      });
    }
  };

  deleteSubscriber = streamManager => {
    const { subscribers } = this.state;
    const index = subscribers.indexOf(streamManager, 0);
    if (index > -1) {
      subscribers.splice(index, 1);
      this.setState({
        subscribers,
      });
    }
  };

  isSpeaking = event =>
    Math.abs(event.value.newValue - event.value.oldValue) > SPEECH_DETECTION_VALUE &&
    (Math.abs(event.value.oldValue < SPEAKING_THRESHOLD_VALUE) ||
      Math.abs(event.value.newValue < SPEAKING_THRESHOLD_VALUE)) &&
    this.state.publishAudio;

  joinSession = () => {
    this.OV = new OpenVidu();
    this.OV.setAdvancedConfiguration({
      iceServers: [
        { urls: 'stun:www.keycloak.test:3478' },
        { urls: ['turn:www.keycloak.test:3478', 'turn:www.keycloak.test:3478?transport=tcp'] },
      ],
    });

    this.setState(
      {
        session: this.OV.initSession(),
        publishVideo: this.state.initialVideo,
        publishAudio: this.state.initialAudio,
      },
      () => {
        const mySession = this.state.session;

        mySession.on('streamCreated', event => {
          const { subscribers } = this.state;
          const subscriber = mySession.subscribe(event.stream, undefined);
          subscribers.push(subscriber);

          this.setState({
            subscribers,
          });
        });

        mySession.on('streamDestroyed', event => {
          this.deleteSubscriber(event.stream.streamManager);
        });

        mySession.on('signal:my-chat', event => {
          const message = event.data.split(';');
          const messageObj = {
            id: shortid.generate(),
            message: message[0],
            initials: message[1].charAt(0).toUpperCase(),
            username: message[1],
            iconColor: message[1] === this.props.username ? Colors.SHIP_COVE : Colors.BLUMINE,
            userMessage: message[1] === this.props.username,
          };

          this.setState({
            messages: [...this.state.messages, messageObj],
          });
        });

        const role = PUBLISHER_ROLE;
        const sessionRequest = {
          sessionName: this.state.roomId,
          username: this.props.username,
          role,
        };

        this.getToken(sessionRequest).then(token => {
          const tokenString = token.data[0];

          this.setState({
            token: tokenString,
          });

          mySession
            .connect(tokenString, { clientData: this.props.username })
            .then(() => {
              const publisher = this.OV.initPublisher(undefined, {
                audioSource: undefined, // The source of audio. If undefined default microphone
                videoSource: undefined, // The source of video. If undefined default webcam
                publishAudio: this.state.initialAudio,
                publishVideo: this.state.initialVideo,
                resolution: RESOLUTION,
                frameRate: 30,
                insertMode: 'APPEND',
                mirror: true,
              });

              publisher.on('streamAudioVolumeChange', event => {
                this.setState(
                  {
                    isSpeaking: this.isSpeaking(event),
                  },
                  () => {
                    if (this.isSpeaking) {
                      this.setState({
                        mainStreamManager: publisher,
                      });
                    }
                  },
                );
              });

              mySession.publish(publisher);

              this.setState({
                mainStreamManager: publisher,
                publisher,
                sessionStarted: true,
              });
            })
            .catch(error => {
              throw new Error(
                `There was an error connecting to the session: ${error.code}, ${error.message}.`,
              );
            });
        });
      },
    );
  };

  leaveSession = () => {
    const mySession = this.state.session;

    if (mySession) mySession.disconnect();

    this.OV = null;
    this.setState({
      session: undefined,
      subscribers: [],
      mainStreamManager: undefined,
      publisher: undefined,
    });
  };

  removeUserFromSession = () => {
    const leaveSessionRequest = {
      sessionName: this.state.roomId,
      token: this.state.token,
    };

    const headers = {
      'Content-Type': JSON_CONTENT_TYPE,
      'X-Requested-With': 'XMLHttpRequest',
      Authorization: `Bearer ${this.props.token}`,
    };

    return axios.post(`/api/session/leave`, leaveSessionRequest, {
      headers,
    });
  };

  showVideo = () => {
    const { publisher } = this.state;

    this.setState(
      {
        publishVideo: !this.state.publishVideo,
      },
      () => {
        publisher.publishVideo(this.state.publishVideo);

        this.setState({
          publisher,
        });
      },
    );
  };

  muteAudio = () => {
    const { publisher } = this.state;

    this.setState(
      {
        publishAudio: !this.state.publishAudio,
      },
      () => {
        publisher.publishAudio(this.state.publishAudio);

        this.setState({
          publisher,
        });
      },
    );
  };

  shareScreen = () => {
    const videoSource = navigator.userAgent.indexOf('Firefox') !== -1 ? 'window' : 'screen';

    this.OV.getUserMedia({
      videoSource,
      publishAudio: this.state.publishAudio,
      publishVideo: true,
      resolution: RESOLUTION,
      mirror: true,
    }).then(mediaStream => {
      const videoTrack = mediaStream.getVideoTracks()[0];

      const { publisher } = this.state;

      publisher.replaceTrack(videoTrack);

      this.setState({
        publisher,
        isSharingScreen: true,
      });
    });
  };

  stopSharingScreen = () => {
    this.OV.getUserMedia({
      videoSource: undefined,
      publishAudio: this.state.publishAudio,
      publishVideo: this.state.publishVideo,
      resolution: RESOLUTION,
      mirror: true,
    }).then(mediaStream => {
      const videoTrack = mediaStream.getVideoTracks()[0];

      const { publisher } = this.state;

      publisher.replaceTrack(videoTrack);

      this.setState({
        publisher,
        isSharingScreen: false,
      });
    });
  };

  sendChatMessage = message => {
    const mySession = this.state.session;

    mySession.signal({
      data: `${message};${this.props.username}`,
      to: [], // Array of Connection objects (optional. Broadcast to everyone if empty)
      type: 'my-chat', // The type of message (optional)
    });
  };

  goFullScreen = () => {
    const { document } = window;
    const fs = document.getElementById('main-video');
    if (
      !document.fullscreenElement &&
      !document.mozFullScreenElement &&
      !document.webkitFullscreenElement &&
      !document.msFullscreenElement
    ) {
      if (fs.requestFullscreen) {
        fs.requestFullscreen();
      } else if (fs.msRequestFullscreen) {
        fs.msRequestFullscreen();
      } else if (fs.mozRequestFullScreen) {
        fs.mozRequestFullScreen();
      } else if (fs.webkitRequestFullscreen) {
        fs.webkitRequestFullscreen();
      }
    }
  };

  exitHandler = () => {
    this.setState({
      fullScreenMode: !this.state.fullScreenMode,
    });
  };

  render() {
    const { isError, isLoading, isPublic } = this.props;
    const { messages } = this.state;

    if (isError) {
      return <NoRoomFoundPlaceholder />;
    }

    if (!isPublic && !isLoading) {
      return <ProtectedRoom />;
    }

    return (
      <RoomPageWrapperWithLoading isLoading={isLoading}>
        <StreamBox
          onCheckboxChange={this.onCheckboxChange}
          joinSession={this.joinSession}
          stopSharingScreen={this.stopSharingScreen}
          goFullScreen={this.goFullScreen}
          shareScreen={this.shareScreen}
          showVideo={this.showVideo}
          muteAudio={this.muteAudio}
          getVideoClassName={this.getVideoClassName}
          handleMainVideoStream={this.handleMainVideoStream}
          initialAudio={this.state.initialAudio}
          initialVideo={this.state.initialVideo}
          sessionStarted={this.state.sessionStarted}
          isSharingScreen={this.state.isSharingScreen}
          publishVideo={this.state.publishVideo}
          isSpeaking={this.state.isSpeaking}
          publishAudio={this.state.publishAudio}
          publisher={this.state.publisher}
          mainStreamManager={this.state.mainStreamManager}
          subscribers={this.state.subscribers}
          fullScreenMode={this.state.fullScreenMode}
        />
        <ChatBox
          messages={messages}
          sendChatMessage={this.sendChatMessage}
          isSession={this.state.session !== undefined}
        />
      </RoomPageWrapperWithLoading>
    );
  }
}

RoomPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      roomId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  isPublic: PropTypes.bool.isRequired,
  getRoomDetailsFunc: PropTypes.func.isRequired,
  token: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  roomId: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.session.roomDetails.isLoading,
  isError: state.session.roomDetails.isError,
  isPublic: state.session.roomDetails.isPublic,
  token: state.common.authUser.keycloakInfo.token,
  username: state.common.authUser.keycloakInfo.userInfo.preferred_username,
});

const mapDispatchToProps = dispatch => ({
  getRoomDetailsFunc: roomId => dispatch(getRoomDetails(roomId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RoomPage);
