import { combineReducers } from 'redux';
import openViduSession from 'room-page/reducers/sessionReducer';
import roomDetailsReducer from 'room-page/reducers/roomDetailsReducer';

export default combineReducers({
  openViduReducer: openViduSession,
  roomDetails: roomDetailsReducer,
});
