import React from 'react';
import ProfileEditForm from 'profile-page/components/ProfileEditForm';
import ProfileNotifications from 'profile-page/components/ProfileNotifications';
import ProfilePrivacy from 'profile-page/components/ProfilePrivacy';
import styled from 'styled-components';
import Tabs from 'common/components/tabs/Tabs';

const ProfileSettingInputWrapper = styled.div.attrs({ className: 'profile-page-input-wrapper' })`
  padding-top: 20px;
  border: 1px solid #f0f0f0;
  grid: profile-settings-input-area;
  background: #ffffff;
  border-radius: 4px;
  z-index: 1;
`;

const ProfileSettingsInput = () => (
  <ProfileSettingInputWrapper>
    <Tabs>
      <div label="Account Settings">
        <ProfileEditForm />
      </div>
      <div label="Privacy">
        <ProfilePrivacy />
      </div>
      <div label="Notifications">
        <ProfileNotifications />
      </div>
    </Tabs>
  </ProfileSettingInputWrapper>
);

export default ProfileSettingsInput;
