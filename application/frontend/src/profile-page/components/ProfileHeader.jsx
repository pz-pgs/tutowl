import React, { Component } from 'react';
import styled from 'styled-components';
import ChangeCoverModal from 'profile-page/components/ChangeCoverModal';
import bgImage from 'images/profile-header-bg.svg';
import bgImageTwo from 'images/profile-header-bg-2.svg';
import bgImageThree from 'images/profile-header-bg-3.svg';
import cameraIcon from 'images/icons/camera.svg';

const ProfileHeaderComponent = styled.div.attrs({ className: 'profile-header-component' })`
  height: 200px;
  position: relative;
`;

const ButtonBox = styled.div.attrs({ className: 'button-box' })`
  position: absolute;
  top: 25px;
  right: 50px;
`;

const ChangeCoverButton = styled.button.attrs({ className: 'change-cover-button' })`
  background: none;
  border: 1px solid #fff;
  padding: 5px 10px 5px 40px;
  font-family: 'Sen', sans-serif;
  color: #fff;
  font-weight: 100;
  border-radius: 3px;
  line-height: 25px;
  cursor: pointer;
  outline: none;
  position: relative;
  transition: 0.2s;

  &:hover {
    background: rgba(255, 255, 255, 0.25);
  }
`;

const ChangeCoverIcon = styled.img.attrs({
  className: 'change-cover-icon',
  alt: 'change-cover-icon',
})`
  width: 20px;
  height: 20px;
  filter: invert(0.9);
  position: absolute;
  top: 8px;
  left: 10px;
`;

class ProfileHeader extends Component {
  state = {
    backgroundImage: bgImage,
    showModal: false,
  };

  changeBgImage = imageChoice => {
    if (imageChoice === 0) {
      this.setState({
        backgroundImage: bgImage,
      });
    } else if (imageChoice === 1) {
      this.setState({
        backgroundImage: bgImageTwo,
      });
    } else {
      this.setState({
        backgroundImage: bgImageThree,
      });
    }
  };

  showChangeCoverModal = () => (
    <ChangeCoverModal
      onClose={() => {
        this.setState({ showModal: false });
      }}
      onChoice={chosenCover => {
        this.setState({ showModal: false }, () => {
          this.changeBgImage(chosenCover);
        });
      }}
    />
  );

  clickModal = () => {
    this.setState({
      showModal: !this.state.showModal,
    });
  };

  render() {
    const { backgroundImage, showModal } = this.state;

    return (
      <ProfileHeaderComponent style={{ background: `url('${backgroundImage}')` }}>
        <ButtonBox>
          <ChangeCoverButton onClick={this.clickModal}>Change cover</ChangeCoverButton>
          <ChangeCoverIcon src={cameraIcon} />
        </ButtonBox>
        {showModal && this.showChangeCoverModal()}
      </ProfileHeaderComponent>
    );
  }
}

export default ProfileHeader;
