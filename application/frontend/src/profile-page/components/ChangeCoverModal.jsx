import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import GenericModal from 'common/components/GenericModal';
import bgImage from 'images/profile-header-bg.svg';
import bgImageTwo from 'images/profile-header-bg-2.svg';
import bgImageThree from 'images/profile-header-bg-3.svg';

const ModalTitle = styled.p.attrs({ className: 'modal-title' })`
  font-size: 14px;
  margin: 0 0 15px 0;
  font-weight: bold;
`;

const CoverList = styled.ul.attrs({ className: 'cover-list' })`
  list-style-type: none;
  margin: 0 auto;
  text-align: center;
  padding: 0;
`;

const CoverListItem = styled.li.attrs({ className: 'cover-list-item' })`
  margin: 0 20px;
  padding: 0;
  cursor: pointer;
  display: inline-block;
`;

const CoverListItemImage = styled.img.attrs({
  className: 'cover-list-item-image',
  alt: 'cover-list-item-image',
})`
  width: 80px;
  height: 80px;
  border: 2px solid #f0f0f0;
  border-radius: 5px;
`;

const ChangeCoverModal = ({ onClose, onChoice }) => (
  <GenericModal onClose={onClose}>
    <ModalTitle>Choose profile background cover:</ModalTitle>
    <CoverList>
      <CoverListItem onClick={() => onChoice(0)}>
        <CoverListItemImage src={bgImage} />
      </CoverListItem>
      <CoverListItem onClick={() => onChoice(1)}>
        <CoverListItemImage src={bgImageTwo} />
      </CoverListItem>
      <CoverListItem onClick={() => onChoice(2)}>
        <CoverListItemImage src={bgImageThree} />
      </CoverListItem>
    </CoverList>
  </GenericModal>
);

ChangeCoverModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  onChoice: PropTypes.func.isRequired,
};

export default ChangeCoverModal;
