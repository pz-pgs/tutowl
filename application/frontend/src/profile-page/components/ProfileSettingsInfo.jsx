import React, { Component } from 'react';
import styled from 'styled-components';
import LinkToProfile from 'profile-page/components/LinkToProfile';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const ProfileSettingInfoWrapper = styled.div.attrs({ className: 'profile-page-info-wrapper' })`
  border: 1px solid #f0f0f0;
  grid: profile-settings-info-area;
  background: #ffffff;
  border-radius: 3px;
  z-index: 2;
  font-family: 'Titillium Web', sans-serif;
`;

const InfoList = styled.ul.attrs({ className: 'info-list' })`
  list-style-type: none;
  margin: 20px 0 0 0;
  padding: 0;
`;

const InfoListItem = styled.li.attrs({ className: 'info-list-item' })`
  margin: 0;
  padding: 15px;
  color: #545e79;
  border-top: 1px solid #f0f0f0;
  font-size: 12px;
  display: grid;
  grid-template-columns: 3fr 1fr;
  grid-template-areas: 'info-list-left info-list-right';

  &:last-child {
    border-bottom: 1px solid #f0f0f0;
  }
`;

const InfoListLeft = styled.div.attrs({ className: 'info-list-left' })`
  grid: info-list-left;
  text-align: left;
`;

const InfoListRight = styled.div.attrs({ className: 'info-list-right' })`
  grid: info-list-right;
  text-align: right;
  color: #333f60;
  font-weight: bold;
`;

const ProfileImageComponent = styled.div.attrs({
  className: 'profile-image-component',
})`
  width: 120px;
  height: 120px;
  border: 1px solid #f0f0f0;
  border-radius: 50%;
  background: #2d4564;
  line-height: 120px;
  color: #fff;
  text-align: center;
  font-size: 26px;

  display: block;
  margin: 20px auto;
`;

const ProfileName = styled.p.attrs({ className: 'profile-name' })`
  font-size: 16px;
  text-align: center;
  margin: 20px 0 0 0;
  color: #333f60;
  font-weight: bold;
`;

const ViewProfileButton = styled(Link).attrs({ className: 'view-profile-button' })`
  border-radius: 4px;
  backgorund: none;
  border: 1px solid #f0f0f0;
  font-size: 12px;
  text-align: center;
  transition: 0.2s;
  display: block;
  margin: 10px auto 0 auto;
  width: 75%;
  color: #000;
  outline: none;
  cursor: pointer;
  padding: 10px;
  text-decoration: none;

  &:hover {
    background: #f0f0f0;
  }
`;

const ProfileCompany = styled.p.attrs({ className: 'profile-name' })`
  font-size: 13px;
  text-align: center;
  font-weight: 100;
  color: #b2bcc5;
  margin: 0;
`;

const LIST_INFO = [
  {
    id: 0,
    name: 'Attented live streams:',
    value: 29,
  },
  {
    id: 1,
    name: 'Created groups:',
    value: 5,
  },
  {
    id: 2,
    name: 'Private messages:',
    value: 420,
  },
];

class ProfileSettingsInfo extends Component {
  renderList = () => (
    <InfoList>
      {LIST_INFO.map(info => (
        <InfoListItem key={info.id}>
          <InfoListLeft>{info.name}</InfoListLeft>
          <InfoListRight>{info.value}</InfoListRight>
        </InfoListItem>
      ))}
    </InfoList>
  );

  render() {
    return (
      <ProfileSettingInfoWrapper>
        <ProfileImageComponent>{this.props.username.charAt(0).toUpperCase()}</ProfileImageComponent>
        <ProfileName>{this.props.username}</ProfileName>
        <ProfileCompany>PGS Software</ProfileCompany>
        {this.renderList()}
        <ViewProfileButton to={`profile/${this.props.username}`}>
          View Public Profile
        </ViewProfileButton>
        <LinkToProfile link={`http://keycloak.test/profile/${this.props.username}`} />
      </ProfileSettingInfoWrapper>
    );
  }
}

ProfileSettingsInfo.propTypes = {
  username: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  username: state.common.authUser.keycloakInfo.userInfo.preferred_username,
});

export default connect(mapStateToProps, null)(ProfileSettingsInfo);
