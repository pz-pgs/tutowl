import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const UpdateButtonBox = styled.div.attrs({ className: 'update-button-text' })`
  border-top: 1px solid #f0f0f0;
  width: 95%;
  padding: 0 20px;
  margin-top: 40px;
`;

const UpdateButtonComponent = styled.button.attrs({ className: 'update-button' })`
  outline: none;
  border-radius: 4px;
  background: #2d4564;
  font-family: 'Titillium Web', sans-serif;
  color: #fff;
  font-size: 13px;
  padding: 10px;
  width: 100px;
  margin: 20px 0 20px 0;
  cursor: pointer;
  transition: 0.2s;

  &:hover {
    opacity: 0.8;
  }
`;

const UpdateButton = ({ style }) => (
  <UpdateButtonBox style={style}>
    <UpdateButtonComponent>Update</UpdateButtonComponent>
  </UpdateButtonBox>
);

UpdateButton.defaultProps = {
  style: {},
};

UpdateButton.propTypes = {
  style: PropTypes.instanceOf(Object),
};

export default UpdateButton;
