import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import styled from 'styled-components';
import GenericModal from 'common/components/GenericModal';

const ModalTitle = styled.p.attrs({ className: 'modal-title' })`
  font-size: 15px;
  margin: 0 0 15px 0;
  font-weight: bold;
`;

const ProfileImageWrapper = styled.div.attrs({ className: 'profile-image-wrapper' })`
  padding: 30px;
`;

const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16,
};

const thumb = {
  display: 'inline-flex',
  borderRadius: 2,
  border: '1px dotted #eaeaea',
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: 'border-box',
};

const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden',
};

const img = {
  display: 'block',
  width: 'auto',
  height: '100%',
};

const DropzoneSection = styled.p.attrs({ className: 'dropzone-section' })`
  border: 1px dashed #2d4564;
  padding: 20px;
  border-radius: 4px;
  outline: none;
  font-size: 13px;
`;

const DropzoneComponent = styled.div.attrs({ className: 'dropzone-component' })`
  outline: none;
`;

const SaveButton = styled.button.attrs({ className: 'save-button' })`
  border-radius: 4px;
  outline: none;
  font-size: 13px;
  padding: 10px 20px;
  cursor: pointer;
  margin-top: 20px;
  float: right;
  background: #2d4564;
  color: #ffffff;
`;

const ChangeProfileImageModal = ({ onClose, onSave }) => {
  const [files, setFiles] = useState([]);
  const { getRootProps, getInputProps } = useDropzone({
    accept: 'image/*',
    onDrop: acceptedFiles => {
      setFiles(
        acceptedFiles.map(file =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          }),
        ),
      );
    },
  });

  const thumbs = files.map(file => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img src={file.preview} style={img} alt="" />
      </div>
    </div>
  ));

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [files],
  );

  return (
    <GenericModal onClose={onClose}>
      <ProfileImageWrapper>
        <ModalTitle>Change profile image</ModalTitle>
        <section className="container">
          <DropzoneComponent {...getRootProps({ className: 'dropzone' })}>
            <input {...getInputProps()} />
            <DropzoneSection>
              Drag and drop some files here, or click to select files
            </DropzoneSection>
          </DropzoneComponent>
          <aside style={thumbsContainer}>{thumbs}</aside>
        </section>

        <SaveButton onClick={onSave}>Save</SaveButton>
      </ProfileImageWrapper>
    </GenericModal>
  );
};

ChangeProfileImageModal.propTypes = {
  onSave: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ChangeProfileImageModal;
