import React from 'react';
import styled from 'styled-components';
import ProfileSettingsInfo from 'profile-page/components/ProfileSettingsInfo';
import ProfileSettingsInput from 'profile-page/components/ProfileSettingsInput';

const ProfileSettingsWrapper = styled.div.attrs({ className: 'profile-page-wrapper' })`
  padding: 0 50px;
  display: grid;
  grid-template-columns: 1fr 4fr;
  grid-template-areas: 'profile-settings-info-area profile-settings-input-area';
  grid-gap: 40px;
  margin-top: -64px;
`;

const ProfileSettings = () => (
  <ProfileSettingsWrapper>
    <ProfileSettingsInfo />
    <ProfileSettingsInput />
  </ProfileSettingsWrapper>
);

export default ProfileSettings;
