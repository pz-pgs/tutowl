import {
  fetchAllNotifications,
  checkINewNotifications,
  markNotificationsAsRead,
} from 'common/handlers/notificationsHandler';

export const FETCH_NOTIFICATIONS_PENDING = 'FETCH_NOTIFICATIONS_PENDING';
export const FETCH_NOTIFICATIONS_OK = 'FETCH_NOTIFICATIONS_OK';
export const FETCH_NOTIFICATIONS_FAIL = 'FETCH_NOTIFICATIONS_FAIL';

export const FETCH_IF_NEW_NOTIFICATIONS_PENDING = 'FETCH_IF_NEW_NOTIFICATIONS_PENDING';
export const FETCH_IF_NEW_NOTIFICATIONS_OK = 'FETCH_IF_NEW_NOTIFICATIONS_OK';
export const FETCH_IF_NEW_NOTIFICATIONS_FAIL = 'FETCH_IF_NEW_NOTIFICATIONS_FAIL';

export const ADD_NEW_NOTIFICATION = 'ADD_NEW_NOTIFICATION';
export const NOTIFICATIONS_CLICKED = 'NOTIFICATIONS_CLICKED';
export const SET_NOTIFICATION_BELL = 'SET_NOTIFICATION_BELL';
export const ALL_NOTIFICATIONS_READ = 'ALL_NOTIFICATIONS_READ';
export const ALL_NOTIFICATIONS_READ_ON_BACKEND = 'ALL_NOTIFICATIONS_READ_ON_BACKEND';

export const makeMarkNotificationsAsReadOk = () => ({
  type: ALL_NOTIFICATIONS_READ_ON_BACKEND,
});

export const makeFetchNotificationsPending = () => ({
  type: FETCH_NOTIFICATIONS_PENDING,
});

export const makeFetchNotificationsOk = notificationsList => ({
  type: FETCH_NOTIFICATIONS_OK,
  payload: { notificationsList },
});

export const makeFetchNotificationsFail = () => ({
  type: FETCH_NOTIFICATIONS_FAIL,
});

export const makeFetchIfNewNotificationsPending = () => ({
  type: FETCH_IF_NEW_NOTIFICATIONS_PENDING,
});

export const makeFetchIfNewNotificationsOk = newNotifications => ({
  type: FETCH_IF_NEW_NOTIFICATIONS_OK,
  payload: { newNotifications },
});

export const makeFetchIfNewNotificationsFail = () => ({
  type: FETCH_IF_NEW_NOTIFICATIONS_FAIL,
});

export const makeAddNewNotification = newNotification => ({
  type: ADD_NEW_NOTIFICATION,
  payload: { newNotification },
});

export const makeNotificationsClicked = () => ({
  type: NOTIFICATIONS_CLICKED,
});

export const makeAllNotificationsAsRead = () => ({
  type: ALL_NOTIFICATIONS_READ,
});

export const makeSetNotificationBell = newNotifications => ({
  type: SET_NOTIFICATION_BELL,
  payload: { newNotifications },
});

export const fetchNotifications = () => dispatch => {
  dispatch(makeFetchNotificationsPending());

  return fetchAllNotifications()
    .then(res => {
      dispatch(makeFetchNotificationsOk(res.data));
    })
    .catch(() => {
      dispatch(makeFetchNotificationsFail());
    });
};

export const addNewNotification = newNotification => dispatch => {
  dispatch(makeAddNewNotification(newNotification));
};

export const clickOnNotifications = () => dispatch => {
  dispatch(makeNotificationsClicked());
};

export const setNotificationsBell = newNotifications => dispatch => {
  dispatch(makeSetNotificationBell(newNotifications));
};

export const checkIfAnyNewNotifications = () => dispatch => {
  dispatch(makeFetchIfNewNotificationsPending());

  return checkINewNotifications()
    .then(res => {
      dispatch(makeFetchIfNewNotificationsOk(res.data));
    })
    .catch(() => {
      dispatch(makeFetchIfNewNotificationsFail());
    });
};

export const markAllNotificationsAsRead = () => dispatch => {
  dispatch(makeAllNotificationsAsRead());
};

export const markAllNotificationsAsReadOnBackend = () => dispatch =>
  markNotificationsAsRead().then(() => {
    dispatch(makeMarkNotificationsAsReadOk());
  });
