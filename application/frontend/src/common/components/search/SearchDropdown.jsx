import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import ProgressIndicatorCircular from 'common/components/ProgressIndicatorCircular';

const SearchDropdownWrapper = styled.div.attrs({ className: 'search-dropdown-wrapper' })`
  background: #fff;
  border: 1px solid #f0f0f0;
  position: absolute;
  top: 44px;
  left: 13px;
  width: 90%;
  z-index: 5;
  font-size: 10px;
  padding: 5px 10px;
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.25);
`;

const SearchResultList = styled.ul.attrs({ className: 'search-result-list' })`
  list-style-type: none;
  margin: 0;
  padding: 0;
  width: calc(100% - 10px);
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 10px;
  grid-template-areas: 'category-area category-area category-area';
`;

const SearchResultListItem = styled.li.attrs({ className: 'search-result-list-item' })`
  margin: 0;
  padding: 0;
  grid: category-area;
  padding: 10px;
  width: 33%;
`;

const SearchResultCategoryName = styled.p.attrs({ className: 'search-result-category-name' })`
  margin: 0;
  font-weight: bold;
  text-transform: uppercase;
  letter-spacing: 2px;
`;

const ResultItemWrapper = styled.ul.attrs({ className: 'result-item-wrapper' })`
  list-style-type: none;
  margin: 0;
  padding: 0;
  width: 100%;
`;

const ResultItemWrapperItem = styled.li.attrs({ className: 'result-item' })`
  margin: 10px 0 0 0;
  padding: 5px;
  width: 100%;
  display: grid;
  grid-template-columns: 1fr 6fr;
  grid-gap: 10px;
  grid-template-areas: 'result-image-area result-text-area';
  letter-spacing: 1px;
  cursor: pointer;
  border-bottom: 1px solid #f0f0f0;

  &:hover {
    background: #f0f0f0;
  }

  &:last-child {
    border-bottom: none;
  }
`;

const ResultImageArea = styled.div.attrs({ className: 'result-image-area' })`
  grid: result-image-area;
`;

const ResultImageAreaItem = styled.div.attrs({ className: 'result-image-area-item' })`
  border-radius: 50%;
  width: 25px;
  height: 25px;
  line-height: 25px;
  text-align: center;
  background: #2d4564;
  color: #fff;
`;

const ResultTextArea = styled.div.attrs({ className: 'result-text-area' })`
  grid: result-text-area;
`;

const ResultTextAreaTop = styled.p.attrs({ className: 'result-text-area-top' })`
  margin: 0;
  padding: 0;
`;

const ResultTextAreaBottom = styled.p.attrs({ className: 'result-text-area-bottom' })`
  margin: 0;
  padding: 0;
  font-weight: 100;
`;

const NoResults = styled.p.attrs({ className: 'no-results-found' })`
  width: 100%;
  text-align: left;
`;

const SearchErrorBlock = styled.div.attrs({ className: 'search-error-block' })`
  padding: 10px;
  margin: 5px 0;
  text-align: center;
  background: #fce7e6;
  font-size: 12px;
  color: #552526;
`;

const StyledLink = styled(Link).attrs({ className: 'styled-link' })`
  text-decoration: none;
  color: #000000;
`;

class SearchDropdown extends Component {
  checkIfRoomCategory = categoryName => categoryName.toLowerCase() === 'rooms';

  renderSearchResultList = () => (
    <SearchResultList>
      {this.props.searchResult.map(category => (
        <SearchResultListItem key={category.id}>
          <SearchResultCategoryName>
            {category.name} ({category.count})
          </SearchResultCategoryName>
          {category.list.length > 0 ? (
            <ResultItemWrapper>
              {category.list.map(result => (
                <StyledLink
                  to={this.checkIfRoomCategory(category.name) ? `/room/${result.id}` : '/'}
                  key={result.id}
                >
                  <ResultItemWrapperItem>
                    <ResultImageArea>
                      <ResultImageAreaItem>
                        {result.topText.charAt(0).toUpperCase()}
                      </ResultImageAreaItem>
                    </ResultImageArea>
                    <ResultTextArea>
                      <ResultTextAreaTop>{result.topText}</ResultTextAreaTop>
                      <ResultTextAreaBottom>{result.bottomText}</ResultTextAreaBottom>
                    </ResultTextArea>
                  </ResultItemWrapperItem>
                </StyledLink>
              ))}
            </ResultItemWrapper>
          ) : (
            <NoResults>No results found.</NoResults>
          )}
        </SearchResultListItem>
      ))}
    </SearchResultList>
  );

  render() {
    const { isLoading, isError } = this.props;

    if (isLoading)
      return (
        <SearchDropdownWrapper style={{ height: '200px' }}>
          <ProgressIndicatorCircular size={40} />
        </SearchDropdownWrapper>
      );

    if (isError) {
      return (
        <SearchDropdownWrapper>
          <SearchErrorBlock>An error occurred while searching.</SearchErrorBlock>
        </SearchDropdownWrapper>
      );
    }

    return <SearchDropdownWrapper>{this.renderSearchResultList()}</SearchDropdownWrapper>;
  }
}

SearchDropdown.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  searchResult: PropTypes.instanceOf(Array).isRequired,
};

export default SearchDropdown;
