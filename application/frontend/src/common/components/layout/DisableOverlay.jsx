import React from 'react';
import styled from 'styled-components';
import ProgressIndicatorCircular from 'common/components/ProgressIndicatorCircular';

const CentralSpinner = styled.div.attrs({ className: 'app-disabled-overlay ' })`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 10;
`;

export const applyStyle = isDisabled =>
  isDisabled
    ? {
        opacity: 0.5,
        pointerEvents: 'none',
      }
    : {};

const DisableOverlay = () => (
  <CentralSpinner>
    <ProgressIndicatorCircular css={{ xxlarge: true }} />
  </CentralSpinner>
);

export default DisableOverlay;
