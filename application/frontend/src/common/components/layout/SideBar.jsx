/* eslint-disable no-nested-ternary */
import React, { Component } from 'react';
import styled from 'styled-components';
import SideFooter from 'common/components/layout/SideFooter';
import IntegrateApps from 'common/components/layout/IntegrateApps';
import homeIcon from 'images/icons/home.svg';
import { Link } from 'react-router-dom';
import lessonIcon from 'images/icons/lesson.svg';
import resourcesIcon from 'images/icons/resources.svg';
import groupsIcon from 'images/icons/groups.svg';
import PropTypes from 'prop-types';
import messagesIcon from 'images/icons/messages.svg';
import tasksIcon from 'images/icons/tasks.svg';
import calendarIcon from 'images/icons/calendar.svg';
import Colors from 'common/colors';
import { getUnreadMessagesCount, increaseUnreadMesssages } from 'common/actions/newMessagesActions';
import { connect } from 'react-redux';
import SockJsClient from 'react-stomp';
import messageSound from 'sounds/message.mp3';

const SideBarWrapper = styled.div.attrs({ className: 'side-bar-header-wrapper' })`
  grid-area: side-bar-wrapper;
  background: ${Colors.WHITE};
  position: relative;
  border-right: 1px solid ${Colors.GALLERY};
  max-height: 90vh;
  min-width: 80px;
`;

const SideBarMenuWrapper = styled.ul.attrs({ className: 'sidebar-menu-wrapper' })`
  list-style-type: none;
  margin: 0;
  width: 100;
  padding: 0;
  font-family: 'Roboto-Light', sans-serif;
`;

const SideBarMenuItemWrapper = styled.li.attrs({ className: 'sidebar-menu-item-wrapper' })`
  margin: 0;
  background: ${Colors.WHITE};
  border-right: none;
  text-align: center;
  font-size: 14px;
  padding: 15px 0;
  font-weight: bold;
  color: ${Colors.LIGHT_BLACK};
  transition: 0.2s;

  &:first-child {
    border-top: none;
  }

  &:hover {
    cursor: pointer;
    color: #4369a2 !important;
    border-right: 3px solid #4369a2;
    background: #f3f7fd;
  }

  &:hover img {
    filter: invert(0.1) sepia(100%) hue-rotate(190deg) saturate(500%) !important;
  }
`;

const SideBarItemImg = styled.img.attrs({ className: 'sidebar-item-img', alt: 'sidebar-item-img' })`
  display: inline-block;
  margin: 0 auto;
  width: 24px;
  height: 24px;
  grid: img-area;
`;

const StyledLink = styled(Link).attrs({ className: 'sidebar-styled-link' })`
  font-size: 14px;
  line-height: 24px;
  font-weight: bold;
  text-decoration: none;
  display: grid;
  grid-template-columns: 1fr 2fr;
  line-height: 24px;
  grid-template-areas: 'img-area text-area';

  @media only screen and (max-width: 920px) {
    grid-template-areas: 'img-area';
    grid-template-columns: 1fr;
  }
`;

const SideBarItemText = styled.div.attrs({ className: 'sidebar-item-text' })`
  grid: text-area;
  text-align: left;
  font-size: 12px;
  color: #333;

  @media only screen and (max-width: 920px) {
    display: none;
  }
`;

const NewMessagesCount = styled.div.attrs({ className: 'new-messages-count' })`
  background: #fce7e6;
  color: #552526;
  width: 20px;
  line-height: 20px;
  height: 20px;
  font-size: 10px;
  border-radius: 50%;
  text-align: center;
  display: inline-block;
  float: right;
  margin-right: 10px;
  margin-top: 3px;
`;

const MENU_ICONS = [
  {
    id: 0,
    link: '/',
    name: 'Overview',
    icon: homeIcon,
  },
  {
    id: 1,
    link: '/classes',
    name: 'Classes',
    icon: lessonIcon,
  },
  {
    id: 2,
    link: '/messages',
    name: 'Messages',
    icon: messagesIcon,
  },
  {
    id: 3,
    link: '/calendar',
    name: 'Calendar',
    icon: calendarIcon,
  },
  {
    id: 4,
    link: '/',
    name: 'Resources',
    icon: resourcesIcon,
  },
  {
    id: 5,
    link: '/',
    name: 'Tasks',
    icon: tasksIcon,
  },
  {
    id: 6,
    link: '/',
    name: 'Groups',
    icon: groupsIcon,
  },
];

const activeTabStyle = {
  cursor: 'pointer',
  color: '#4369a2',
  borderRight: '3px solid #4369a2',
  background: '#f3f7fd',
};

const activeImageStyle = {
  filter: 'filter: invert(0.1) sepia(100%) hue-rotate(190deg) saturate(500%) !important',
};

class SideBar extends Component {
  state = {
    activeTab: window.location.href.split('/').pop(),
    audio: new Audio(messageSound),
  };

  componentDidMount() {
    this.props.getUnreadMessagesCountFunc();
  }

  onMessageReceive = msg => {
    if (msg.type === 'CHAT' && this.props.chosenChatUser !== msg.sender) {
      this.state.audio.play();
      this.props.increaseUnreadMesssagesFunc();
    }
  };

  compareStrings = (stringOne, stringTwo) => stringOne.toUpperCase() === stringTwo.toUpperCase();

  isActiveTab = item =>
    this.compareStrings(item.name, this.state.activeTab) ||
    (this.state.activeTab === '' && item.name === 'Overview') ||
    (window.location.href.includes('/room/') && item.name === 'Classes');

  renderMessageCount = () => {
    if (this.props.messagesCount > 9) return '9+';
    return this.props.messagesCount;
  };

  renderMenuIcons = () => (
    <SideBarMenuWrapper>
      {MENU_ICONS.map(item => (
        <SideBarMenuItemWrapper key={item.id} style={this.isActiveTab(item) ? activeTabStyle : {}}>
          <StyledLink to={item.link}>
            <SideBarItemImg
              src={item.icon}
              style={this.isActiveTab(item) ? activeImageStyle : {}}
            />
            <SideBarItemText>
              {item.name}
              {item.name === 'Messages' ? (
                this.props.messagesCount === 0 ? null : (
                  <NewMessagesCount>{this.renderMessageCount()}</NewMessagesCount>
                )
              ) : null}
            </SideBarItemText>
          </StyledLink>
        </SideBarMenuItemWrapper>
      ))}
    </SideBarMenuWrapper>
  );

  render() {
    const wsSourceUrl = `${window.location.protocol}//${window.location.host}/api/chat/ws`;

    return (
      <SideBarWrapper>
        {this.renderMenuIcons()}
        <IntegrateApps />
        <SideFooter />

        <SockJsClient
          url={wsSourceUrl}
          topics={[`/user/${this.props.currentUsername.toString().toLowerCase()}/reply`]}
          onMessage={this.onMessageReceive}
          ref={client => {
            this.clientRef = client;
          }}
        />
      </SideBarWrapper>
    );
  }
}

SideBar.propTypes = {
  messagesCount: PropTypes.number.isRequired,
  getUnreadMessagesCountFunc: PropTypes.func.isRequired,
  increaseUnreadMesssagesFunc: PropTypes.func.isRequired,
  currentUsername: PropTypes.string.isRequired,
  chosenChatUser: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  chosenChatUser: state.messages.usersList.currentChatUsername,
  messagesCount: state.common.messagesSidebar.messagesCount,
  currentUsername: state.common.authUser.keycloakInfo.userInfo.preferred_username,
});

const mapDispatchToProps = dispatch => ({
  getUnreadMessagesCountFunc: () => dispatch(getUnreadMessagesCount()),
  increaseUnreadMesssagesFunc: () => dispatch(increaseUnreadMesssages()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
