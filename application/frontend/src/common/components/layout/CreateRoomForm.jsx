/* eslint-disable react/no-did-update-set-state */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GenericModal from 'common/components/GenericModal';
import TextInput from 'common/components/TextInput';
import withLoading, { ProgIndSize } from 'common/utils/withLoading';
import SelectInput from 'common/components/SelectInput';
import TextArea from 'common/components/TextArea';
import ToggleSwitch from 'common/components/ToggleSwitch';
import ButtonProgressIndicator from 'common/components/ButtonProgressIndicator';
import styled from 'styled-components';
import {
  createRoom,
  createRoomSetDefault,
  checkRoomName,
  getCategoriesNames,
} from 'common/actions/createRoomActions';
import { connect } from 'react-redux';
import DatePicker, { registerLocale } from 'react-datepicker';
import TimeRangeSlider from 'react-time-range-slider';
import enGb from 'date-fns/locale/en-GB';

const CreateRoomFormWrapper = styled.div.attrs({ className: 'create-room-form-wrapper' })`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 15px;
  grid-template-areas: 'create-room-form-left create-room-form-right';
  line-height: 20px;
`;

const FormWrapper = styled.div.attrs({ className: 'form-wrapper' })``;

const LoadingWrapper = withLoading(FormWrapper, ProgIndSize.XX_LARGE);

const CreateFormLeft = styled.div.attrs({ className: 'create-room-form-left' })`
  grid: create-room-form-left;
`;

const CreateFormRight = styled.div.attrs({ className: 'create-room-form-right' })`
  grid: create-room-form-right;
`;

const CreateFormModal = styled.div.attrs({ className: 'create-room-modal' })`
  line-height: 20px;
  max-width: 700px;
`;

const ModalTitle = styled.p.attrs({ className: 'modal-title' })`
  margin: 0 0 10px 0;
  font-size: 20px;
`;

const CreateSuccessTitle = styled.p.attrs({ className: 'create-success-title' })`
  display: block;
  margin: 0 0 30px 0;
  font-size: 15px;
  text-align: center;
  line-height: 20px;
  font-family: 'Roboto', sans-serif !important;
  font-weight: 100;
`;

const RoomPasswordText = styled.span.attrs({ className: 'room-password-text' })`
  color: #59637d;
  font-family: 'Titillium Web', sans-serif;
  font-size: 14px;
  margin: 0 10px 5px 0;
  display: inline-block;
`;

const ErrorBlock = styled.div.attrs({ className: 'error-block' })`
  padding: 10px;
  margin: 5px 0;
  text-align: center;
  background: #fce7e6;
  font-size: 12px;
  color: #552526;
`;

const DatePickerBox = styled.div.attrs({ className: 'date-picker-box' })`
  margin-bottom: 5px;
`;

const DatePickerBoxTitle = styled.p.attrs({ className: 'date-picker-box-title' })`
  margin: 0 0 5px 0;
  font-size: 14px;
  color: #59637d;
  font-family: 'Titillium Web', sans-serif;
  display: inline-block;
`;

const SendButton = styled.button.attrs({ className: 'send-button' })`
  padding: 10px 20px;
  font-size: 12px;
  border-radius: 5px;
  border: 1px solid #f0f0f0;
  background: #2d4564;
  color: #fff;
  cursor: pointer;
  outline: none;
  width: 100%;
  margin-top: 41px;

  &:disabled {
    opacity: 0.6;
  }

  &:disabled:hover {
    cursor: auto;
  }
`;

const CreateRoomSuccessBox = styled.div.attrs({ className: 'create-room-success-box' })`
  padding: 25px 25px 15px 25px;
`;

class CreateRoomForm extends Component {
  state = {
    privateRoom: false,
    roomName: '',
    roomPassword: '',
    roomDescription: '',
    choosenCategory: '',
    date: new Date(),
    timeValue: {
      start: '7:00',
      end: '21:00',
    },
  };

  componentWillMount() {
    registerLocale('en-gb', enGb);
  }

  componentDidMount() {
    this.props.getCategoriesNamesFunc();
  }

  componentDidUpdate() {
    if (this.state.choosenCategory === '' && this.props.categoriesList.length > 0) {
      this.setState({
        choosenCategory: this.props.categoriesList[0].toUpperCase(),
      });
    }
  }

  onSend = () => {
    const {
      roomDescription,
      roomName,
      choosenCategory,
      roomPassword,
      privateRoom,
      date,
    } = this.state;

    const startTimeArray = this.state.timeValue.start.split(':');
    const endTimeArray = this.state.timeValue.end.split(':');

    const startDateToSend = new Date(date);
    startDateToSend.setHours(startTimeArray[0]);
    startDateToSend.setMinutes(startTimeArray[1]);
    startDateToSend.setMilliseconds(0);
    startDateToSend.setSeconds(0);

    const endDateToSend = new Date(date);
    endDateToSend.setHours(endTimeArray[0]);
    endDateToSend.setMinutes(endTimeArray[1]);
    endDateToSend.setMilliseconds(0);
    endDateToSend.setSeconds(0);

    const objectToSend = {
      name: roomName,
      description: roomDescription,
      category: choosenCategory,
      is_public: !privateRoom,
      password: roomPassword,
      event_start_date: startDateToSend,
      event_end_date: endDateToSend,
    };

    this.props.createRoomFunc(objectToSend);
  };

  onCategoryChange = value => {
    this.setState({
      choosenCategory: value.toUpperCase(),
    });
  };

  onTextChange = (name, value) => {
    this.setState({ [name]: value });
  };

  onCloseModal = () => {
    this.props.createRoomSetDefaultFunc();
    this.props.onClose();
  };

  onRoomNameChange = (name, value) => {
    this.setState(
      {
        [name]: value,
      },
      () => {
        if (value.length > 3) this.props.checkRoomNameFunc(value);
      },
    );
  };

  setDate = date => {
    this.setState({ date });
  };

  changePrivatePassword = () => {
    this.setState({
      privateRoom: !this.state.privateRoom,
    });
  };

  isWeekday = date => {
    const day = Date.prototype.getDay(date);
    return day !== 0 && day !== 6;
  };

  canSendForm = () =>
    this.state.roomDescription.length !== 0 &&
    this.state.roomName.length !== 0 &&
    !this.props.nameTaken;

  canSendFormWithPassword = () =>
    this.state.roomDescription.length !== 0 &&
    this.state.roomName.length !== 0 &&
    this.state.roomPassword.length !== 0 &&
    !this.props.nameTaken;

  timeChangeHandler = time => {
    this.setState({
      timeValue: time,
    });
  };

  render() {
    const canSendForm = this.state.privateRoom
      ? this.canSendFormWithPassword()
      : this.canSendForm();
    const { privateRoom, date, timeValue } = this.state;
    const {
      isLoading,
      isError,
      isSuccess,
      nameTaken,
      isCategoriesLoading,
      isCategoriesError,
      categoriesList,
    } = this.props;

    if (isCategoriesError)
      return (
        <GenericModal nClose={this.onCloseModal}>
          <ErrorBlock style={{ lineHeight: '20px' }}>
            There was an error while fetching room categories.
          </ErrorBlock>
        </GenericModal>
      );

    return (
      <GenericModal
        style={isCategoriesLoading ? { padding: '181px 252px' } : {}}
        onClose={this.onCloseModal}
      >
        <LoadingWrapper isLoading={isCategoriesLoading}>
          {isSuccess ? (
            <CreateRoomSuccessBox>
              <CreateSuccessTitle>Room created succesfully!</CreateSuccessTitle>
              <div className="success-checkmark">
                <div
                  style={
                    !isSuccess
                      ? {
                          display: 'none',
                        }
                      : {}
                  }
                  className="check-icon"
                >
                  <span className="icon-line line-tip" />
                  <span className="icon-line line-long" />
                  <div className="icon-circle" />
                  <div className="icon-fix" />
                </div>
              </div>
            </CreateRoomSuccessBox>
          ) : (
            <CreateFormModal>
              <ModalTitle>Create new room</ModalTitle>
              <ErrorBlock
                style={
                  isError || nameTaken
                    ? {}
                    : {
                        display: 'none',
                      }
                }
              >
                {isError ? 'There was an error during room creation. Try again.' : ''}
                {nameTaken ? 'Room name is already taken!' : ''}
              </ErrorBlock>
              <CreateRoomFormWrapper>
                <CreateFormLeft>
                  <TextInput
                    id="roomName"
                    name="roomName"
                    onChange={this.onRoomNameChange}
                    label="Room Name"
                    validator={() => true}
                  />
                  <TextArea
                    name="roomDescription"
                    onChange={this.onTextChange}
                    label="Description"
                  />
                  <React.Fragment>
                    <RoomPasswordText>Protected with password</RoomPasswordText>
                    <ToggleSwitch
                      name="privateRoom"
                      onCheck={this.changePrivatePassword}
                      isChecked={privateRoom}
                      style={{
                        display: 'inline-block',
                      }}
                    />
                  </React.Fragment>
                  <TextInput
                    name="roomPassword"
                    onChange={this.onTextChange}
                    type="password"
                    style={
                      privateRoom
                        ? {}
                        : {
                            visibility: 'hidden',
                          }
                    }
                    label=""
                    validator={() => true}
                  />
                </CreateFormLeft>
                <CreateFormRight>
                  <SelectInput
                    onChange={this.onCategoryChange}
                    options={categoriesList}
                    id="category-select"
                    label="Category"
                  />
                  <DatePickerBox>
                    <DatePickerBoxTitle>Event date</DatePickerBoxTitle>
                    <DatePicker
                      selected={date}
                      onChange={dateObj => this.setDate(dateObj)}
                      minDate={new Date()}
                      locale="en-gb"
                      timeIntervals={15}
                    />
                  </DatePickerBox>
                  <DatePickerBox>
                    <DatePickerBoxTitle>
                      Event time: {this.state.timeValue.start} - {this.state.timeValue.end}
                    </DatePickerBoxTitle>
                    <TimeRangeSlider
                      maxValue="21:00"
                      minValue="7:00"
                      name="time_range"
                      onChange={this.timeChangeHandler}
                      step={15}
                      value={timeValue}
                    />
                  </DatePickerBox>
                  <SendButton disabled={!canSendForm} onClick={this.onSend}>
                    {isLoading ? <ButtonProgressIndicator /> : 'Create'}
                  </SendButton>
                </CreateFormRight>
              </CreateRoomFormWrapper>
            </CreateFormModal>
          )}
        </LoadingWrapper>
      </GenericModal>
    );
  }
}

CreateRoomForm.propTypes = {
  onClose: PropTypes.func.isRequired,
  createRoomFunc: PropTypes.func.isRequired,
  createRoomSetDefaultFunc: PropTypes.func.isRequired,
  checkRoomNameFunc: PropTypes.func.isRequired,
  getCategoriesNamesFunc: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  isSuccess: PropTypes.bool.isRequired,
  nameTaken: PropTypes.bool.isRequired,
  isCategoriesLoading: PropTypes.bool.isRequired,
  isCategoriesError: PropTypes.bool.isRequired,
  categoriesList: PropTypes.instanceOf(Array).isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.common.createRoom.isLoading,
  isError: state.common.createRoom.isError,
  isSuccess: state.common.createRoom.isSuccess,
  wasNameChecked: state.common.createRoom.wasNameChecked,
  isNameLoading: state.common.createRoom.isNameLoading,
  nameTaken: state.common.createRoom.nameTaken,
  isCategoriesLoading: state.common.createRoom.isCategoriesLoading,
  isCategoriesError: state.common.createRoom.isCategoriesError,
  categoriesList: state.common.createRoom.categoriesList,
});

const mapDispatchToProps = dispatch => ({
  createRoomFunc: data => dispatch(createRoom(data)),
  checkRoomNameFunc: roomName => dispatch(checkRoomName(roomName)),
  createRoomSetDefaultFunc: () => dispatch(createRoomSetDefault()),
  getCategoriesNamesFunc: () => dispatch(getCategoriesNames()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateRoomForm);
