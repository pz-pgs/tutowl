import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import dropDownIcon from 'images/icons/down-arrow.svg';
import ProfileActionsDropdownMenu from 'common/components/layout/ProfileActionsDropdownMenu';

const ProfileCircleWrapper = styled.div.attrs({ className: 'profile-circle-wrapper' })`
  display: inline-block;
  line-height: 70px;
  float: right;
  font-size: 12px;
  margin-left: 15px;
  position: relative;
`;

const UserImg = styled.div.attrs({ className: 'user-img' })`
  margin-top: 15px;
  width: 36px;
  height: 36px;
  border-radius: 50%;
  border: 1px solid #f0f0f0;
  cursor: pointer;
  margin-right: 10px;
  text-align: center;
  color: #fff;
  background: #2d4564;
  display: inline-block;
  line-height: 36px;
  transition: 0.2s;

  &:hover {
    opacity: 0.8;
  }
`;

const DropDownIcon = styled.img.attrs({ className: 'drop-down-icon', alt: 'drop-down-icon' })`
  width: 12px;
  height: 12px;
  cursor: pointer;
`;

const DropdownHoverBox = styled.div.attrs({ className: 'dropdown-hover-box' })`
  position: absolute;
  right: 0;
  display: inline-block;
  z-index: 10;
`;

const RelativeBox = styled.div.attrs({ className: 'relative-box' })`
  position: relative;
`;

const UsernameBox = styled.p.attrs({ className: 'username-box' })`
  margin: 0 20px 0 0;
  padding: 0;
  display: inline-block;
  height: 70px;
  line-height: 70px;
  float: right;
`;

class ProfileCircle extends Component {
  state = {
    showDropDown: false,
  };

  onClick = () => {
    this.setState({ showDropDown: !this.state.showDropDown });
  };

  render() {
    const { showDropDown } = this.state;
    const { onLogout } = this.props;

    return (
      <ProfileCircleWrapper>
        <UserImg>{this.props.username.charAt(0).toUpperCase()}</UserImg>
        <UsernameBox>{this.props.username}</UsernameBox>
        <DropdownHoverBox>
          <RelativeBox>
            <DropDownIcon src={dropDownIcon} onClick={this.onClick} />
            {showDropDown && (
              <ProfileActionsDropdownMenu onLogout={onLogout} onClose={this.onClick} />
            )}
          </RelativeBox>
        </DropdownHoverBox>
      </ProfileCircleWrapper>
    );
  }
}

ProfileCircle.propTypes = {
  onLogout: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
};

export default ProfileCircle;
