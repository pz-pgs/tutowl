import React, { Component } from 'react';
import PropTypes from 'prop-types';
import triangleImg from 'images/triangleImg.png';
import styled from 'styled-components';
import logoutIcon from 'images/icons/logout_2.svg';
import onClickOutside from 'react-onclickoutside';

const DropdownMenu = styled.div.attrs({ className: 'dropdown-menu' })`
  padding: 10px;
  background: #ffffff;
  position: absolute;
  bottom: -40px;
  left: -95px;
  width: 100px;

  border: 1px solid rgba(100, 100, 100, 0.4);
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.25);
`;

const TopTriangle = styled.div.attrs({ className: 'top-triangle' })`
  background-image: url(${triangleImg});
  background-repeat: no-repeat;
  background-size: 33px 521px;
  background-position: 0 -305px;
  height: 11px;
  position: absolute;
  top: -11px;
  right: 10px;
  width: 20px;
`;

const DropDownList = styled.ul.attrs({ className: 'drop-down-list' })`
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

const DropDownListItem = styled.li.attrs({ className: 'drop-down-list-item' })`
  margin: 0;
  padding: 0;
  cursor: pointer;
  height: 50px;
  margin-top: -20px;
`;

const DropDownListIcon = styled.img.attrs({
  className: 'drop-down-list-icon',
  alt: 'drop-down-list-icon',
})`
  margin: 0 10px 0 0;
  padding: 0;
  width: 16px;
  height: 16px;
  display: inline-block;
`;

class ProfileActionsDropdownMenu extends Component {
  handleClickOutside = () => {
    this.props.onClose();
  };

  render() {
    return (
      <DropdownMenu>
        <DropDownList>
          <DropDownListItem id="logout-button" onClick={this.props.onLogout}>
            <DropDownListIcon src={logoutIcon} />
            Logout
          </DropDownListItem>
        </DropDownList>
        <TopTriangle />
      </DropdownMenu>
    );
  }
}

ProfileActionsDropdownMenu.propTypes = {
  onLogout: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default onClickOutside(ProfileActionsDropdownMenu);
