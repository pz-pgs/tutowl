import React from 'react';
import styled from 'styled-components';
import gmailIcon from 'images/apps/gmail.svg';
import driveIcon from 'images/apps/google_drive.svg';
import officeIcon from 'images/apps/office.svg';

const IntegrateAppsWrapper = styled.div.attrs({ className: 'integrate-apps-wrapper' })`
  display: block;
  margin: 30px auto 0 auto;
  width: 70%;
  padding: 10px;
  border-radius: 5px;
  background: #e5f3fd;
  font-size: 12px;
  text-align: center;

  @media only screen and (max-width: 920px) {
    display: none;
  }
`;

const BoxTitle = styled.p.attrs({ className: 'box-title' })`
  font-size: 12px;
  font-weight: bold;
  color: #475b70;
`;

const IntegrationList = styled.ul.attrs({ className: 'integration-list' })`
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

const IntegrationListItem = styled.li.attrs({ className: 'integration-list-item' })`
  margin: 0 5px;
  padding: 0;
  display: inline-block;
`;

const IntegrationListItemImage = styled.img.attrs({
  className: 'integration-list-item-image',
  alt: 'integration-list-item-image',
})`
  margin: 0;
  padding: 0;
  width: 16px;
  height: 16px;
`;

const BoxSubTitle = styled.p.attrs({ className: 'box-sub-title' })`
  font-size: 10px;
  font-weight: 300;
  color: #475b70;
`;

const TryButton = styled.button.attrs({ className: 'try-button' })`
  cursor: pointer;
  border-radius: 5px;
  outline: none;
  color: #ffffff;
  padding: 10px;
  display: block;
  margin: 10px auto;
  border: 1px solid #f0f0f0;
  background: #2e4663;
  transition: 0.2s;

  &:hover {
    opacity: 0.8;
  }
`;

const APPS_LIST = [
  {
    id: 0,
    name: 'Google Drive',
    icon: driveIcon,
  },
  {
    id: 1,
    name: 'Gmail',
    icon: gmailIcon,
  },
  {
    id: 2,
    name: 'Office 365',
    icon: officeIcon,
  },
];

const IntegrateApps = () => (
  <IntegrateAppsWrapper>
    <BoxTitle>Integration Apps</BoxTitle>
    <BoxSubTitle>You can integrate several apps to accelerate productivity.</BoxSubTitle>
    <IntegrationList>
      {APPS_LIST.map(app => (
        <IntegrationListItem key={app.id}>
          <IntegrationListItemImage src={app.icon} />
        </IntegrationListItem>
      ))}
    </IntegrationList>

    <TryButton>Try integration</TryButton>
  </IntegrateAppsWrapper>
);

export default IntegrateApps;
