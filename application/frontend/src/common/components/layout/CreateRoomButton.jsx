import React, { Component } from 'react';
import styled from 'styled-components';
import createRoomIcon from 'images/icons/create_room.svg';
import CreateRoomForm from 'common/components/layout/CreateRoomForm';

const CreateRoomButtonWrapper = styled.button.attrs({ className: 'create-room-button-wrapper' })`
  color: #fff;
  background: #596b8f;
  border-radius: 5px;
  width: 150px;
  border: 1px solid #cedaf0;
  margin: 0;
  padding: 0px;
  font-weight: bold;
  line-height: 40px;
  transition: 0.2s;
  display: inline-block;
  float: left;
  margin-top: 15px;
  margin-right: 25px;
  outline: none;

  &:hover {
    cursor: pointer;
    background: #46648b;
  }

  @media only screen and (max-width: 575px) {
    width: 50px;
    margin-right: 15px;
  }
`;

const CreateRoomButtonText = styled.span.attrs({ className: 'create-room-button-text' })`
  height: 20px;
  line-height: 20px;
  display: inline-block;
  font-family: 'Roboto', sans-serif !important;
  font-weight: 800;
  font-size: 10px;

  @media only screen and (max-width: 575px) {
    display: none;
  }
`;

const CreateRoomButtonIcon = styled.img.attrs({
  className: 'create-room-button-icon',
  alt: 'create-room-button-icon',
})`
  display: inline-block;
  height: 20px;
  margin-bottom: -5px;
  margin-right: 5px;
  filter: invert(0.9);

  @media only screen and (max-width: 575px) {
    margin-right: 0px;
  }
`;

class CreateRoomButton extends Component {
  state = {
    isModalOpened: false,
  };

  createRoom = () => {
    this.setState({ isModalOpened: true });
  };

  abortPopUp = () => {
    this.setState({ isModalOpened: false });
  };

  render() {
    return (
      <div>
        <CreateRoomButtonWrapper onClick={this.createRoom}>
          <CreateRoomButtonIcon src={createRoomIcon} />
          <CreateRoomButtonText>Create room</CreateRoomButtonText>
        </CreateRoomButtonWrapper>
        {this.state.isModalOpened && <CreateRoomForm onClose={this.abortPopUp} />}
      </div>
    );
  }
}

export default CreateRoomButton;
