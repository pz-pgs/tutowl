import React, { Component } from 'react';
import styled from 'styled-components';
import SearchDropdown from 'common/components/search/SearchDropdown';
import searchIcon from 'images/icons/search_left.svg';
import onClickOutside from 'react-onclickoutside';
import clearIcon from 'images/icons/clear.svg';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { searchByQueryText } from 'common/actions/searchActions';

const SearchBarWrapper = styled.div.attrs({ className: 'search-bar-wrapper' })`
  position: relative;
  margin-left: 30px;
  margin-top: 13px;
  float: left;
  width: 40%;

  @media only screen and (max-width: 1120px) {
    width: 35%;
  }

  @media only screen and (max-width: 970px) {
    display: none;
  }
`;

const SearchBarInput = styled.input.attrs({ className: 'search-bar-input' })`
  border: 1px solid #f0f0f0;
  color: #000;
  background: #fff;
  padding: 15px 15px 15px 55px;
  outline: none;
  width: 80%;
  z-index: 100;
  font-size: 11px;

  @media only screen and (max-width: 1120px) {
    width: 70%;
  }
`;

const SearchBarIcon = styled.img.attrs({ className: 'search-bar-icon', alt: 'search-bar-icon' })`
  position: absolute;
  width: 20px;
  height: 20px;
  top: 12px;
  left: 25px;
`;

const SearchBarClearIcon = styled.img.attrs({
  className: 'search-bar-clear-icon',
  alt: 'search-bar-clear-icon',
})`
  position: absolute;
  width: 10px;
  top: 18px;
  cursor: pointer;
  transition: 0.2s;
  right: 40px;

  &:hover {
    opacity: 0.6;
  }
`;

const PLACEHOLDER = 'Find classes, lives and anything...';

class SearchBar extends Component {
  state = {
    searchStatus: '',
  };

  onClearClick = () => {
    this.setState({
      searchStatus: '',
    });
  };

  handleClickOutside = () => {
    this.setState({
      searchStatus: '',
    });
  };

  updateSearchBar = e => {
    this.setState(
      {
        searchStatus: e.target.value,
      },
      () => {
        this.props.searchMethod(this.state.searchStatus);
      },
    );
  };

  shouldShowDropdown = () => this.state.searchStatus.length > 0;

  render() {
    const { searchStatus } = this.state;
    const { isLoading, isError, searchResult } = this.props;

    return (
      <SearchBarWrapper>
        <SearchBarIcon src={searchIcon} />
        {this.shouldShowDropdown() ? (
          <SearchBarClearIcon src={clearIcon} onClick={this.onClearClick} />
        ) : null}
        <SearchBarInput
          autoComplete="off"
          style={this.shouldShowDropdown() ? { boxShadow: '0 3px 8px rgba(0, 0, 0, 0.25)' } : {}}
          aria-label="Search"
          id="search-bar-input"
          name="search-bar-input"
          onChange={this.updateSearchBar}
          text={searchStatus}
          value={this.state.searchStatus}
          placeholder={PLACEHOLDER}
        />
        {this.shouldShowDropdown() && (
          <SearchDropdown isLoading={isLoading} isError={isError} searchResult={searchResult} />
        )}
      </SearchBarWrapper>
    );
  }
}

SearchBar.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  searchMethod: PropTypes.func.isRequired,
  searchResult: PropTypes.instanceOf(Array).isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.common.search.isLoading,
  isError: state.common.search.isError,
  searchResult: state.common.search.searchResult,
});

const mapDispatchToProps = dispatch => ({
  searchMethod: searchQuery => dispatch(searchByQueryText(searchQuery)),
});

export default connect(mapStateToProps, mapDispatchToProps)(onClickOutside(SearchBar));
