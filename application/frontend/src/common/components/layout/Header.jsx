import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Colors from 'common/colors';
import RightMenu from 'common/components/layout/RightMenu';
import SearchBar from 'common/components/layout/SearchBar';

const HeaderWrapper = styled.div.attrs({ className: 'header-wrapper' })`
  grid-area: header-area;
  background: #fff;
  padding: 5px 15px;
  border-bottom: 1px solid ${Colors.GALLERY};
  text-align: center;
`;

const Header = ({ username, onLogout }) => (
  <HeaderWrapper>
    <SearchBar />
    <RightMenu onLogout={onLogout} username={username} />
  </HeaderWrapper>
);

Header.propTypes = {
  username: PropTypes.string.isRequired,
  onLogout: PropTypes.func.isRequired,
};

export default Header;
