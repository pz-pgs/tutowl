/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import RealmRolesService from 'common/authorization/RealmRolesService';
import NotificationDropdown from 'common/components/layout/NotificationDropdown';
import realmRoles from 'common/authorization/realmRoles';
import CurrentTime from 'common/components/layout/CurrentTime';
import ProfileCircle from 'common/components/layout/ProfileCircle';
import CreateRoomButton from 'common/components/layout/CreateRoomButton';
import VerticalLineRight from 'common/components/layout/VerticalLineRight';
import notificationIcon from 'images/icons/bell.svg';
import settingsIcon from 'images/icons/settings.svg';
import SockJsClient from 'react-stomp';
import {
  checkIfAnyNewNotifications,
  setNotificationsBell,
  addNewNotification,
  markAllNotificationsAsRead,
} from 'common/actions/notificationsActions';
import { connect } from 'react-redux';
import { store } from 'react-notifications-component';
import notificationSound from 'sounds/notification.mp3';

const RightMenuWrapper = styled.div.attrs({ className: 'right-menu-wrapper' })`
  float: right;
  display: inline-block;
  height: 70px;
  line-height: 80px;
  font-size: 14px;
  font-family: 'Titillium Web', sans-serif;
  margin-right: 40px;

  @media only screen and (max-width: 1120px) {
    float: none;
  }

  @media only screen and (max-width: 970px) {
    margin-right: 0;
  }
`;

const TopMenuWrapper = styled.ul.attrs({ className: 'top-menu-wrapper' })`
  margin: 0;
  padding: 0;
  list-style-type: none;
  display: inline-block;
  margin-right: 20px;
`;

const TopMenuItem = styled.li.attrs({ className: 'top-menu-item' })`
  margin: 0;
  padding: 0;
  cursor: pointer;
  margin-left: 15px;
  display: inline-block;
  position: relative;
`;

const NewNotifications = styled.div.attrs({ className: 'new-notifications' })`
  width: 8px;
  height: 8px;
  background: #2d4564;
  position: absolute;
  top: 24px;
  left: 11px;
  border-radius: 50%;
`;

const CreateRoomButtonWrapper = styled.div.attrs({ className: 'create-room-button-wrapper' })`
  display: inline-block;
  float: left;
`;

const TopMenuImg = styled.img.attrs({ className: 'top-menu-img', alt: 'top-menu-img' })`
  margin: 0;
  width: 20px;
  height: 20px;
`;

class RightMenu extends Component {
  state = {
    showNewNotoficationsDropDown: false,
    addedNotifications: 0,
    originalPageTitle: document.title,
    audio: new Audio(notificationSound),
  };

  componentDidMount() {
    if (!this.props.notificationsFetched) this.props.checkIfAnyNewNotificationsFunc();
  }

  componentDidUpdate() {
    const { addedNotifications, originalPageTitle } = this.state;
    if (addedNotifications > 0) {
      document.title = `(${addedNotifications}) | ${originalPageTitle}`;
    } else {
      document.title = originalPageTitle;
    }
  }

  onNotificationsClick = () => {
    this.props.setNotificationsBellFunc(false);
    this.setState({
      addedNotifications: 0,
    });

    if (this.props.notificationsFetched) {
      this.props.markAllNotificationsAsReadFunc();
    }

    this.setState({
      showNewNotoficationsDropDown: !this.state.showNewNotoficationsDropDown,
    });
  };

  onMessageReceive = message => {
    if (message.author !== this.props.currentUsername) {
      this.setState({
        addedNotifications: this.state.addedNotifications + 1,
      });

      this.state.audio.play();
      this.props.setNotificationsBellFunc(true);
      this.createNotification(message);

      if (this.props.notificationsFetched) {
        this.props.addNewNotificationFunc(message);
      }
    }
  };

  createNotification = message => {
    store.addNotification({
      message: `${message.author} ${message.content}`,
      type: 'info',
      insert: 'top',
      container: 'bottom-right',
      animationIn: ['animated', 'fadeIn'],
      animationOut: ['animated', 'fadeOut'],
      dismiss: {
        duration: 5000,
        onScreen: true,
        pauseOnHover: true,
      },
    });
  };

  showCreateRoomButton = () =>
    RealmRolesService.hasAnyRole([realmRoles.ROLE_LIVE_ADMIN, realmRoles.ROLE_LIVE_TEACHER]);

  render() {
    const wsSourceUrl = `${window.location.protocol}//${window.location.host}/api/notifications/ws`;
    const { showNewNotoficationsDropDown } = this.state;

    return (
      <RightMenuWrapper>
        {this.showCreateRoomButton() ? (
          <CreateRoomButtonWrapper>
            <CreateRoomButton />
          </CreateRoomButtonWrapper>
        ) : null}
        <TopMenuWrapper>
          <TopMenuItem>
            <TopMenuImg
              src={notificationIcon}
              onClick={showNewNotoficationsDropDown ? null : this.onNotificationsClick}
            />
            {this.props.newNotifications && <NewNotifications />}
            {showNewNotoficationsDropDown && (
              <NotificationDropdown onOutsideClick={this.onNotificationsClick} />
            )}
          </TopMenuItem>
          <TopMenuItem>
            <Link to="/settings">
              <TopMenuImg src={settingsIcon} />
            </Link>
          </TopMenuItem>
        </TopMenuWrapper>
        <ProfileCircle onLogout={this.props.onLogout} username={this.props.username} />
        <VerticalLineRight />
        <CurrentTime />
        <VerticalLineRight />

        <SockJsClient
          url={wsSourceUrl}
          topics={['/notify']}
          onMessage={this.onMessageReceive}
          ref={client => {
            this.clientRef = client;
          }}
          autoReconnect={false}
        />
      </RightMenuWrapper>
    );
  }
}

RightMenu.propTypes = {
  onLogout: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
  currentUsername: PropTypes.string.isRequired,
  newNotifications: PropTypes.bool.isRequired,
  notificationsFetched: PropTypes.bool.isRequired,
  checkIfAnyNewNotificationsFunc: PropTypes.func.isRequired,
  setNotificationsBellFunc: PropTypes.func.isRequired,
  addNewNotificationFunc: PropTypes.func.isRequired,
  markAllNotificationsAsReadFunc: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  currentUsername: state.common.authUser.keycloakInfo.userInfo.preferred_username,
  newNotifications: state.common.notifications.newNotifications,
  notificationsFetched: state.common.notifications.notificationsFetched,
});

const mapDispatchToProps = dispatch => ({
  checkIfAnyNewNotificationsFunc: () => dispatch(checkIfAnyNewNotifications()),
  markAllNotificationsAsReadFunc: () => dispatch(markAllNotificationsAsRead()),
  setNotificationsBellFunc: newNotifications => dispatch(setNotificationsBell(newNotifications)),
  addNewNotificationFunc: notification => dispatch(addNewNotification(notification)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RightMenu);
