import React, { Component } from 'react';
import styled from 'styled-components';
import arrowIcon from 'images/icons/double-arrow-2.svg';
import PropTypes from 'prop-types';
import shortid from 'shortid';

const SelectInputBox = styled.div.attrs({ className: 'select-input-box' })`
  position: relative;
  margin-bottom: 15px;
`;

const SelectInputComponent = styled.select.attrs({ className: 'select-input-component' })`
  outline: none;
  background: none;
  border-radius: 4px;
  border: 1px solid #f0f0f0;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  padding: 10px;
  background: #fcfdfe;
  width: 95%;

  &:focus {
    border: 1px solid #6c89e6;
  }
`;

const SelectInputText = styled.p.attrs({ className: 'text-input-text' })`
  margin: 0 0 5px 0;
  font-size: 14px;
  color: #59637d;
  font-family: 'Titillium Web', sans-serif;
`;

const ArrowIcon = styled.img.attrs({ className: 'arrow-icon-select', alt: 'arrow-icon-select' })`
  width: 12px;
  height: 12px;
  position: absolute;
  right: 20px;
  bottom: 10px;
  filter: invert(0.5);
`;

class SelectInput extends Component {
  state = {
    choosenOption: '',
  };

  onChange = event => {
    const {
      target: { value },
    } = event;

    this.setState({
      choosenOption: value,
    });

    this.props.onChange(value);
  };

  toCamelCase = inputArray => {
    let result = '';

    for (let i = 0, len = inputArray.length; i < len; i += 1) {
      let currentStr = inputArray[i];

      if (i === 0) currentStr = currentStr.toUpperCase();
      else currentStr = currentStr.toLowerCase();

      result += currentStr;
    }

    return result;
  };

  renderSelect = (id, options) => (
    <SelectInputComponent onChange={this.onChange} id={id} value={this.state.choosenOption}>
      {options.map(option => (
        <option value={option} key={shortid.generate()}>
          {this.toCamelCase(option.toLowerCase())}
        </option>
      ))}
    </SelectInputComponent>
  );

  render() {
    const { label, id, options } = this.props;

    return (
      <SelectInputBox>
        <SelectInputText>{label}</SelectInputText>
        {this.renderSelect(id, options)}
        <ArrowIcon src={arrowIcon} />
      </SelectInputBox>
    );
  }
}

SelectInput.propTypes = {
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  options: PropTypes.instanceOf(Array).isRequired,
  onChange: PropTypes.func.isRequired,
};

export default SelectInput;
