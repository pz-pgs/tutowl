export const BASE_PATH = '/';
export const LOGOUT_PATH = '/logout';
export const ROOM_PATTERN_PATH = '/room/:roomId';
export const INTERNAL_SERVER_ERROR_PATH = '/internal-error';
