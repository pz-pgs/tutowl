import RequestService from 'common/services/RequestService';

export const fetchAllNotifications = () => RequestService.get(`/api/notifications/`);

export const checkINewNotifications = () => RequestService.get(`/api/notifications/new`);

export const markNotificationsAsRead = () => RequestService.post(`/api/notifications/read`);
