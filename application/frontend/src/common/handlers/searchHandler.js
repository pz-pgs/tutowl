import RequestService from 'common/services/RequestService';

export default searchQuery => RequestService.get(`/api/search?searchQuery=${searchQuery}`);
