import {
  FETCH_MESSAGES_COUNT_PENDING,
  FETCH_MESSAGES_COUNT_OK,
  FETCH_MESSAGES_COUNT_FAIL,
  INCREASE_MESSAGES_COUNT,
} from 'common/actions/newMessagesActions';

export const MESSAGES_COUNT_INITIAL_STATE = {
  isError: false,
  isLoading: false,
  messagesCount: 0,
};

export default (state, { type, payload }) => {
  const stateDefinition = typeof state === 'undefined' ? MESSAGES_COUNT_INITIAL_STATE : state;
  switch (type) {
    case FETCH_MESSAGES_COUNT_PENDING:
      return {
        ...stateDefinition,
        isError: false,
        isLoading: true,
        messagesCount: 0,
      };
    case FETCH_MESSAGES_COUNT_OK:
      return {
        ...stateDefinition,
        isError: false,
        isLoading: false,
        messagesCount: payload.messagesCount,
      };
    case FETCH_MESSAGES_COUNT_FAIL:
      return {
        ...stateDefinition,
        isError: true,
        isLoading: false,
        messagesCount: 0,
      };
    case INCREASE_MESSAGES_COUNT:
      return {
        ...stateDefinition,
        messagesCount: stateDefinition.messagesCount + 1,
      };
    default:
      return stateDefinition;
  }
};
