import { SEARCH_PENDING, SEARCH_OK, SEARCH_FAIL } from 'common/actions/searchActions';

export const CREATE_ROOM_INITIAL_STATE = {
  isError: false,
  isLoading: false,
  searchResult: [],
};

export default (state, { type, payload }) => {
  const stateDefinition = typeof state === 'undefined' ? CREATE_ROOM_INITIAL_STATE : state;
  switch (type) {
    case SEARCH_PENDING:
      return {
        ...stateDefinition,
        isError: false,
        isLoading: true,
        searchResult: [],
      };
    case SEARCH_OK:
      return {
        ...stateDefinition,
        isLoading: false,
        isError: false,
        searchResult: payload.searchResult,
      };
    case SEARCH_FAIL:
      return { ...stateDefinition, isLoading: false, isError: true, searchResult: [] };
    default:
      return stateDefinition;
  }
};
