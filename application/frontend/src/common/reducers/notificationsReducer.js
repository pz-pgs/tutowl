import {
  FETCH_NOTIFICATIONS_PENDING,
  FETCH_NOTIFICATIONS_OK,
  FETCH_NOTIFICATIONS_FAIL,
  ADD_NEW_NOTIFICATION,
  NOTIFICATIONS_CLICKED,
  SET_NOTIFICATION_BELL,
  FETCH_IF_NEW_NOTIFICATIONS_PENDING,
  FETCH_IF_NEW_NOTIFICATIONS_OK,
  FETCH_IF_NEW_NOTIFICATIONS_FAIL,
  ALL_NOTIFICATIONS_READ,
} from 'common/actions/notificationsActions';

export const NOTIFICATIONS_INITIAL_STATE = {
  isError: false,
  isLoading: false,
  newNotifications: false,
  notificationsList: [],
  checkNewNotificationsLoading: false,
  checkNewNotificationsError: false,
  notificationsFetched: false,
};

export default (state, { type, payload }) => {
  const stateDefinition = typeof state === 'undefined' ? NOTIFICATIONS_INITIAL_STATE : state;
  switch (type) {
    case FETCH_NOTIFICATIONS_PENDING:
      return {
        ...stateDefinition,
        isLoading: true,
        isError: false,
        notificationsList: [],
      };
    case FETCH_NOTIFICATIONS_OK:
      return {
        ...stateDefinition,
        isLoading: false,
        isError: false,
        notificationsList: payload.notificationsList,
        newNotifications: false,
        notificationsFetched: true,
      };
    case FETCH_NOTIFICATIONS_FAIL:
      return { ...stateDefinition, isLoading: false, isError: true, notificationsList: [] };
    case ADD_NEW_NOTIFICATION:
      return {
        ...stateDefinition,
        notificationsList: [payload.newNotification, ...stateDefinition.notificationsList],
        newNotification: true,
      };
    case NOTIFICATIONS_CLICKED:
      return {
        ...stateDefinition,
        newNotification: false,
      };
    case SET_NOTIFICATION_BELL:
      return {
        ...stateDefinition,
        newNotifications: payload.newNotifications,
      };
    case FETCH_IF_NEW_NOTIFICATIONS_PENDING:
      return {
        ...stateDefinition,
        checkNewNotificationsError: false,
        checkNewNotificationsLoading: true,
      };
    case FETCH_IF_NEW_NOTIFICATIONS_OK:
      return {
        ...stateDefinition,
        checkNewNotificationsError: false,
        checkNewNotificationsLoading: false,
        newNotifications: payload.newNotifications,
      };
    case FETCH_IF_NEW_NOTIFICATIONS_FAIL:
      return {
        ...stateDefinition,
        checkNewNotificationsError: true,
        checkNewNotificationsLoading: false,
      };
    case ALL_NOTIFICATIONS_READ:
      return {
        ...stateDefinition,
        notificationsList: stateDefinition.notificationsList.map(item =>
          item.read === false ? { ...item, read: true } : item,
        ),
      };
    default:
      return stateDefinition;
  }
};
