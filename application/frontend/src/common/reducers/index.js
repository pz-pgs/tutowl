import { combineReducers } from 'redux';
import lockScreenReducer from 'common/reducers/lockScreenReducer';
import authUserReducer from 'common/reducers/authUserReducer';
import createRoomReducer from 'common/reducers/createRoomReducer';
import searchReducer from 'common/reducers/searchReducer';
import newMessagesReducer from 'common/reducers/newMessagesReducer';
import notificationsReducer from 'common/reducers/notificationsReducer';

export default combineReducers({
  applicationScreen: lockScreenReducer,
  authUser: authUserReducer,
  createRoom: createRoomReducer,
  search: searchReducer,
  messagesSidebar: newMessagesReducer,
  notifications: notificationsReducer,
});
