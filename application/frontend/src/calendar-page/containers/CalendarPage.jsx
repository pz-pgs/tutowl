import React from 'react';
import styled from 'styled-components';
import BigCalendarComponent from 'calendar-page/components/BigCalendarComponent';

const CalendarPageWrapper = styled.div.attrs({ className: 'calendar-page-wrapper' })`
  padding: 30px 50px 10px 50px;
`;

const CalendarPage = () => (
  <CalendarPageWrapper>
    <BigCalendarComponent />
  </CalendarPageWrapper>
);

export default CalendarPage;
