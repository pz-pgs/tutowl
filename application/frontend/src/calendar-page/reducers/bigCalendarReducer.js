import {
  GET_CALENDAR_EVENTS_PENDING,
  GET_CALENDAR_EVENTS_OK,
  GET_CALENDAR_EVENTS_FAIL,
} from 'calendar-page/actions/bigCalendarActions';

const INITIAL_STATE = {
  isLoading: false,
  isError: false,
  events: [],
};

export default (state, { type, payload }) => {
  const stateDefinition = typeof state === 'undefined' ? INITIAL_STATE : state;
  switch (type) {
    case GET_CALENDAR_EVENTS_PENDING:
      return { ...stateDefinition, isLoading: true, isError: false, events: [] };
    case GET_CALENDAR_EVENTS_OK:
      return {
        ...stateDefinition,
        isLoading: false,
        isError: false,
        events: payload.events,
      };
    case GET_CALENDAR_EVENTS_FAIL:
      return { ...stateDefinition, isLoading: false, isError: true, events: [] };
    default:
      return stateDefinition;
  }
};
