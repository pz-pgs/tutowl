import { combineReducers } from 'redux';
import bigCalendarReducer from 'calendar-page/reducers/bigCalendarReducer';

export default combineReducers({
  calendarEvents: bigCalendarReducer,
});
