/* eslint-disable prefer-template */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styled from 'styled-components';
import GenericModal from 'common/components/GenericModal';
import { getRoomDetails } from 'room-page/actions/roomDetailsActions';
import withLoading, { ProgIndSize } from 'common/utils/withLoading';
import Colors from 'common/colors';
import schoolImage from 'images/school_tables.svg';
import { Link } from 'react-router-dom';

const LoadingWrapper = styled.div.attrs({ className: 'loading-wrapper' })``;

const GridWrapper = styled.div.attrs({ className: 'loading-wrapper' })`
  display: grid;
  grid-template-columns: 3fr 4fr;
  grid-template-areas: 'event-info event-image';
`;

const EventInfo = styled.div.attrs({ className: 'event-info' })`
  grid: event-info;
`;
const EventImage = styled.div.attrs({ className: 'event-image' })`
  grid: event-image;
`;

const LoadingWrapperWithLoading = withLoading(LoadingWrapper, ProgIndSize.XX_LARGE);

const ModalTitle = styled.p.attrs({ className: 'modal-title' })`
  font-size: 22px;
  margin: 0 0 15px 0;
  font-weight: bold;
`;

const ModalImage = styled.img.attrs({ className: 'modal-image' })`
  height: 200px;
`;

const RoomDetail = styled.div.attrs({ className: 'room-detail' })`
  font-size: 13px;
  font-family: 'Roboto', sans-serif !important;
  margin-bottom: 15px;
`;

const RoomDetailName = styled.span.attrs({ className: 'room-detail-name' })`
  font-weight: bold;
  margin-right: 5px;
`;

const RoomDetailContent = styled.span.attrs({ className: 'room-detail-content' })`
  font-weight: 100;
`;

const RoomDetailContentBig = styled.p.attrs({ className: 'room-detail-content-big' })`
  font-weight: 100;
  margin: 5px 0 0 0;
`;

const ParticipantsWrapper = styled.div.attrs({ className: 'participants-wrapper' })`
  display: block;
  margin: 10px 0 0 10px;
`;

const ParticipantItem = styled.div.attrs({ className: 'participant-item' })`
  display: inline-block;
  border-radius: 50%;
  width: 32px;
  height: 32px;
  line-height: 32px;
  text-align: center;
  color: ${Colors.WHITE};
  margin-left: -8px;
  border: 1px solid #f0f0f0;
  cursor: pointer;
  transition: 0.2s;

  &:hover {
    opacity: 0.7;
  }
`;

const GoToButton = styled.button.attrs({ className: 'go-to-button' })`
  padding: 10px 0;
  font-weight: 100;
  font-size: 13px;
  background: #2d4564;
  color: #fff;
  text-align: center;
  cursor: pointer;
  border-radius: 4px;
  border: 1px solid #f0f0f0;
  width: 100%;
  transition: 0.2s;

  &:hover {
    opacity: 0.8;
  }
`;

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

class CalendarEventModal extends Component {
  componentWillMount() {
    const { event } = this.props;
    this.props.getRoomDetailsFunc(event.id);
  }

  getFirstCharAndUpperCase = string => string.charAt(0).toUpperCase();

  randomInRange = (min, max) => Math.floor(Math.random() * (max - min) + min);

  addZeroBefore = n => (n < 10 ? '0' : '') + n;

  pickRandomBackground = () => {
    const randomNumber = this.randomInRange(0, 4);

    switch (randomNumber) {
      case 0:
        return Colors.LIMEADE;
      case 1:
        return Colors.MATISSE;
      case 2:
        return Colors.ORANGE;
      case 3:
        return Colors.SCIENCE_BLUE;
      case 4:
        return Colors.TOREA_BAY;
      default:
        return Colors.BURGUNDY;
    }
  };

  renderParticipants = roomData => {
    if (roomData.participants !== undefined) {
      if (roomData.participants.length > 0) {
        return (
          <ParticipantsWrapper>
            {roomData.participants.map(user => (
              <ParticipantItem key={user} style={{ background: this.pickRandomBackground() }}>
                {this.getFirstCharAndUpperCase(user)}
              </ParticipantItem>
            ))}
          </ParticipantsWrapper>
        );
      }
    }
    return null;
  };

  renderEventDate = (eventStartDate, eventEndDate) => {
    const eventStartDateObject = new Date(eventStartDate);
    const eventEndDateObject = new Date(eventEndDate);

    return (
      this.addZeroBefore(eventStartDateObject.getHours()) +
      ':' +
      this.addZeroBefore(eventStartDateObject.getMinutes()) +
      ' - ' +
      this.addZeroBefore(eventEndDateObject.getHours()) +
      ':' +
      this.addZeroBefore(eventEndDateObject.getMinutes()) +
      ', ' +
      this.addZeroBefore(eventStartDateObject.getDate()) +
      ' ' +
      monthNames[eventStartDateObject.getMonth()] +
      ' ' +
      eventStartDateObject.getFullYear()
    );
  };

  render() {
    const { isRoomLoading, isRoomError, roomData, onClose } = this.props;

    if (isRoomError) {
      return 'nie dziala';
    }

    return (
      <GenericModal onClose={onClose}>
        <LoadingWrapperWithLoading isLoading={isRoomLoading}>
          <ModalTitle>Room details</ModalTitle>
          <GridWrapper>
            <EventInfo>
              <RoomDetail>
                <RoomDetailName>Name:</RoomDetailName>
                <RoomDetailContent>{roomData.name}</RoomDetailContent>
              </RoomDetail>
              <RoomDetail>
                <RoomDetailName>Date:</RoomDetailName>
                <RoomDetailContent>
                  {this.renderEventDate(roomData.event_start_time, roomData.event_end_time)}
                </RoomDetailContent>
              </RoomDetail>
              <RoomDetail>
                <RoomDetailName>Category:</RoomDetailName>
                <RoomDetailContent>{roomData.category}</RoomDetailContent>
              </RoomDetail>
              <RoomDetail>
                <RoomDetailName>Owner:</RoomDetailName>
                <RoomDetailContent>{roomData.owner}</RoomDetailContent>
              </RoomDetail>
              <RoomDetail>
                <RoomDetailName>Description:</RoomDetailName>
                <RoomDetailContentBig>{roomData.description}</RoomDetailContentBig>
              </RoomDetail>
              <RoomDetail>
                <RoomDetailName>Participants:</RoomDetailName>
                {this.renderParticipants(roomData)}
              </RoomDetail>
            </EventInfo>
            <EventImage>
              <ModalImage src={schoolImage} alt="schoolImage" />
            </EventImage>
          </GridWrapper>
          <Link to={`/room/${roomData.id}`}>
            <GoToButton>View</GoToButton>
          </Link>
        </LoadingWrapperWithLoading>
      </GenericModal>
    );
  }
}

CalendarEventModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  event: PropTypes.instanceOf(Object).isRequired,
  roomData: PropTypes.instanceOf(Object).isRequired,
  isRoomLoading: PropTypes.bool.isRequired,
  isRoomError: PropTypes.bool.isRequired,
  getRoomDetailsFunc: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isRoomLoading: state.session.roomDetails.isLoading,
  isRoomError: state.session.roomDetails.isError,
  roomData: state.session.roomDetails.roomData,
});

const mapDispatchToProps = dispatch => ({
  getRoomDetailsFunc: roomId => dispatch(getRoomDetails(roomId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CalendarEventModal);
