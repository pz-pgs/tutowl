import React from 'react';
import styled from 'styled-components';
import Colors from 'common/colors';
import UserInfoHeader from 'main-page/components/UserInfoHeader';
import UpcomingEvents from 'main-page/components/UpcomingEvents';

const UserInfoWrapper = styled.div.attrs({ className: 'user-info-wrapper' })`
  background: ${Colors.WHITE};
`;

const CalendarUserInfo = () => (
  <UserInfoWrapper>
    <UserInfoHeader />
    <UpcomingEvents />
  </UserInfoWrapper>
);

export default CalendarUserInfo;
