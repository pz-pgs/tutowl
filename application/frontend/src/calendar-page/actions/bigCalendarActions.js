import getCalendarEvents from 'calendar-page/handlers/bigCalendarHandler';

export const GET_CALENDAR_EVENTS_PENDING = 'GET_CALENDAR_EVENTS_PENDING';
export const GET_CALENDAR_EVENTS_OK = 'GET_CALENDAR_EVENTS_OK';
export const GET_CALENDAR_EVENTS_FAIL = 'GET_CALENDAR_EVENTS_FAIL';

export const makeGetCalendarEventsPending = () => ({
  type: GET_CALENDAR_EVENTS_PENDING,
});

export const makeGetCalendarEventsOk = events => ({
  type: GET_CALENDAR_EVENTS_OK,
  payload: { events },
});

export const makeGetCalendarEventsFail = () => ({
  type: GET_CALENDAR_EVENTS_FAIL,
});

export const fetchCalendarEvents = () => dispatch => {
  dispatch(makeGetCalendarEventsPending());

  return getCalendarEvents()
    .then(res => {
      dispatch(makeGetCalendarEventsOk(res.data));
    })
    .catch(() => {
      dispatch(makeGetCalendarEventsFail());
    });
};
