import getTimeTableEvents from 'main-page/handlers/timeTableHandler';

export const TIME_TABLE_PENDING = 'TIME_TABLE_PENDING';
export const TIME_TABLE_OK = 'TIME_TABLE_OK';
export const TIME_TABLE_FAIL = 'TIME_TABLE_PENDING';

export const makeTimeTablePending = () => ({
  type: TIME_TABLE_PENDING,
});

export const makeTimeTableOk = timeTableData => ({
  type: TIME_TABLE_OK,
  payload: { timeTableData },
});

export const makeTimeTableFail = () => ({
  type: TIME_TABLE_FAIL,
});

export const fetchTimeTableEvents = () => dispatch => {
  dispatch(makeTimeTablePending());

  return getTimeTableEvents()
    .then(res => {
      dispatch(makeTimeTableOk(res.data));
    })
    .catch(() => {
      dispatch(makeTimeTableFail());
    });
};
