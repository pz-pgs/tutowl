/* eslint-disable react/no-array-index-key */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-unused-vars */
/* eslint-disable prefer-template */
import React, { Component } from 'react';
import styled from 'styled-components';
import chemistryIcon from 'images/icons/chemistry_icon.svg';
import peIcon from 'images/icons/pe_icon.svg';
import geographyIcon from 'images/icons/geography_icon.svg';
import biologyIcon from 'images/icons/biology_icon.svg';
import timeIcon from 'images/icons/event_date_icon.svg';
import shortid from 'shortid';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchTimeTableEvents } from 'main-page/actions/timeTableActions';
import { Link } from 'react-router-dom';
import liveIcon from 'images/icons/live.svg';
import ProgressIndicatorCircular from 'common/components/ProgressIndicatorCircular';

const ScheduleAreaWrapper = styled.div.attrs({ className: 'schedule-area-wrapper' })`
  grid: schedule-area;
  background: #ffffff;
  padding: 20px 30px;
  border: 1px solid #f0f0f0;
  overflow: hidden;
  height: 80%;
  position: relative;
`;

const StyledLink = styled(Link)`
  text-decoration: none;
  color: #333;
`;

const CardTitle = styled.p.attrs({ className: 'card-title' })`
  font-size: 16px;
  font-family: 'Heebo', sans-serif;
  margin: 0 0 5px 0;
  font-weight: 100;
`;

const TimetableGrid = styled.div.attrs({ className: 'timetable-grid' })`
  display: grid;
  grid-template-areas: 'hours-area lectures-area';
  grid-template-columns: 1fr 9fr;
  margin-top: 20px;
`;

const TimetableHours = styled.ul.attrs({ className: 'hours-area' })`
  grid: hours-area;
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

const TimeStripesList = styled.ul.attrs({ className: 'time-stripes-list' })`
  list-style-type: none;
  margin: 0;
  padding: 0;
  width: 100%;
`;

const TimeStripesListItem = styled.li.attrs({ className: 'time-stripes-list-item' })`
  list-style-type: none;
  height: 40px;
  width: 100%;
  border-top: 1px dashed #e0e0e0;
`;

const TimetableHoursItem = styled.li.attrs({ className: 'hours-area-item' })`
  grid: hours-area;
  color: #333;
  font-size: 10px;
  margin-bottom: 30px;
`;

const TimetableLectures = styled.div.attrs({ className: 'lectures-area' })`
  grid: lectures-area;
  position: relative;
`;

const TimeTableLecturesList = styled.ul.attrs({ className: 'lectures-area-list' })`
  list-style-type: none;
  margin: 0;
  padding: 20px;
  position: relative;
  height: 100%;
  width: 100%;
`;

const ActualTimeLineBox = styled.div.attrs({ className: 'actual-time-line-box' })`
  position: absolute;
  left: 7%;
  width: 95%;
`;

const ActualTimeLine = styled.div.attrs({ className: 'actual-time-line' })`
  width: 100%;
  border-bottom: 1px dashed #2f4762;
  position: relative;
  margin-bottom: 30px;
  z-index: 3;
`;

const TimeDot = styled.div.attrs({ className: 'actual-time-line' })`
  position: absolute;
  left: -10px;
  top: -10px;
  width: 14px;
  height: 14px;
  border-radius: 50%;
  background: #2f4762;
  border: 3px solid #e6eff6;
`;

const TimetableLecturesItem = styled.li.attrs({ className: 'lectures-area-item' })`
  grid: lectures-area;
  border-left: 4px solid #2f4762;
  padding: 0 20px;
  position: absolute;
  width: 70%;
`;

const TimetableLecturesItemBox = styled.div.attrs({ className: 'lectures-area-item-box' })`
  grid: lectures-area;
  padding: 10px;
  width: 100%;
  font-size: 12px;
  background: #f6f7fb;
  border: 1px solid #f0f0f0;
  border-radius: 6px;
  height: calc(100% - 20px);
`;

const SubjectGrid = styled.div.attrs({ className: 'subject-grid' })`
  display: grid;
  grid-template-areas: 'subject-image-area subject-details-area';
  grid-template-columns: 1fr 4fr;
`;

const LiveIcon = styled.img.attrs({ className: 'live-icon' })`
  float: right;
  height: 25px;
`;

const TimeStripes = styled.div.attrs({ className: 'time-stripes' })`
  position: absolute;
  top: 0;
  left: 0;
  width: 95%;
  padding: 7px 20px 20px 20px;
`;

const SubjectImageArea = styled.div.attrs({ className: 'subject-image-area' })`
  grid: subject-image-area;
`;

const SubjectDetailsArea = styled.div.attrs({ className: 'subject-details-area' })`
  grid: subject-details-area;
`;

const SubjectImageAreaItem = styled.img.attrs({
  className: 'subject-image-area-item',
  alt: 'subject-image-area-item',
})`
  width: 30px;
`;

const SubjectDateIcon = styled.img.attrs({
  className: 'subject-date-icon',
  alt: 'subject-date-icon',
})`
  width: 10px;
  margin-right: 5px;
`;

const SubjectName = styled.p.attrs({ className: 'subject-name' })`
  font-weight: bold;
  font-size: 13px;
  margin: 0;
  font-family: 'Heebo', sans-serif;
  font-weight: 100;
`;

const SubjectDate = styled.p.attrs({ className: 'subject-date' })`
  font-weight: 100;
  margin: 0;
  font-family: 'Heebo', sans-serif;
  font-weight: 100;
  font-size: 10px;
`;

const ErrorBlock = styled.div.attrs({ className: 'error-blok' })`
  padding: 10px;
  text-align: center;
  background: #fce7e6;
  font-size: 12px;
  color: #552526;
  width: 40%;
  font-weight: bold;
  margin: 0 auto 10px auto;
`;

let TIME_ARRAY = [];

const pastLessonStyle = {
  opacity: '0.55',
  borderLeftColor: '#fff',
};

class ScheduleArea extends Component {
  componentWillMount() {
    const currentDate = new Date();

    const currentHour = currentDate.getHours();
    TIME_ARRAY = [
      this.addZeroBefore(this.checkIfGoodHour(currentHour - 2)) + ':00',
      this.addZeroBefore(this.checkIfGoodHour(currentHour - 2)) + ':30',
      this.addZeroBefore(this.checkIfGoodHour(currentHour - 1)) + ':00',
      this.addZeroBefore(this.checkIfGoodHour(currentHour - 1)) + ':30',
      this.addZeroBefore(currentHour) + ':00',
      this.addZeroBefore(currentHour) + ':30',
      this.addZeroBefore(this.checkIfGoodHour(currentHour + 1)) + ':00',
      this.addZeroBefore(this.checkIfGoodHour(currentHour + 1)) + ':30',
      this.addZeroBefore(this.checkIfGoodHour(currentHour + 2)) + ':00',
      this.addZeroBefore(this.checkIfGoodHour(currentHour + 2)) + ':30',
    ];
  }

  componentDidMount() {
    this.props.fetchTimeTableEventsFunc();
  }

  chooseTimeLinePosition = () => {
    const currentDate = new Date();

    const lineHour = new Date(currentDate);
    lineHour.setHours(currentDate.getHours() - 2);
    lineHour.setMinutes(0);

    const endValue = currentDate - lineHour;

    const hoursPassed = endValue / 1000 / 60 / 60;
    const topValue = ((hoursPassed - 0.2) / 4.5) * 100;

    return { top: `${topValue}%` };
  };

  chooseEventPosition = (startDate, endDate) => {
    // Top position
    const currentDate = new Date();

    const lowHour = new Date(currentDate);
    lowHour.setHours(currentDate.getHours() - 2);
    lowHour.setMinutes(0);

    const endValue = new Date(startDate) - lowHour;

    const hoursPassed = endValue / 1000 / 60 / 60;
    const topValue = ((hoursPassed - 0.2) / 4.5) * 100;

    // Height
    const startDateObject = new Date(startDate);
    const endDateObject = new Date(endDate);

    const endValueHeight = endDateObject - startDateObject;

    const hoursPassedHeight = endValueHeight / 1000 / 60;
    const topValueHeight = ((hoursPassedHeight - 0.2) / 270) * 100;

    return { top: `${topValue}%`, height: `${topValueHeight}%` };
  };

  stylePastLesson = isFinished => (isFinished ? pastLessonStyle : {});

  chooseCategoryIcon = category => {
    switch (category) {
      case 'CHEMISTRY':
        return chemistryIcon;
      case 'PHYSICS':
        return peIcon;
      case 'MATH':
        return biologyIcon;
      case 'GEOGRAPHY':
        return geographyIcon;
      default:
        return null;
    }
  };

  addZeroBefore = n => (n < 10 ? '0' : '') + n;

  showLiveIcon = (startDate, endDate) => {
    const startDateObj = new Date(startDate);
    const nowDateObj = new Date();
    const endDateObj = new Date(endDate);

    return nowDateObj > startDateObj && nowDateObj < endDateObj;
  };

  checkIfGoodHour = hour => (hour > 23 ? hour - 24 : hour < 0 ? hour + 24 : hour);

  renderEventDate = (eventStartDate, eventEndDate) => {
    const eventStartDateObject = new Date(eventStartDate);
    const eventEndDateObject = new Date(eventEndDate);

    return (
      this.addZeroBefore(eventStartDateObject.getHours()) +
      ':' +
      this.addZeroBefore(eventStartDateObject.getMinutes()) +
      ' - ' +
      this.addZeroBefore(eventEndDateObject.getHours()) +
      ':' +
      this.addZeroBefore(eventEndDateObject.getMinutes())
    );
  };

  renderTimeStripes = () => (
    <TimeStripesList>
      {Array(10)
        .fill()
        .map((_, i) => (
          <TimeStripesListItem key={shortid.generate()} />
        ))}
    </TimeStripesList>
  );

  render() {
    const { timeTableData, isLoading, isError } = this.props;

    if (isLoading)
      return (
        <ScheduleAreaWrapper>
          <ProgressIndicatorCircular size={40} />
        </ScheduleAreaWrapper>
      );

    if (isError) {
      return (
        <ScheduleAreaWrapper>
          <ErrorBlock>There was an error during TimeTable fetching.</ErrorBlock>
        </ScheduleAreaWrapper>
      );
    }

    return (
      <ScheduleAreaWrapper>
        <CardTitle>Time table</CardTitle>
        <TimetableGrid>
          <TimetableHours>
            {TIME_ARRAY.map(time => (
              <TimetableHoursItem key={time}>{time}</TimetableHoursItem>
            ))}
          </TimetableHours>
          <TimetableLectures>
            <TimeStripes>{this.renderTimeStripes()}</TimeStripes>
            <ActualTimeLineBox style={this.chooseTimeLinePosition()}>
              <ActualTimeLine>
                <TimeDot />
              </ActualTimeLine>
            </ActualTimeLineBox>

            <TimeTableLecturesList>
              {timeTableData.map(lesson => (
                <TimetableLecturesItem
                  style={{
                    ...this.chooseEventPosition(lesson.event_start_time, lesson.event_end_time),
                    ...this.stylePastLesson(lesson.is_finished),
                  }}
                  key={lesson.room_id}
                >
                  <StyledLink to={`room/${lesson.room_id}`}>
                    <TimetableLecturesItemBox>
                      {this.showLiveIcon(lesson.event_start_time, lesson.event_end_time) && (
                        <LiveIcon src={liveIcon} />
                      )}
                      <SubjectGrid>
                        <SubjectImageArea>
                          <SubjectImageAreaItem src={this.chooseCategoryIcon(lesson.category)} />
                        </SubjectImageArea>
                        <SubjectDetailsArea>
                          <SubjectName>{lesson.name}</SubjectName>
                          <SubjectDate>
                            <SubjectDateIcon src={timeIcon} />
                            {this.renderEventDate(lesson.event_start_time, lesson.event_end_time)}
                          </SubjectDate>
                        </SubjectDetailsArea>
                      </SubjectGrid>
                    </TimetableLecturesItemBox>
                  </StyledLink>
                </TimetableLecturesItem>
              ))}
            </TimeTableLecturesList>
          </TimetableLectures>
        </TimetableGrid>
      </ScheduleAreaWrapper>
    );
  }
}

ScheduleArea.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  fetchTimeTableEventsFunc: PropTypes.func.isRequired,
  timeTableData: PropTypes.instanceOf(Array).isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.mainPage.timeTable.isLoading,
  isError: state.mainPage.timeTable.isError,
  timeTableData: state.mainPage.timeTable.timeTableData,
});

const mapDispatchToProps = dispatch => ({
  fetchTimeTableEventsFunc: () => dispatch(fetchTimeTableEvents()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ScheduleArea);
