import React from 'react';
import styled from 'styled-components';
import chemistryIcon from 'images/icons/chemistry_icon.svg';
import peIcon from 'images/icons/pe_icon.svg';
import geographyIcon from 'images/icons/geography_icon.svg';
import biologyIcon from 'images/icons/biology_icon.svg';

const BottomStatsAreaWrapper = styled.div.attrs({ className: 'bottom-stats-area-wrapper' })`
  grid: bottom-stats-area;
  background: #ffffff;
  padding: 20px;
  border: 1px solid #f0f0f0;
  font-size: 12px;
  font-family: 'Heebo', sans-serif;
  font-weight: 100;
  width: 87%;
  overflow-x: hidden;
  overflow-y: scroll;
  height: 100%;
`;

const HomeworkList = styled.ul.attrs({ className: 'homework-list' })`
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

const HomeworkListItem = styled.li.attrs({ className: 'homework-list-item' })`
  margin: 10px 0;
  display: grid;
  grid-template-columns: 2fr 5fr 4fr;
  grid-template-areas: 'homework-icon homework-name homework-due';
`;

const HomeworkIcon = styled.div.attrs({ className: 'homework-icon' })`
  grid: homework-icon;
`;

const HomeworkIconImage = styled.img.attrs({
  className: 'homework-icon-image',
  alt: 'homework-icon-image',
})`
  width: 36px;
`;

const HomeworkName = styled.div.attrs({ className: 'homework-name' })`
  grid: homework-name;
`;

const HomeworkDue = styled.div.attrs({ className: 'homework-due' })`
  grid: homework-due;
  text-align: right;
  line-height: 36px;
`;

const HomeworkTopBar = styled.div.attrs({ className: 'homework-top-bar' })`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-areas: 'home-work-left home-work-right';
`;

const HomeworkLeft = styled.div.attrs({ className: 'homework-left' })`
  grid: home-work-left;
`;

const HomeworkNameItem = styled.p.attrs({ className: 'homework-name-item' })`
  margin: 0;
  font-family: 'Roboto', sans-serif;
  font-weight: 400;
`;

const HomeworkRight = styled.div.attrs({ className: 'homework-right' })`
  grid: home-work-right;
  text-align: right;
`;

const SeeAllHomework = styled.span.attrs({ className: 'see-all-homework' })`
  color: #2f4762;
  cursor: pointer;
`;

const HomeWorkDueButton = styled.span.attrs({ className: 'homework-due-button' })`
  border-radius: 10px;
  padding: 5px 10px;
  font-size: 9px;
  font-weight: bold;
`;

const HomeworkWarning = styled.span.attrs({ className: 'homework-warning' })`
  border-radius: 50%;
  background: #fc2e20;
  font-weight: bold;
  padding: 1px 8px;
  color: #ffffff;
  margin-right: 10px;
`;

const redDate = {
  color: '#552526',
  background: '#fce7e6',
};

const normalDate = {
  color: '#2c2f3a',
  background: '#eff1fb',
};

const HOMEWORK_ARRAY = [
  {
    id: 0,
    icon: chemistryIcon,
    name: 'CHEMISTRY',
    description: 'Enzymes',
    dueDate: '3 MAY 2020',
    showRedDate: true,
    showWarning: true,
  },

  {
    id: 1,
    icon: peIcon,
    name: 'PHYSICAL EDUCATION',
    description: 'Football history',
    dueDate: '8 MAY 2020',
    showRedDate: true,
    showWarning: false,
  },
  {
    id: 2,
    icon: geographyIcon,
    name: 'GEOGRAPHY',
    description: 'African lands',
    dueDate: '9 MAY 2020',
    showRedDate: false,
    showWarning: false,
  },
  {
    id: 3,
    icon: biologyIcon,
    name: 'BIOLOGY',
    description: 'Human body',
    dueDate: '5 MAY 2020',
    showRedDate: false,
    showWarning: false,
  },
];

const BottomStatsArea = () => (
  <BottomStatsAreaWrapper>
    <HomeworkTopBar>
      <HomeworkLeft>Homework</HomeworkLeft>
      <HomeworkRight>
        <SeeAllHomework>See all homework</SeeAllHomework>
      </HomeworkRight>
    </HomeworkTopBar>
    <HomeworkList>
      {HOMEWORK_ARRAY.map(homework => (
        <HomeworkListItem key={homework.id}>
          <HomeworkIcon>
            <HomeworkIconImage src={homework.icon} />
          </HomeworkIcon>
          <HomeworkName>
            <HomeworkNameItem>{homework.name}</HomeworkNameItem>
            <HomeworkNameItem style={{ fontWeight: '100' }}>
              {homework.description}
            </HomeworkNameItem>
          </HomeworkName>
          <HomeworkDue>
            {homework.showWarning ? <HomeworkWarning>!</HomeworkWarning> : null}
            <HomeWorkDueButton style={homework.showRedDate ? redDate : normalDate}>
              {homework.dueDate}
            </HomeWorkDueButton>
          </HomeworkDue>
        </HomeworkListItem>
      ))}
    </HomeworkList>
  </BottomStatsAreaWrapper>
);

export default BottomStatsArea;
