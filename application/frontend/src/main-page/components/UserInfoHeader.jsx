import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const UserInfoHeaderWrapper = styled.div.attrs({ className: 'user-info-header-wrapper' })`
  display: grid;
  grid-template-columns: 1fr 3fr;
  grid-gap: 20px;
  font-family: 'Titillium Web', sans-serif;
  grid-template-areas: 'user-info-image-area user-info-text-area;';
`;

const UserInfoImageWrapper = styled.div.attrs({ className: 'user-info-image-wrapper' })`
  grid: user-info-image-area;
`;

const UserInfoTextWrapper = styled.div.attrs({ className: 'user-info-text-wrapper' })`
  grid: user-info-text-area;
`;

const UserInfoImage = styled.div.attrs({ className: 'user-info-image' })`
  width: 64px;
  height: 64px;
  border-radius: 50%;
  border: 2px solid #f4f4f4;
  background: #2d4564;
  line-height: 64px;
  color: #fff;
  text-align: center;
  font-size: 22px;
  transition: 0.2s;
  cursor: pointer;

  &:hover {
    opacity: 0.85;
  }
`;

const UserName = styled.p.attrs({ className: 'user-name' })`
  font-family: 'Roboto-Medium';
  font-size: 14px;
  margin: 15px 0 3px 0;
`;

const UserStatus = styled.p.attrs({ className: 'user-status' })`
  font-family: 'Roboto-Thin';
  font-size: 12px;
  margin: 0;
`;

const UserInfoHeader = props => (
  <UserInfoHeaderWrapper>
    <UserInfoImageWrapper>
      <UserInfoImage>{props.username.charAt(0).toUpperCase()}</UserInfoImage>
    </UserInfoImageWrapper>
    <UserInfoTextWrapper>
      <UserName>{props.username}</UserName>
      <UserStatus>PGS Software</UserStatus>
    </UserInfoTextWrapper>
  </UserInfoHeaderWrapper>
);

UserInfoHeader.propTypes = {
  username: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  username: state.common.authUser.keycloakInfo.userInfo.preferred_username,
});

export default connect(mapStateToProps, null)(UserInfoHeader);
