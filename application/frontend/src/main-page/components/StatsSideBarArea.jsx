import React from 'react';
import styled from 'styled-components';
import TopStatsArea from 'main-page/components/TopStatsArea';
import BottomStatsArea from 'main-page/components/BottomStatsArea';

const StatsSideBarAreaWrapper = styled.div.attrs({ className: 'stats-sidebar-area-wrapper' })`
  grid: stats-side-bar-area;
  display: grid;
  grid-template-rows: 1fr 1fr;
  grid-gap: 20px;
  grid-template-areas:
    'top-stats-area'
    'bottom-stats-area';
  height: 80%;

  @media only screen and (max-width: 800px) {
    display: none;
  }
`;

const StatsSideBarArea = () => (
  <StatsSideBarAreaWrapper>
    <TopStatsArea />
    <BottomStatsArea />
  </StatsSideBarAreaWrapper>
);

export default StatsSideBarArea;
