import React from 'react';
import styled from 'styled-components';
import { Doughnut } from 'react-chartjs-2';

const TopStatsAreaWrapper = styled.div.attrs({ className: 'top-stats-area-wrapper' })`
  grid: top-stats-area;
  background: #ffffff;
  padding: 20px;
  border: 1px solid #f0f0f0;
  width: 87%;
`;

const CardTitle = styled.p.attrs({ className: 'card-title' })`
  font-size: 14px;
  font-family: 'Heebo', sans-serif;
  margin: 0 0 5px 0;
  font-weight: 100;
`;

const CHART_DATA = {
  labels: ['Done', 'In progress', 'To-Do'],
  datasets: [
    {
      label: 'Count',
      data: [25, 12, 45],
      backgroundColor: ['#CAE1FF', '#2e4663', '#596b8f'],
    },
  ],
};

const TopStatsArea = () => (
  <TopStatsAreaWrapper>
    <CardTitle>Assigned tasks</CardTitle>
    <Doughnut
      data={CHART_DATA}
      options={{
        responsive: true,
        maintainAspectRatio: true,
        legend: {
          display: true,
          position: 'bottom',
        },
      }}
    />
  </TopStatsAreaWrapper>
);

export default TopStatsArea;
