import React, { Component } from 'react';
import styled from 'styled-components';
import eventDateIcon from 'images/icons/event_date_icon.svg';
import powerpointIcon from 'images/icons/powerpoint.svg';
import oneNoteIcon from 'images/icons/onenote.svg';
import outlookIcon from 'images/icons/outlook.svg';
import moreIcon from 'images/icons/more.svg';

const UpcomingEventsWrapper = styled.div.attrs({ className: 'upcoming-events-wrapper' })``;

const SectionTitlte = styled.p.attrs({ className: 'section-title' })`
  font-weight: bold;
  font-size: 14px;
  display: inline-block;
`;

const EventsList = styled.ul.attrs({ className: 'events-list' })`
  list-style-type: none;
  margin: 0;
  padding: 0;
`;

const EventItem = styled.li.attrs({ className: 'event-item' })`
  margin: 0;
  padding: 10px 0;
  display: grid;
  grid-template-columns: 1fr 3fr;
  grid-gap: 20px;
  font-family: 'Roboto-Light', sans-serif;
  grid-template-areas: 'event-icon-area event-info-area';
  border-bottom: 2px solid #ecf0f3;

  &:last-child {
    border-bottom: none;
  }
`;

const EventDateIcon = styled.img.attrs({ className: 'event-date-icon', alt: 'event-date-icon' })`
  width: 14px;
  height: 14px;
  display: inline-block;
  margin: 0;
  margin-right: 5px;
  float: left;
`;

const EventsMoreIcon = styled.img.attrs({ className: 'events-more-icon', alt: 'events-more-icon' })`
  width: 14px;
  height: 14px;
  cursor: pointer;
  display: inline-block;
  margin-top: 15px;
  float: right;
`;

const EventIcon = styled.div.attrs({ className: 'event-icon' })`
  grid: event-icon-area;
`;

const EventInfo = styled.div.attrs({ className: 'event-icon' })`
  grid: event-info-area;
`;

const EventIconImage = styled.img.attrs({ className: 'event-icon-image', alt: 'event-icon-image' })`
  margin: 0;
  width: 25px;
  height: 25px;
  padding: 4px;
  border-radius: 5px;
  border: 1px solid #f4f4f4;
`;

const EventName = styled.p.attrs({ className: 'event-name' })`
  font-size: 12px;
  font-family: 'Roboto-Light';
  text-overflow: ellipsis;
  margin: 0;
  margin-bottom: 2px;

  @media only screen and (max-width: 1250px) {
    font-size: 10px;
  }
`;

const EventBottomInfo = styled.p.attrs({ className: 'event-bottom-info' })`
  font-size: 14px;
  font-weight: bold;
  margin: 0;
`;

const EventDate = styled.span.attrs({ className: 'event-date' })`
  padding-right: 10px;
  border-right: 1px solid #f4f4f4;
  font-weight: 300;
  font-size: 11px;
  display: inline-block;
  height: 14px;
  float: left;
  line-height: 14px;
`;

const EventDueTo = styled.span.attrs({ className: 'event-due-to' })`
  padding-left: 10px;
  font-weight: 400;
  font-size: 11px;

  height: 14px;
  float: left;
  line-height: 14px;
`;

const EVENTS_ARRAY = [
  {
    name: 'Presentation for Brandon',
    icon: oneNoteIcon,
    date: '13/09',
    severity: 'Due soon',
    bgColor: '#ebd4ec',
  },
  {
    name: 'Tasks Document for HR',
    icon: powerpointIcon,
    date: '16/09',
    severity: 'Due soon',
    bgColor: '#fcede8',
  },
  {
    name: 'CuDO Input',
    icon: outlookIcon,
    date: '13/09',
    severity: 'Soon',
    bgColor: '#eaeef5',
  },
];

class UpcomingEvents extends Component {
  chooseColor = severity => {
    if (severity === 'Soon') {
      return {
        color: '#245a2d',
      };
    }

    return {
      color: '#5a3e12',
    };
  };

  renderEvents = () => (
    <EventsList>
      {EVENTS_ARRAY.map(event => (
        <EventItem key={event.name}>
          <EventIcon>
            <EventIconImage style={{ background: `${event.bgColor}` }} src={event.icon} />
          </EventIcon>
          <EventInfo>
            <EventName>{event.name}</EventName>
            <EventBottomInfo>
              <EventDateIcon src={eventDateIcon} />
              <EventDate>{event.date}</EventDate>
              <EventDueTo style={this.chooseColor(event.severity)}>{event.severity}</EventDueTo>
            </EventBottomInfo>
          </EventInfo>
        </EventItem>
      ))}
    </EventsList>
  );

  render() {
    return (
      <UpcomingEventsWrapper>
        <SectionTitlte>Upcoming events</SectionTitlte>
        <EventsMoreIcon src={moreIcon} />
        {this.renderEvents()}
      </UpcomingEventsWrapper>
    );
  }
}

export default UpcomingEvents;
