/* eslint-disable prefer-template */
import React, { Component } from 'react';
import styled from 'styled-components';
import Colors from 'common/colors';
import badgeBg from 'images/gradient_2.svg';
import studyImg from 'images/study.svg';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const NewUserBadgeWrapper = styled.div.attrs({ className: 'new-user-badge-wrapper' })`
  background: url(${badgeBg});
  background-size: cover;
  border-radius: 0px;
  width: calc(98% - 100px);
  color: ${Colors.WHITE};
  font-family: 'Roboto-Light', sans-serif;
  padding: 20px 50px;
  position: relative;
`;

const StudyImage = styled.img.attrs({ className: 'study-image', alt: 'study-image' })`
  height: 150px;
  position: absolute;
  right: 15px;
  top: -30px;
`;

const BadgeHeader = styled.h2.attrs({ className: 'badge-header' })`
  margin: 0;
  font-size: 18px;

  @media only screen and (max-width: 600px) {
    font-size: 15px;
  }
`;

const BadgeDescription = styled.p.attrs({ className: 'badge-description' })`
  font-size: 12px;
  font-weight: 300;
  width: 50%;

  @media only screen and (max-width: 600px) {
    font-size: 10px;
  }
`;

const BadgeBottomText = styled.p.attrs({ className: 'badge-bottom-text' })`
  font-size: 10px;
  font-weight: 300;
  width: 70%;
  margin-top: 0px;
`;

const BoldText = styled.span.attrs({ className: 'bold-text' })`
  font-family: 'Roboto-Medium', sans-serif;
`;

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

class WelcomeBackBadge extends Component {
  state = {
    showBadge: true,
  };

  closeBadge = () => {
    this.setState({ showBadge: false });
  };

  addZeroBefore = n => (n < 10 ? '0' : '') + n;

  renderTodayDate = () => {
    const date = new Date();

    return (
      this.addZeroBefore(date.getDate()) +
      ' ' +
      monthNames[date.getMonth()] +
      ' ' +
      this.addZeroBefore(date.getFullYear()) +
      ', ' +
      dayNames[date.getDay()]
    );
  };

  render() {
    const { showBadge } = this.state;
    const { username } = this.props;

    if (showBadge) {
      return (
        <NewUserBadgeWrapper>
          <BadgeHeader>Welcome back, {username}!</BadgeHeader>
          <BadgeDescription>
            You are on a <BoldText>10 days learning streak!</BoldText> Keep it up and improve your
            results.
          </BadgeDescription>
          <BadgeBottomText>
            Today: <BoldText>{this.renderTodayDate()}</BoldText>
          </BadgeBottomText>
          <StudyImage src={studyImg} />
        </NewUserBadgeWrapper>
      );
    }

    return null;
  }
}

WelcomeBackBadge.propTypes = {
  username: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  username: state.common.authUser.keycloakInfo.userInfo.preferred_username,
});
export default connect(mapStateToProps, null)(WelcomeBackBadge);
