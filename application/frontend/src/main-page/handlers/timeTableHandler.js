import RequestService from 'common/services/RequestService';

export default () => RequestService.get(`/api/room/range`);
