import React from 'react';
import styled from 'styled-components';
import MainStats from 'main-page/containers/MainStats';
import UserInfo from 'main-page/containers/UserInfo';

const MainPageWrapper = styled.div.attrs({ className: 'main-page-wrapper' })`
  display: grid;
  grid-template-columns: 27fr 8fr;
  grid-template-areas: 'main-stats-area user-info-area';
  height: 100%;

  @media only screen and (max-width: 1150px) {
    grid-template-columns: 1fr;
    grid-template-areas: 'main-stats-area';
  }
`;

export default function MainPage() {
  return (
    <MainPageWrapper>
      <MainStats />
      <UserInfo />
    </MainPageWrapper>
  );
}
