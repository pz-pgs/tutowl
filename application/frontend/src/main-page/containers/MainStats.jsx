import React from 'react';
import styled from 'styled-components';
import Colors from 'common/colors';
import WelcomeBackBadge from 'main-page/components/WelcomeBackBadge';
import ScheduleArea from 'main-page/components/ScheduleArea';
import StatsSideBarArea from 'main-page/components/StatsSideBarArea';

const MainStatsWrapper = styled.div.attrs({ className: 'main-stats-wrapper' })`
  grid: main-stats-area;
  background: ${Colors.BACKGROUND_COLOR};
  padding: 40px 30px;

  @media only screen and (max-width: 800px) {
    padding: 40px 20px 20px 20px;
  }
`;

const MainStatsGridArea = styled.div.attrs({ className: 'main-stats-grid-area' })`
  margin-top: 20px;
  width: 100%;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  box-sizing: border-box;
  grid-gap: 20px;
  grid-template-areas: 'schedule-area stats-side-bar-area';
  height: 59vh;
  overflow: hidden;
  max-height: 59vh;

  @media only screen and (max-width: 800px) {
    grid-template-areas: 'schedule-area';
    grid-template-columns: 1fr;
  }
`;

const MainStats = () => (
  <MainStatsWrapper>
    <WelcomeBackBadge />
    <MainStatsGridArea>
      <ScheduleArea />
      <StatsSideBarArea />
    </MainStatsGridArea>
  </MainStatsWrapper>
);

export default MainStats;
