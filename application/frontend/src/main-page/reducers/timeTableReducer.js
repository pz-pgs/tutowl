import {
  TIME_TABLE_PENDING,
  TIME_TABLE_OK,
  TIME_TABLE_FAIL,
} from 'main-page/actions/timeTableActions';

const INITIAL_STATE = {
  isLoading: false,
  isError: false,
  timeTableData: [],
};

export default (state, { type, payload }) => {
  const stateDefinition = typeof state === 'undefined' ? INITIAL_STATE : state;
  switch (type) {
    case TIME_TABLE_PENDING:
      return { ...stateDefinition, isLoading: true, isError: false, timeTableData: [] };
    case TIME_TABLE_OK:
      return {
        ...stateDefinition,
        isLoading: false,
        isError: false,
        timeTableData: payload.timeTableData,
      };
    case TIME_TABLE_FAIL:
      return { ...stateDefinition, isLoading: false, isError: true, timeTableData: [] };
    default:
      return stateDefinition;
  }
};
