import { combineReducers } from 'redux';
import timeTableReducer from 'main-page/reducers/timeTableReducer';

export default combineReducers({
  timeTable: timeTableReducer,
});
