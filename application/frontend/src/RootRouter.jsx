import React from 'react';
import { Route, Switch } from 'react-router-dom';
import getPath from 'common/utils/path';
import makeLoadable from 'common/utils/loadable';
import NotFoundPage from 'common/components/NotFoundPage';
import { ROOM_PATTERN_PATH } from 'common/paths';

export const ROOT_PATH = getPath('/');
export const ROOM_PATH = getPath(ROOM_PATTERN_PATH);
export const PROFILE_PATH = getPath('/settings');
export const MESSAGES_PATH = getPath('/messages');
export const CLASSES_PATH = getPath('/classes');
export const CALENDAR_PATH = getPath('/calendar');

export const LoadableMainPage = makeLoadable(() => import('main-page/containers/MainPage'));
export const LoadableRoomPage = makeLoadable(() => import('room-page/containers/RoomPage'));
export const LoadableClassesPage = makeLoadable(() =>
  import('classes-page/containers/ClassesPage'),
);
export const LoadableCalendarPage = makeLoadable(() =>
  import('calendar-page/containers/CalendarPage'),
);
export const LoadableProfilePage = makeLoadable(() =>
  import('profile-page/containers/ProfilePage'),
);
export const LoadableMessagesPage = makeLoadable(() =>
  import('messages-page/containers/MessagesPage'),
);

const RootRouter = () => (
  <Switch>
    <Route exact path={ROOT_PATH} component={LoadableMainPage} />
    <Route exact path={ROOM_PATH} component={LoadableRoomPage} />
    <Route exact path={PROFILE_PATH} component={LoadableProfilePage} />
    <Route exact path={MESSAGES_PATH} component={LoadableMessagesPage} />
    <Route exact path={CLASSES_PATH} component={LoadableClassesPage} />
    <Route exact path={CALENDAR_PATH} component={LoadableCalendarPage} />
    <Route component={NotFoundPage} />
  </Switch>
);

export default RootRouter;
