import React, { Component } from 'react';
import styled from 'styled-components';
import MessageBoxArea from 'messages-page/components/MessageBoxArea';
import MessagesListArea from 'messages-page/components/MessagesListArea';
import { connect } from 'react-redux';
import { setCurrentChat } from 'messages-page/actions/newMessageActions';
import { fetchChatHistory, addNewUserToChatBox } from 'messages-page/actions/messagesListActions';
import PropTypes from 'prop-types';
import shortid from 'shortid';

const MessagesPageWrapper = styled.div.attrs({ className: 'message-page-wrapper' })`
  display: grid;
  grid-template-columns: 1fr 4fr;
  grid-template-areas: 'messages-list-area message-box-area';
  height: 100%;
`;

class MessagesPage extends Component {
  addNewUser = username => {
    if (this.props.chatList.some(message => message.sender === username)) {
      this.props.setCurrentChatFunc(username);
      this.props.fetchChatHistoryFunc(username);
    } else {
      const newMessage = {
        id: shortid.generate(),
        sender: username,
        dateTime: new Date(),
        content: '',
        unread: 0,
        active: false,
      };

      this.props.addNewUserToChatBoxFunc(newMessage);
    }
  };

  render() {
    return (
      <MessagesPageWrapper>
        <MessagesListArea addNewUser={this.addNewUser} />
        <MessageBoxArea addNewUser={this.addNewUser} />
      </MessagesPageWrapper>
    );
  }
}

MessagesPage.propTypes = {
  setCurrentChatFunc: PropTypes.func.isRequired,
  fetchChatHistoryFunc: PropTypes.func.isRequired,
  addNewUserToChatBoxFunc: PropTypes.func.isRequired,
  chatList: PropTypes.instanceOf(Array).isRequired,
};

const mapStateToProps = state => ({
  chatList: state.messages.messagesList.chatList,
});

const mapDispatchToProps = dispatch => ({
  setCurrentChatFunc: username => dispatch(setCurrentChat(username)),
  fetchChatHistoryFunc: username => dispatch(fetchChatHistory(username)),
  addNewUserToChatBoxFunc: chatBoxItem => dispatch(addNewUserToChatBox(chatBoxItem)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MessagesPage);
