import fetchUsers from 'messages-page/handlers/newMessageHandlers';

export const FETCH_USERS_PENDING = 'FETCH_USERS_PENDING';
export const FETCH_USERS_OK = 'FETCH_USERS_OK';
export const FETCH_USERS_FAIL = 'FETCH_USERS_FAIL';

export const SET_CURRENT_CHAT = 'SET_CURRENT_CHAT';

export const makeFetchUsersPending = () => ({
  type: FETCH_USERS_PENDING,
});

export const makeFetchUsersOk = usersData => ({
  type: FETCH_USERS_OK,
  payload: { usersData },
});

export const makeFetchUsersFail = () => ({
  type: FETCH_USERS_FAIL,
});

export const makeSetCurrentChat = username => ({
  type: SET_CURRENT_CHAT,
  payload: { username },
});

export const fetchUsersList = () => dispatch => {
  dispatch(makeFetchUsersPending());

  return fetchUsers()
    .then(res => {
      dispatch(makeFetchUsersOk(res.data));
    })
    .catch(() => {
      dispatch(makeFetchUsersFail());
    });
};

export const setCurrentChat = username => dispatch => {
  dispatch(makeSetCurrentChat(username));
};
