/* eslint-disable prefer-template */
import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import MessageMainInput from 'messages-page/components/MessageMainInput';
import MessagesMainComponent from 'messages-page/components/MessagesMainComponent';
import { connect } from 'react-redux';
import { addNewMessage } from 'messages-page/actions/messagesListActions';
import SockJsClient from 'react-stomp';
import ProgressIndicatorCircular from 'common/components/ProgressIndicatorCircular';
import shortid from 'shortid';
import messageSound from 'sounds/message.mp3';

const MessagesMainAreaWrapper = styled.div.attrs({ className: 'messages-main-area-wrapper' })`
  grid: messages-main-area;
  position: relative;
  height: 91%;
  font-family: 'Roboto-Light', sans-serif;
`;

const ErrorBlock = styled.div.attrs({ className: 'error-block' })`
  padding: 10px;
  text-align: center;
  background: #fce7e6;
  font-size: 12px;
  color: #552526;
`;

class MessagesMainArea extends Component {
  state = {
    isTyping: false,
    audio: new Audio(messageSound),
  };

  onMessageReceive = msg => {
    if (msg.type === 'TYPING' && this.props.chosenChatUser === msg.sender) {
      this.setState({
        isTyping: true,
      });
    } else if (msg.type === 'STOP_TYPING' && this.props.chosenChatUser === msg.sender) {
      this.setState({
        isTyping: false,
      });
    } else {
      this.setState({
        isTyping: false,
      });

      this.state.audio.play();
      this.props.addNewUser(msg.sender);
      const messageObj = msg.id === undefined ? { ...msg, id: shortid.generate() } : msg;
      this.props.addNewMessageFunc(messageObj);
    }
  };

  sendMessage = msg => {
    try {
      this.clientRef.sendMessage('/app/sendPrivateMessage', JSON.stringify(msg));

      if (msg.type === 'CHAT') {
        const messageObj = msg.id === undefined ? { ...msg, id: shortid.generate() } : msg;
        this.props.addNewMessageFunc(messageObj);
      }

      return true;
    } catch (e) {
      return false;
    }
  };

  render() {
    const wsSourceUrl = window.location.protocol + '//' + window.location.host + '/api/chat/ws';
    const { isChatHistoryLoading, isChatHistoryLoadingError } = this.props;

    if (isChatHistoryLoadingError)
      return <ErrorBlock>There was an error during chat boxes fetching.</ErrorBlock>;

    return (
      <MessagesMainAreaWrapper style={this.props.chosenChatUser ? {} : { visibility: 'hidden' }}>
        {isChatHistoryLoading ? (
          <ProgressIndicatorCircular size={40} />
        ) : (
          <MessagesMainComponent
            currentUser={this.props.currentUsername}
            messagesArray={this.props.chatHistory}
            isTyping={this.state.isTyping}
            chosenChatUser={this.props.chosenChatUser}
          />
        )}

        <SockJsClient
          url={wsSourceUrl}
          topics={[`/user/${this.props.currentUsername.toString().toLowerCase()}/reply`]}
          onMessage={this.onMessageReceive}
          ref={client => {
            this.clientRef = client;
          }}
        />

        <MessageMainInput
          sender={this.props.currentUsername}
          receiver={this.props.chosenChatUser}
          onSend={this.sendMessage}
        />
      </MessagesMainAreaWrapper>
    );
  }
}

MessagesMainArea.propTypes = {
  chosenChatUser: PropTypes.string.isRequired,
  currentUsername: PropTypes.string.isRequired,
  addNewUser: PropTypes.func.isRequired,
  addNewMessageFunc: PropTypes.func.isRequired,
  isChatHistoryLoading: PropTypes.bool.isRequired,
  isChatHistoryLoadingError: PropTypes.bool.isRequired,
  chatHistory: PropTypes.instanceOf(Array).isRequired,
};

const mapStateToProps = state => ({
  chosenChatUser: state.messages.usersList.currentChatUsername,
  currentUsername: state.common.authUser.keycloakInfo.userInfo.preferred_username,
  isChatHistoryLoading: state.messages.messagesList.isChatHistoryLoading,
  isChatHistoryLoadingError: state.messages.messagesList.isChatHistoryLoadingError,
  chatHistory: state.messages.messagesList.chatHistory,
});

const mapDispatchToProps = dispatch => ({
  addNewMessageFunc: message => dispatch(addNewMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MessagesMainArea);
