import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { filterMessagesChatBox } from 'messages-page/actions/messagesListActions';
import searchIcon from 'images/icons/search_left.svg';

const MessageSearchInputWrapper = styled.div.attrs({ className: 'message-serach-input-wrapper' })`
  position: relative;
  margin-top: 10px;
  display: inline-block;
  width: 60%;

  @media only screen and (max-width: 1130px) {
    width: 40%;
  }

  @media only screen and (max-width: 990px) {
    display: none;
  }
`;

const MessageSearchInputComponent = styled.input.attrs({
  className: 'message-search-input-component',
})`
  border-radius: 5px;
  background: #f5f7f9;
  color: #cbced0;
  font-weight: 300;
  padding: 15px 15px 15px 40px;
  border: 1px solid #f0f0f0;
  outline: none;
  color: #000000;
  font-size: 10px;
  width: 70%;
  margin-left: 10px;
`;

const MessagesSearchIcon = styled.img.attrs({
  className: 'messages-search-icon',
  alt: 'messages-search-icon',
})`
  width: 16px;
  height: 16px;
  position: absolute;
  left: 20px;
  top: 12px;
`;

const PLACEHOLDER = 'Search for messages...';

class MessageSearchInput extends Component {
  state = {
    searchInput: '',
  };

  onChange = event => {
    const {
      target: { value },
    } = event;

    this.setState(
      {
        searchInput: value,
      },
      () => {
        this.props.filterMessagesChatBoxFunc(value);
      },
    );
  };

  render() {
    const { searchInput } = this.state;
    return (
      <MessageSearchInputWrapper>
        <MessagesSearchIcon src={searchIcon} />
        <MessageSearchInputComponent
          placeholder={PLACEHOLDER}
          onChange={this.onChange}
          value={searchInput}
        />
      </MessageSearchInputWrapper>
    );
  }
}

MessageSearchInput.propTypes = {
  filterMessagesChatBoxFunc: PropTypes.func.isRequired,
};

const mapDispatchToProps = dispatch => ({
  filterMessagesChatBoxFunc: keyword => dispatch(filterMessagesChatBox(keyword)),
});

export default connect(null, mapDispatchToProps)(MessageSearchInput);
