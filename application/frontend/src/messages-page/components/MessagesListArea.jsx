/* eslint-disable no-nested-ternary */
import React, { Component } from 'react';
import styled from 'styled-components';
import MessageSearchInput from 'messages-page/components/MessageSearchInput';
import MessageItem from 'messages-page/components/MessageItem';
import NewMessageModal from 'messages-page/components/NewMessageModal';
import newMessageIcon from 'images/icons/create_room.svg';
import { setCurrentChat } from 'messages-page/actions/newMessageActions';
import {
  fetchChatBoxList,
  fetchChatHistory,
  markChatWithUserAsRead,
} from 'messages-page/actions/messagesListActions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ProgressIndicatorCircular from 'common/components/ProgressIndicatorCircular';

const MessagesListAreaWrapper = styled.div.attrs({ className: 'messages-list-area' })`
  grid: messages-list-area;
  background: #fff;
  border-right: 1px solid #f0f0f0;
`;

const MessagesList = styled.div.attrs({ className: 'messages-list' })`
  margin-top: 10px;
  max-height: 95vh;

  &:hover {
    overflow-y: scroll;
  }

  @media only screen and (max-width: 990px) {
    margin-top: 65px;
  }
`;

const NewMessageButton = styled.div.attrs({ className: 'new-mesage-button' })`
  display: inline-block;
  border-radius: 5px;
  width: 5%;
  padding: 10px 13px 5px 10px;
  border: 1px solid #f0f0f0;
  transition: 0.2s;
  cursor: pointer;
  float: right;
  margin: 10px 10px 0 0;
  height: 28px;
  text-align: center;

  @media only screen and (max-width: 990px) {
    width: 70%;
  }

  @media only screen and (max-width: 790px) {
    width: 45%;
  }

  &:hover {
    background: #f0f0f0;
  }
`;

const NewMessageIcon = styled.img.attrs({
  className: 'new-message-icon',
  alt: 'new-message-icon',
})`
  height: 20px;
`;

const MessageListHeader = styled.div.attrs({ className: 'message-list-header' })`
  display: block;
  width: 100%;
`;

const NoMessagesBox = styled.div.attrs({ className: 'no-messages-box' })`
  text-align: center;
  font-size: 10px;
  width: 60%;
  padding: 20px;
  margin: 40px auto 0 auto;
  border: 1px solid #f0f0f0;
`;

const ErrorBlock = styled.div.attrs({ className: 'error-block' })`
  padding: 20px 10px;
  margin: 5px 0;
  text-align: center;
  background: #fce7e6;
  font-size: 12px;
  color: #552526;
  width: 80%;
  margin: 20px auto;
`;

class MessagesListArea extends Component {
  state = {
    shouldShowNewMessageModal: false,
  };

  componentWillMount() {
    this.props.fetchChatBoxListFunc();
  }

  setCurrentChatClick = message => {
    this.props.setCurrentChatFunc(message.sender);
    this.props.fetchChatHistoryFunc(message.sender);
    this.props.markChatWithUserAsReadFunc(message.sender);
  };

  showNewMessageModal = () => (
    <NewMessageModal
      onClose={() => {
        this.setState({ shouldShowNewMessageModal: false });
      }}
      onChoice={username => {
        this.props.addNewUser(username);

        this.setState({
          shouldShowNewMessageModal: false,
        });
      }}
    />
  );

  clickModal = () => {
    this.setState({
      shouldShowNewMessageModal: !this.state.shouldShowNewMessageModal,
    });
  };

  renderMessages = () => {
    const valueToMap =
      this.props.searchValue.length > 0 ? this.props.filteredChatList : this.props.chatList;
    return (
      <MessagesList>
        {valueToMap.map(message => (
          <MessageItem
            key={message.id}
            messageObject={message}
            onClick={() => this.setCurrentChatClick(message)}
            chosenChatUser={this.props.chosenChatUser}
          />
        ))}
      </MessagesList>
    );
  };

  render() {
    const { shouldShowNewMessageModal } = this.state;
    const { isChatListLoading, isChatListError } = this.props;
    const valueToMap =
      this.props.searchValue.length > 0 ? this.props.filteredChatList : this.props.chatList;

    return (
      <MessagesListAreaWrapper>
        <MessageListHeader>
          <MessageSearchInput />
          <NewMessageButton onClick={this.clickModal}>
            <NewMessageIcon src={newMessageIcon} />
          </NewMessageButton>
        </MessageListHeader>
        {isChatListLoading ? (
          <ProgressIndicatorCircular size={40} />
        ) : isChatListError ? (
          <ErrorBlock>There was an error during chat boxes fetching.</ErrorBlock>
        ) : valueToMap.length === 0 ? (
          <NoMessagesBox>No messages available.</NoMessagesBox>
        ) : (
          this.renderMessages()
        )}
        {shouldShowNewMessageModal && this.showNewMessageModal()}
      </MessagesListAreaWrapper>
    );
  }
}

MessagesListArea.propTypes = {
  setCurrentChatFunc: PropTypes.func.isRequired,
  chosenChatUser: PropTypes.string.isRequired,
  searchValue: PropTypes.string.isRequired,
  addNewUser: PropTypes.func.isRequired,
  fetchChatHistoryFunc: PropTypes.func.isRequired,
  fetchChatBoxListFunc: PropTypes.func.isRequired,
  markChatWithUserAsReadFunc: PropTypes.func.isRequired,
  isChatListLoading: PropTypes.bool.isRequired,
  isChatListError: PropTypes.bool.isRequired,
  chatList: PropTypes.instanceOf(Array).isRequired,
  filteredChatList: PropTypes.instanceOf(Array).isRequired,
};

const mapStateToProps = state => ({
  chosenChatUser: state.messages.usersList.currentChatUsername,
  isChatListLoading: state.messages.messagesList.isChatListLoading,
  isChatListError: state.messages.messagesList.isChatListError,
  chatList: state.messages.messagesList.chatList,
  filteredChatList: state.messages.messagesList.filteredChatList,
  searchValue: state.messages.messagesList.searchValue,
});

const mapDispatchToProps = dispatch => ({
  setCurrentChatFunc: username => dispatch(setCurrentChat(username)),
  fetchChatBoxListFunc: () => dispatch(fetchChatBoxList()),
  fetchChatHistoryFunc: username => dispatch(fetchChatHistory(username)),
  markChatWithUserAsReadFunc: username => dispatch(markChatWithUserAsRead(username)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MessagesListArea);
