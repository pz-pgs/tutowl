import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import TextInput from 'common/components/TextInput';
import GenericModal from 'common/components/GenericModal';
import { connect } from 'react-redux';
import { fetchUsersList, setCurrentChat } from 'messages-page/actions/newMessageActions';
import ProgressIndicatorCircular from 'common/components/ProgressIndicatorCircular';

const NewMessageErrorBlock = styled.div.attrs({ className: 'new-message-error-block' })`
  padding: 10px;
  margin: 5px 0;
  text-align: center;
  background: #fce7e6;
  font-size: 12px;
  color: #552526;
`;

const ListOfPeople = styled.ul.attrs({ className: 'list-of-people' })`
  margin: 0;
  list-style-type: none;
  padding: 0;
`;

const ListOfPeopleItem = styled.li.attrs({ className: 'list-of-people-item' })`
  margin: 0;
  display: grid;
  grid-template-columns: 1fr 5fr;
  grid-gap: 10px;
  grid-template-areas: 'person-avatar person-username';
  border: 1px solid #f0f0f0;
  padding: 10px;
  line-height: 20px;
  border-radius: 4px;
  cursor: pointer;
  transition: 0.2s;

  &:hover {
    background: #f0f0f0;
  }
`;

const PersonAvatar = styled.div.attrs({ className: 'person-avatar' })`
  grid: person-avatar;
  display: inline-block;
  border-radius: 50%;
  width: 20px;
  height: 20px;
  line-height: 20px;
  color: #fff;
  background: #2d4564;
  text-align: center;
  font-size: 12px;
`;

const PersonUsername = styled.div.attrs({ className: 'person-username' })`
  grid: person-username;
  font-size: 12px;
`;

class NewMessageModal extends Component {
  state = {
    personText: '',
    filteredListOfPeople: [],
  };

  componentDidMount() {
    this.props.fetchUsersListFunc();
  }

  onTextChange = (name, value) => {
    this.setState(
      {
        [name]: value,
      },
      () => {
        const filteredArray = this.props.usersData
          .filter(
            user => user.username.includes(value) && user.username !== this.props.currentUsername,
          )
          .slice(0, 5);

        this.setState({
          filteredListOfPeople: filteredArray,
        });
      },
    );
  };

  usernameOnClick = username => {
    this.props.setCurrentChatFunc(username);
    this.props.onChoice(username);
  };

  renderListOfPeople = () => (
    <ListOfPeople>
      {this.state.filteredListOfPeople.map(person => (
        <ListOfPeopleItem key={person.id} onClick={() => this.usernameOnClick(person.username)}>
          <PersonAvatar>{person.username.charAt(0).toUpperCase()}</PersonAvatar>
          <PersonUsername>{person.username}</PersonUsername>
        </ListOfPeopleItem>
      ))}
    </ListOfPeople>
  );

  render() {
    const { personText, filteredListOfPeople } = this.state;
    const { isError, isLoading } = this.props;

    if (isLoading) return <ProgressIndicatorCircular size={40} />;

    if (isError) {
      return (
        <GenericModal onClose={this.props.onClose}>
          <NewMessageErrorBlock>An error occurred while fetching users list.</NewMessageErrorBlock>
        </GenericModal>
      );
    }

    return (
      <GenericModal onClose={this.props.onClose}>
        <TextInput
          id="personText"
          name="personText"
          onChange={this.onTextChange}
          label="Find people..."
          validator={() => true}
        />
        {filteredListOfPeople.length > 0 && personText.length > 0 && this.renderListOfPeople()}
      </GenericModal>
    );
  }
}

NewMessageModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  onChoice: PropTypes.func.isRequired,
  setCurrentChatFunc: PropTypes.func.isRequired,
  fetchUsersListFunc: PropTypes.func.isRequired,
  usersData: PropTypes.instanceOf(Array).isRequired,
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  currentUsername: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.messages.usersList.isLoading,
  isError: state.messages.usersList.isError,
  usersData: state.messages.usersList.usersData,
  currentUsername: state.common.authUser.keycloakInfo.userInfo.preferred_username,
});

const mapDispatchToProps = dispatch => ({
  fetchUsersListFunc: () => dispatch(fetchUsersList()),
  setCurrentChatFunc: username => dispatch(setCurrentChat(username)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewMessageModal);
