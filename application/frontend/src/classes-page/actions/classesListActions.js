import getRooms from 'classes-page/handlers/classesListHandler';

export const GET_ROOM_LIST_PENDING = 'GET_ROOM_LIST_PENDING';
export const GET_ROOM_LIST_OK = 'GET_ROOM_LIST_OK';
export const GET_ROOM_LIST_FAIL = 'GET_ROOM_LIST_FAIL';

export const makeGetRoomListPending = () => ({
  type: GET_ROOM_LIST_PENDING,
});

export const makeGetRoomListOk = roomsData => ({
  type: GET_ROOM_LIST_OK,
  payload: { roomsData },
});

export const makeGetRoomListFail = () => ({
  type: GET_ROOM_LIST_FAIL,
});

export const getRoomsList = () => dispatch => {
  dispatch(makeGetRoomListPending());

  return getRooms()
    .then(res => {
      dispatch(makeGetRoomListOk(res.data));
    })
    .catch(() => {
      dispatch(makeGetRoomListFail());
    });
};
