import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import withLoading, { ProgIndSize } from 'common/utils/withLoading';
import { connect } from 'react-redux';
import ClassesCategory from 'classes-page/components/ClassesCategory';
import { getRoomsList } from 'classes-page/actions/classesListActions';
import ClassesPageErrorPlaceholder from 'classes-page/components/ClassesPageErrorPlaceholder';

const ClassesPageWrapper = styled.div.attrs({ className: 'classes-page-wrapper' })`
  padding: 30px;
  overflow-y: scroll;
  max-height: 82vh;
`;

const ClassesPageWrapperWithLoading = withLoading(ClassesPageWrapper, ProgIndSize.XX_LARGE);

class ClassesPage extends Component {
  componentWillMount() {
    this.props.getRoomsListFunc();
  }

  render() {
    const { isLoading, isError, roomsList } = this.props;

    if (isError) {
      return <ClassesPageErrorPlaceholder />;
    }

    return (
      <ClassesPageWrapperWithLoading isLoading={isLoading}>
        {roomsList.map(classData => (
          <ClassesCategory key={classData.id} classData={classData} />
        ))}
      </ClassesPageWrapperWithLoading>
    );
  }
}

ClassesPage.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  getRoomsListFunc: PropTypes.func.isRequired,
  roomsList: PropTypes.instanceOf(Array).isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.classes.classesList.isLoading,
  isError: state.classes.classesList.isError,
  roomsList: state.classes.classesList.roomsList,
});

const mapDispatchToProps = dispatch => ({
  getRoomsListFunc: () => dispatch(getRoomsList()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ClassesPage);
