import { combineReducers } from 'redux';
import classesListReducer from 'classes-page/reducers/classesListReducer';

export default combineReducers({
  classesList: classesListReducer,
});
