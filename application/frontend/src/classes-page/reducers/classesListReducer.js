import {
  GET_ROOM_LIST_PENDING,
  GET_ROOM_LIST_OK,
  GET_ROOM_LIST_FAIL,
} from 'classes-page/actions/classesListActions';

const INITIAL_STATE = {
  isLoading: false,
  isError: false,
  roomsList: [],
};

export default (state, { type, payload }) => {
  const stateDefinition = typeof state === 'undefined' ? INITIAL_STATE : state;
  switch (type) {
    case GET_ROOM_LIST_PENDING:
      return { ...stateDefinition, isLoading: true, isError: false, roomsList: [] };
    case GET_ROOM_LIST_OK:
      return {
        ...stateDefinition,
        isLoading: false,
        isError: false,
        roomsList: payload.roomsData,
      };
    case GET_ROOM_LIST_FAIL:
      return { ...stateDefinition, isLoading: false, isError: true, roomsList: [] };
    default:
      return stateDefinition;
  }
};
