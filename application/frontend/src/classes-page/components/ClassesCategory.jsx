/* eslint-disable prefer-template */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import csImage from 'images/classes/computer_science.svg';
import artImage from 'images/classes/art.svg';
import financesImage from 'images/classes/finances.svg';
import chemistryImage from 'images/classes/chemistry.svg';
import frenchImage from 'images/classes/french.svg';
import physicsIcon from 'images/classes/physics.svg';
import geographyIconTwo from 'images/classes/geography.svg';
import mathIcon from 'images/classes/math.svg';
import chemistryIconTwo from 'images/classes/chemistry_2.svg';

const ClassesCategoryWrapper = styled.div.attrs({ className: 'classes-category-wrapper' })`
  padding: 20px;
  background: #ffffff;
  width: calc(100% - 40px);
  margin-bottom: 20px;
`;

const ClassName = styled.p.attrs({ className: 'class-name' })`
  font-size: 22px;
  display: inline-block;
  font-family: 'Heebo', sans-serif;
  line-height: 36px;
  margin: 0;
  float: left;
`;

const BoldText = styled.span.attrs({ className: 'bold-text' })`
  font-weight: bold;
`;

const ClassDescription = styled.p.attrs({ className: 'class-description' })`
  font-family: 'Heebo', sans-serif;
  font-weight: 100;
  margin: 50px 0 0 0;
  font-size: 12px;
`;

const ClassIconImage = styled.img.attrs({ className: 'class-icon-image', alt: 'class-icon-image' })`
  height: 36px;
  margin-right: 10px;
  float: left;
`;

const StyledLink = styled(Link).attrs({ className: 'styled-link' })`
  text-decoration: none;
  color: #ffffff;
  font-weight: bold;
`;

const ClassItemsList = styled.div.attrs({ className: 'class-items-list' })`
  margin: 20px 0 0 0;
  padding: 0;
  max-width: 80vw;
  overflow-x: scroll;
  white-space: nowrap;
`;

const ViewButton = styled.button.attrs({ className: 'view-button' })`
  padding: 10px 20px;
  color: #fff;
  background: #2d4564;
  border-radius: 4px;
  border: 1px solid #f0f0f0;
  position: absolute;
  bottom: 10px;
  right: 10px;
  cursor: pointer;
  outline: none;
`;

const ClassImageArea = styled.div.attrs({ className: 'class-image-area' })`
  grid: class-image-area;
  border-bottom: 1px solid #f0f0f0;
  cursor: pointer;
`;

const ClassImageComponent = styled.img.attrs({
  className: 'class-image-component',
  alt: 'class-image-component',
})`
  height: 100%;
  border-top-right-radius: 5px;
  border-top-left-radius: 5px;
`;

const ClassDetailsArea = styled.div.attrs({ className: 'class-details-area' })`
  grid: class-details-area;
  padding: 20px;
  position: relative;
  font-size: 12px;
`;

const ClassItem = styled.li.attrs({ className: 'class-item' })`
  display: inline-block;
  margin: 0;
  padding: 0;
  width: 200px;
  overflow: hidden;
  vertical-align: top;
  white-space: normal;
`;

const AllTopicsSeparator = styled.div.attrs({ className: 'all-topics-separator' })`
  font-size: 12px;
  border-bottom: 1px solid #f0f0f0;
  position: relative;
  margin: 20px 40px 0 70px;
  font-weight: 100;
  font-family: 'Heebo', sans-serif;
`;

const AllTopicsText = styled.div.attrs({ className: 'all-topics-separator' })`
  font-size: 10px;
  position: absolute;
  left: -70px;
  top: -8px;
  font-weight: 100;
`;

const ClassItemWrapper = styled.div.attrs({ className: 'class-item-wrapper' })`
  border-radius: 5px;
  border: 1px solid #f0f0f0;
  display: inline-block;
  margin-right: 20px;
  display: grid;
  grid-template-rows: 1fr 1fr;
  grid-template-areas:
    'class-image-area'
    'class-details-area';
  overflow: hidden;
`;

const RoomName = styled.p.attrs({ className: 'room-name' })`
  font-size: 12px;
  margin: 0;
  padding: 0;
  font-family: 'Heebo', sans-serif;
`;

const RoomDescription = styled.p.attrs({ className: 'room-description' })`
  font-size: 9px;
  font-family: 'Heebo', sans-serif;
  font-weight: 100;
  margin: 10px 0 0 0;
  padding: 0;
`;

const NoRoomsAvailable = styled.div.attrs({ className: 'no-room-available' })`
  font-size: 13px;
  font-family: 'Roboto', sans-serif !important;
  margin: 40px 0 20px 0;
  font-weight: 100;
`;

const DateComponent = styled.div.attrs({ className: 'date-component' })`
  padding: 8px;
  background: #f3f7fd;
  border-radius: 4px;
  border: 1px solid #f0f0f0;
  position: absolute;
  bottom: 55px;
  width: 80%;
  text-align: center;
  left: 50%;
  transform: translate(-50%, 0);
  font-size: 10px;
`;

const monthNames = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

class ClassesCategory extends Component {
  getRandomRoomImage = () => {
    const randomNumber = this.randomInRange(0, 4);

    switch (randomNumber) {
      case 0:
        return csImage;
      case 1:
        return artImage;
      case 2:
        return financesImage;
      case 3:
        return chemistryImage;
      case 4:
        return frenchImage;
      default:
        return null;
    }
  };

  chooseClassIcon = className => {
    if (className === 'GEOGRAPHY') return geographyIconTwo;
    if (className === 'CHEMISTRY') return chemistryIconTwo;
    if (className === 'MATH') return mathIcon;
    if (className === 'PHYSICS') return physicsIcon;
    return null;
  };

  randomInRange = (min, max) => Math.floor(Math.random() * (max - min) + min);

  addZeroBefore = n => (n < 10 ? '0' : '') + n;

  renderEventDate = (eventStartDate, eventEndDate) => {
    const eventStartDateObject = new Date(eventStartDate);
    const eventEndDateObject = new Date(eventEndDate);

    return (
      this.addZeroBefore(eventStartDateObject.getHours()) +
      ':' +
      this.addZeroBefore(eventStartDateObject.getMinutes()) +
      ' - ' +
      this.addZeroBefore(eventEndDateObject.getHours()) +
      ':' +
      this.addZeroBefore(eventEndDateObject.getMinutes()) +
      ', ' +
      this.addZeroBefore(eventStartDateObject.getDate()) +
      ' ' +
      monthNames[eventStartDateObject.getMonth()] +
      ' ' +
      eventStartDateObject.getFullYear()
    );
  };

  render() {
    const { classData } = this.props;

    return (
      <ClassesCategoryWrapper>
        <ClassIconImage src={this.chooseClassIcon(classData.name)} />
        <ClassName>{classData.name}</ClassName>
        <ClassDescription>
          There {classData.available_rooms > 1 ? 'are' : 'is'}{' '}
          <BoldText>
            {classData.available_rooms} topic{classData.available_rooms > 1 ? 's' : ''}
          </BoldText>{' '}
          available in {classData.name}.
        </ClassDescription>
        <AllTopicsSeparator>
          <AllTopicsText>
            All topics (<BoldText>{classData.available_rooms}</BoldText>)
          </AllTopicsText>
        </AllTopicsSeparator>
        {classData.available_rooms === 0 ? (
          <NoRoomsAvailable>No rooms available.</NoRoomsAvailable>
        ) : (
          <ClassItemsList>
            {classData.rooms_list.map(room => (
              <ClassItem key={room.room_id}>
                <ClassItemWrapper>
                  <ClassImageArea>
                    <Link to={`/room/${room.room_id}`}>
                      <ClassImageComponent src={this.getRandomRoomImage()} />
                    </Link>
                  </ClassImageArea>
                  <ClassDetailsArea>
                    <RoomName>{room.name}</RoomName>
                    <RoomDescription>{room.description}</RoomDescription>
                    <ViewButton>
                      <StyledLink to={`/room/${room.room_id}`}>View</StyledLink>
                    </ViewButton>
                    <DateComponent>
                      {this.renderEventDate(room.event_start_time, room.event_end_time)}
                    </DateComponent>
                  </ClassDetailsArea>
                </ClassItemWrapper>
              </ClassItem>
            ))}
          </ClassItemsList>
        )}
      </ClassesCategoryWrapper>
    );
  }
}

ClassesCategory.propTypes = {
  classData: PropTypes.instanceOf(Object).isRequired,
};

export default ClassesCategory;
