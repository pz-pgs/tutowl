import React from 'react';
import Placeholder from 'common/components/Placeholder';
import errorImage from 'images/not_found.svg';

const TITLE = 'No classes found.';
const SUBTITLE = 'There was an error during rooms list fetch.';
const ALT = 'There was an error during rooms list fetch.';
const CUSTOM_CLASS_NAME = 'classes-page-error-placeholder';

const ClassesPageErrorPlaceholder = () => (
  <Placeholder
    src={errorImage}
    alt={ALT}
    title={TITLE}
    subtitle={SUBTITLE}
    customClassName={CUSTOM_CLASS_NAME}
  />
);

export default ClassesPageErrorPlaceholder;
