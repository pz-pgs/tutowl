keytool -import -trustcacerts -keystore /opt/java/jre/lib/security/cacerts -storepass changeit -noprompt -alias mycert -file /tmp/nginx.crt
exec java ${JAVA_OPTS} -noverify -XX:+AlwaysPreTouch -Djava.security.egd=file:/dev/./urandom -cp /app/resources/:/app/classes/:/app/libs/* "pz.pgs.streamapp.StreamApplication"  "$@"
