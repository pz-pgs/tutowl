package pz.pgs.streamapp.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import pz.pgs.streamapp.entity.room.RoomCategory;

import java.time.OffsetDateTime;

@Data
public class RoomBrief {

    @JsonProperty("room_id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("category")
    private RoomCategory category;

    @JsonProperty("event_start_time")
    private OffsetDateTime eventStartDateTime;

    @JsonProperty("event_end_time")
    private OffsetDateTime eventEndDateTime;

    @JsonProperty("is_finished")
    private Boolean is_finished;
}
