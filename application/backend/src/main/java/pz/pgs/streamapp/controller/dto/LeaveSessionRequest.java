package pz.pgs.streamapp.controller.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class LeaveSessionRequest {

    @NotNull
    @NotBlank
    String sessionName;

    @NotNull
    @NotBlank
    String token;

}
