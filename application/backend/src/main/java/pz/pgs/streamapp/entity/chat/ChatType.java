package pz.pgs.streamapp.entity.chat;

public enum ChatType {
    CHAT,
    TYPING,
    STOP_TYPING,
    LEAVE
}
