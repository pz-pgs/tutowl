package pz.pgs.streamapp.entity.notification;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Document("notifications")
@TypeAlias("Notification")
@Data
@NoArgsConstructor
public class Notification {

    private String id;

    @NotNull
    @Field("notification_type")
    private NotificationType notificationType;

    @NotNull
    @Field("author")
    private String author;

    @NotNull
    @Field("content")
    private String content;

    @NotNull
    @Field("read")
    private boolean read;

    @NotNull
    @Field("dateTime")
    private LocalDateTime dateTime;
}
