package pz.pgs.streamapp.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SearchResult {

    @JsonProperty("id")
    private String id;

    @JsonProperty("topText")
    private String topText;

    @JsonProperty("bottomText")
    private String bottomText;

}
