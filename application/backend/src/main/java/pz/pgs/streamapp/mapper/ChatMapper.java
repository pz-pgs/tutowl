package pz.pgs.streamapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pz.pgs.streamapp.controller.dto.ChatMessageRequest;
import pz.pgs.streamapp.entity.chat.ChatMessage;

@Mapper
public interface ChatMapper {

    ChatMapper INSTANCE = Mappers.getMapper(ChatMapper.class);

    ChatMessage createRequestToChatMessage(ChatMessageRequest request);
}
