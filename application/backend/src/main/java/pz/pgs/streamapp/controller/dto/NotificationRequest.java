package pz.pgs.streamapp.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongodb.lang.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pz.pgs.streamapp.entity.notification.NotificationType;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NotificationRequest {
    @NotNull
    @JsonProperty("notification_type")
    private NotificationType notificationType;

    @Nullable
    @JsonProperty("author")
    private String author;

    @Nullable
    @JsonProperty("content")
    private String content;

    @Nullable
    @JsonProperty("read")
    private boolean read;

    @Nullable
    @JsonProperty("dateTime")
    private LocalDateTime dateTime;
}
