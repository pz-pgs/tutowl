package pz.pgs.streamapp.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;

@Data
@AllArgsConstructor
public class RoomCategoryWithRoomsDto {

    @JsonProperty("id")
    private int categoryId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("available_rooms")
    private Long availableRooms;

    @JsonProperty("rooms_list")
    private Collection<RoomBrief> roomsList;
}
