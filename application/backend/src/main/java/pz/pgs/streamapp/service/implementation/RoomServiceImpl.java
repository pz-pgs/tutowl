package pz.pgs.streamapp.service.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pz.pgs.streamapp.controller.dto.*;
import pz.pgs.streamapp.controller.error.exception.RoomAccessDeniedException;
import pz.pgs.streamapp.controller.error.exception.RoomDeletionException;
import pz.pgs.streamapp.controller.error.exception.RoomNotFoundException;
import pz.pgs.streamapp.entity.notification.Notification;
import pz.pgs.streamapp.entity.notification.NotificationType;
import pz.pgs.streamapp.entity.room.Room;
import pz.pgs.streamapp.entity.room.RoomCategory;
import pz.pgs.streamapp.mapper.RoomMapper;
import pz.pgs.streamapp.repository.RoomRepository;
import pz.pgs.streamapp.repository.RoomSort;
import pz.pgs.streamapp.service.NotificationsService;
import pz.pgs.streamapp.service.RoomService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static pz.pgs.streamapp.security.SecurityUtils.getCurrentUsername;

@Service
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {

    private final RoomRepository roomRepository;
    private final RoomMapper roomMapper;
    private final PasswordEncoder encoder;
    private final NotificationsService notificationsService;

    @Override
    public Flux<Room> getPagedListOfRooms(int page, int size, RoomSort sortBy) {
        Sort sort = Sort.by(RoomSort.START_DATE.getVariableNameToSortBy()).ascending();

        if (sortBy != null) {
            sort = Sort.by(sortBy.getVariableNameToSortBy());
        }
        PageRequest pageRequest = PageRequest.of(page, size, sort);

        return roomRepository.findAllByIdNotNull(pageRequest);
    }

    @Override
    public Flux<Room> getListOfRoomsInGivenCategory(int page, int size, RoomSort sortBy, String categoryName) {
        Sort sort = Sort.by(RoomSort.START_DATE.getVariableNameToSortBy());
        if (sortBy != null) {
            sort = Sort.by(sortBy.getVariableNameToSortBy());
        }
        PageRequest pageRequest = PageRequest.of(page, size, sort);

        return roomRepository.findAllByIdNotNullAndCategory(pageRequest, categoryName);
    }

    @Override
    public Mono<Room> createRoom(CreateRoomRequest request, String username) {
        notificationsService.sendNotification(getCurrentUsername(), NotificationType.ROOM_CREATED);

        return Mono.just(roomMapper.createRequestToRoom(request))
                .map(room -> {
                    room.setOwnerUsername(username);
                    room.addParticipant(username);
                    if (request.getPassword() != null) {
                        room.setPassword(encoder.encode(room.getPassword()));
                    }
                    return room;
                })
                .flatMap(roomRepository::save);
    }

    @Override
    public Mono<Room> getRoomDetails(String roomId) {
        return roomRepository
                .findById(roomId)
                .switchIfEmpty(Mono.error(new RoomNotFoundException()));
    }

    @Override
    public Mono<Void> deleteRoom(String roomId, String issuer, boolean isAdmin) {
        notificationsService.sendNotification(getCurrentUsername(), NotificationType.ROOM_DELETED);

        return roomRepository
                .findById(roomId)
                .switchIfEmpty(
                        Mono.error(new RoomNotFoundException())
                )
                .flatMap(room -> {
                    if (isAdmin || room.isOwnedBy(issuer))
                        return roomRepository.deleteById(roomId);
                    else
                        return Mono.error(new RoomDeletionException());
                });
    }

    @Override
    public Mono<Room> update(String roomId, CreateRoomRequest roomRequest, String username) {
        return roomRepository
                .findByIdAndOwnerUsername(roomId, username)
                .switchIfEmpty(Mono.error(new RoomNotFoundException()))
                .flatMap(room -> {
                    Room update = roomMapper.createRequestToRoom(roomRequest);
                    update.setOwnerUsername(room.getOwnerUsername());
                    update.setParticipants(room.getParticipants());
                    update.setId(room.getId());
                    if (update.getPassword() != null) {
                        update.setPassword(encoder.encode(update.getPassword()));
                    }
                    return roomRepository.save(update);
                });
    }

    @Override
    public Mono<Collection<String>> getParticipants(String roomId) {
        return roomRepository
                .findById(roomId)
                .switchIfEmpty(Mono.error(new RoomNotFoundException()))
                .map(Room::getParticipants);
    }

    @Override
    public Mono<Room> joinRoom(String roomId, JoinRoomRequest request, String username) {
        return roomRepository
                .findById(roomId)
                .switchIfEmpty(Mono.error(new RoomNotFoundException()))
                .flatMap(room -> {
                    if (room.isPublic() || isRoomPasswordCorrect(room, request)) {
                        room.addParticipant(username);
                        return roomRepository.save(room);
                    } else {
                        return Mono.error(new RoomAccessDeniedException());
                    }
                });
    }

    @Override
    public Mono<Void> leaveRoom(String roomId, String username) {
        return roomRepository
                .findById(roomId)
                .switchIfEmpty(Mono.error(new RoomNotFoundException()))
                .flatMap(room -> {
                    room.removeParticipant(username);
                    return roomRepository.save(room);
                })
                .flatMap(room -> Mono.empty());
    }

    @Override
    public Mono<Boolean> checkIfRoomExists(String roomName) {
        return roomRepository
                .existsRoomByName(roomName);
    }

    @Override
    public Flux<RoomCategory> getListOfCategories() {
        return Flux.fromIterable(Arrays.asList(RoomCategory.values()));
    }

    @Override
    public Flux<RoomCategoryWithRoomsDto> getCategorizedRooms(int pageSize) {
        Sort sort = Sort.by(RoomSort.START_DATE.getVariableNameToSortBy()).ascending();
        PageRequest page = PageRequest.of(0, 10, sort);

        return Flux.fromArray(RoomCategory.values())
                .map(roomCategory ->
                        new RoomCategoryWithRoomsDto(
                                roomCategory.getId(),
                                roomCategory.getName(),
                                roomRepository.countAllByIdNotNullAndCategory(roomCategory).blockOptional().orElse(0L),
                                roomRepository.findAllByIdNotNullAndCategory(page, roomCategory)
                                        .map(roomMapper::roomToBrief)
                                        .collectList()
                                        .blockOptional()
                                        .orElse(new ArrayList<>())
                        )
                );
    }

    @Override
    public Flux<Room> getCalendarEvents() {
        return roomRepository.findAllByIdNotNull();
    }

    @Override
    public Flux<Room> getByRange(int start, int end) {
        var now = Instant.now();
        var startAfter = now.minus(start, ChronoUnit.HOURS);
        var endBefore = now.plus(start, ChronoUnit.HOURS);

        return roomRepository
                .findAllByIdNotNullAndEventStartDateTimeAfterAndEventEndDateTimeBefore(startAfter, endBefore);
    }

    private boolean isRoomPasswordCorrect(Room room, @Nullable JoinRoomRequest request) {
        return (request != null && encoder.matches(request.getPassword(), room.getPassword()));
    }
}





