package pz.pgs.streamapp.mapper;

import org.jetbrains.annotations.NotNull;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;
import pz.pgs.streamapp.controller.dto.CreateRoomRequest;
import pz.pgs.streamapp.controller.dto.RoomBrief;
import pz.pgs.streamapp.controller.dto.RoomCalendar;
import pz.pgs.streamapp.controller.dto.RoomPreview;
import pz.pgs.streamapp.controller.dto.SearchResult;
import pz.pgs.streamapp.entity.room.Room;
import pz.pgs.streamapp.entity.room.RoomCategory;

@Mapper(uses = TimeMappings.class)
public interface RoomMapper {

    RoomMapper INSTANCE = Mappers.getMapper(RoomMapper.class);

    @Mapping(target = "participants", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "ownerUsername", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    Room createRequestToRoom(CreateRoomRequest request);

    RoomPreview roomToPreview(Room room);

    @Mapping(target = "is_finished")
    RoomBrief roomToBrief(Room room);

    RoomCalendar roomToCalendar(Room room);

    @AfterMapping
    default void addIsFinished(@NotNull Room room, @NotNull @MappingTarget RoomBrief brief){
        brief.setIs_finished(room.isFinished());
    }

    default SearchResult roomToSearchResult(Room room) {
        if(room == null)
            return null;

        SearchResult searchResult = new SearchResult();

        searchResult.setId(room.getId());
        searchResult.setTopText(room.getName());
        searchResult.setBottomText(room.getDescription());

        return searchResult;
    }

    default SearchResult roomRoomCategoryToSearchResult(RoomCategory roomCategory) {
        if(roomCategory == null)
            return null;

        SearchResult searchResult = new SearchResult();

        searchResult.setId(String.valueOf(roomCategory.getId()));
        searchResult.setTopText(roomCategory.getName());
        searchResult.setBottomText("Amanda Green");

        return searchResult;
    }
}
