package pz.pgs.streamapp.mapper;

import pz.pgs.streamapp.controller.dto.SearchResult;

import org.keycloak.representations.idm.UserRepresentation;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.UUID;

@Mapper
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    default SearchResult userRepresentationToSearchResult(UserRepresentation userRepresentation) {
        if(userRepresentation == null)
            return null;

        SearchResult searchResult = new SearchResult();

        searchResult.setId(UUID.randomUUID().toString());
        searchResult.setTopText(userRepresentation.getUsername());
        searchResult.setBottomText(userRepresentation.getEmail());

        return searchResult;
    }
}
