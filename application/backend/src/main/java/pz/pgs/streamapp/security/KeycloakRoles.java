package pz.pgs.streamapp.security;

public final class KeycloakRoles {
    public static final String ROLE_STUDENT = "ROLE_live-student";
    public static final String ROLE_TEACHER = "ROLE_live-teacher";
    public static final String ROLE_ADMIN = "ROLE_live-admin";

    private KeycloakRoles() {
        throw new IllegalStateException("Utility class");
    }
}

