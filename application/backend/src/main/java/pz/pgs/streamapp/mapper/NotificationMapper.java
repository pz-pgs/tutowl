package pz.pgs.streamapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pz.pgs.streamapp.controller.dto.NotificationItem;
import pz.pgs.streamapp.controller.dto.NotificationRequest;
import pz.pgs.streamapp.entity.notification.Notification;

@Mapper
public interface NotificationMapper {
    NotificationMapper INSTANCE = Mappers.getMapper(NotificationMapper.class);

    NotificationItem createNotificationToNotificationItem(Notification notification);

    Notification createNotificationRequestToNotification(NotificationRequest notificationRequest);
}
