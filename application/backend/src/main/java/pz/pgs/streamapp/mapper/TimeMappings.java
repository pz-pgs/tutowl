package pz.pgs.streamapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;

@Mapper
public interface TimeMappings {

    TimeMappings INSTANCE = Mappers.getMapper(TimeMappings.class);

    default Instant offsetDateTimeToInstant(OffsetDateTime date){
        return date.toInstant();
    }

    default OffsetDateTime instantToOffsetDateTime(Instant instant){
        return OffsetDateTime.ofInstant(instant, ZoneId.of("Z"));
    }
}
