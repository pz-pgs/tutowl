package pz.pgs.streamapp.controller.dto;

import io.openvidu.java.client.OpenViduRole;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
public class SessionRequest {

    @NotNull
    @NotBlank
    String username;

    @NotNull
    @NotBlank
    String sessionName;

    @NotNull
    OpenViduRole role;
}
