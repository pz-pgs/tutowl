package pz.pgs.streamapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pz.pgs.streamapp.controller.dto.LeaveSessionRequest;
import pz.pgs.streamapp.controller.dto.SessionRequest;
import pz.pgs.streamapp.service.OpenViduService;
import pz.pgs.streamapp.utils.Logger;

import javax.validation.Valid;

import static pz.pgs.streamapp.security.KeycloakRoles.ROLE_STUDENT;
import static pz.pgs.streamapp.security.SecurityUtils.getCurrentUsername;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/session")
public class OpenViduController {

    private static final Logger LOGGER = new Logger();

    private final OpenViduService openViduService;

    @Secured(ROLE_STUDENT)
    @PostMapping("/join")
    public ResponseEntity<?> getToken(@RequestBody @Valid SessionRequest sessionRequest) {
        LOGGER.info("Getting a token from OpenVidu Server by user {} | Session name: {}.",
                    getCurrentUsername(), sessionRequest.getSessionName());

        return openViduService.getToken(sessionRequest);
    }

    @Secured(ROLE_STUDENT)
    @PostMapping("/leave")
    public ResponseEntity<?> removeUser(@RequestBody LeaveSessionRequest leaveSessionRequest) {
        LOGGER.info("Removing a user {} from session | Session name: {}.",
                    getCurrentUsername(), leaveSessionRequest.getSessionName());

        return openViduService.removeUser(leaveSessionRequest);
    }
}
