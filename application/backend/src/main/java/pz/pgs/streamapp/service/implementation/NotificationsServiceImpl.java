package pz.pgs.streamapp.service.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import pz.pgs.streamapp.controller.dto.NotificationItem;
import pz.pgs.streamapp.controller.dto.NotificationRequest;
import pz.pgs.streamapp.entity.notification.Notification;
import pz.pgs.streamapp.entity.notification.NotificationType;
import pz.pgs.streamapp.mapper.NotificationMapper;
import pz.pgs.streamapp.repository.NotificationRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import pz.pgs.streamapp.utils.Logger;

import java.time.LocalDateTime;

import static pz.pgs.streamapp.security.SecurityUtils.getCurrentUsername;

@Service
@RequiredArgsConstructor
public class NotificationsServiceImpl implements pz.pgs.streamapp.service.NotificationsService {

    private static final Logger LOGGER = new Logger();

    private final NotificationRepository notificationRepository;
    private final NotificationMapper notificationMapper;
    private final SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public Flux<NotificationItem> getNotificationsList() {
        return notificationRepository.findAllByIdNotNullAndAuthorNotOrderByDateTimeDesc(getCurrentUsername())
                .map(notificationMapper::createNotificationToNotificationItem);
    }

    @Override
    public Notification sendNotification(String author, NotificationType notificationType) {
        NotificationRequest notificationRequest = createNotificationRequest(author, notificationType);

        simpMessagingTemplate.convertAndSend("/notify", notificationRequest);

        LOGGER.info("Adding new notification by user {}.", getCurrentUsername());

        return notificationRepository
                .save(notificationMapper.createNotificationRequestToNotification(notificationRequest)).block();
    }

    @Override
    public Mono<Boolean> checkIfAnyNewNotifications() {
        return notificationRepository.existsByIdNotNullAndReadFalseAndAuthorNot(getCurrentUsername());
    }

    private NotificationRequest createNotificationRequest(String author, NotificationType notificationType) {
        String notificationContent = createNotificationContent(notificationType);

        return new NotificationRequest(
            notificationType,
            author,
            notificationContent,
            false,
            LocalDateTime.now()
        );
    }

    private String createNotificationContent(NotificationType notificationType) {
        switch(notificationType) {
            case ROOM_CREATED:
                return "has just created a room";
            case ROOM_DELETED:
                return "has just deleted a room";
            default:
                return "error";
        }
    }
}
