package pz.pgs.streamapp.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pz.pgs.streamapp.entity.notification.NotificationType;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NotificationItem {
    @JsonProperty("id")
    private String id;

    @JsonProperty("notification_type")
    private NotificationType notificationType;

    @JsonProperty("author")
    private String author;

    @JsonProperty("content")
    private String content;

    @JsonProperty("read")
    private boolean read;

    @JsonProperty("dateTime")
    private LocalDateTime dateTime;
}
