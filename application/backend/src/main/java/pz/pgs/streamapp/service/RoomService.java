package pz.pgs.streamapp.service;

import org.springframework.lang.Nullable;
import pz.pgs.streamapp.controller.dto.RoomCalendar;
import pz.pgs.streamapp.controller.dto.RoomCategoryWithRoomsDto;
import pz.pgs.streamapp.controller.dto.CreateRoomRequest;
import pz.pgs.streamapp.controller.dto.JoinRoomRequest;
import pz.pgs.streamapp.entity.room.Room;
import pz.pgs.streamapp.entity.room.RoomCategory;
import pz.pgs.streamapp.repository.RoomSort;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.Optional;

public interface RoomService {
    Flux<Room> getPagedListOfRooms(int page, int size, @Nullable RoomSort sortBy);

    Flux<Room> getListOfRoomsInGivenCategory(int page, int size, @Nullable RoomSort sortBy, String categoryName);

    Mono<Room> createRoom(CreateRoomRequest request, String username);

    Mono<Room> getRoomDetails(String roomId);

    Mono<Void> deleteRoom(String roomId, String issuer, boolean isAdmin);

    Mono<Room> update(String roomId, CreateRoomRequest roomRequest, String username);

    Mono<Collection<String>> getParticipants(String roomId);

    Mono<Room> joinRoom(String roomId, JoinRoomRequest request, String username);

    Mono<Void> leaveRoom(String roomId, String username);

    Mono<Boolean> checkIfRoomExists(String roomName);

    Flux<RoomCategory> getListOfCategories();

    Flux<RoomCategoryWithRoomsDto> getCategorizedRooms(int pageSize);

    Flux<Room> getCalendarEvents();

    Flux<Room> getByRange(int start, int end);
}
