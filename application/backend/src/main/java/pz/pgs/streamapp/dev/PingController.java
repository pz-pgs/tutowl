package pz.pgs.streamapp.dev;


import org.springframework.context.annotation.Profile;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import static pz.pgs.streamapp.security.KeycloakRoles.ROLE_STUDENT;

@RestController
@Profile("dev")
public class PingController {

    @Secured(ROLE_STUDENT)
    @GetMapping("/api/ping")
    public Mono<String> pong() {
        return Mono.just("pong!");
    }

    @GetMapping("/api/test")
    public Mono<String> test() { return Mono.just("test"); }

}
