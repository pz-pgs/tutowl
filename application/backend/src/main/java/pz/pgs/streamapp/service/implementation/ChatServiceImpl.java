package pz.pgs.streamapp.service.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import pz.pgs.streamapp.controller.dto.ChatMessageRequest;
import pz.pgs.streamapp.controller.dto.ChatMessageItem;
import pz.pgs.streamapp.entity.chat.ChatMessage;
import pz.pgs.streamapp.entity.chat.ChatType;
import pz.pgs.streamapp.mapper.ChatMapper;
import pz.pgs.streamapp.repository.ChatRepository;
import pz.pgs.streamapp.service.ChatService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import pz.pgs.streamapp.utils.Logger;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static pz.pgs.streamapp.security.SecurityUtils.getCurrentUsername;

@Service
@RequiredArgsConstructor
public class ChatServiceImpl implements ChatService {

    private static final Logger LOGGER = new Logger();

    private final SimpMessagingTemplate simpMessagingTemplate;
    private final ChatRepository chatRepository;
    private final ChatMapper chatMapper;

    @Override
    public Mono<ChatMessage> sendPrivateMessage(ChatMessageRequest chatMessageRequest) {
        ChatMessage chatMessage = chatMapper.createRequestToChatMessage(chatMessageRequest);

        simpMessagingTemplate.convertAndSendToUser(
                chatMessage.getReceiver().trim(), "/reply", chatMessage);

        if(chatMessage.getType() == ChatType.TYPING || chatMessage.getType() == ChatType.STOP_TYPING) {
            return null;
        }

        return chatRepository.save(chatMessage);
    }

    @Override
    public Flux<ChatMessage> getChatHistoryWithUser(String username) {
        return chatRepository.findAllBySenderEqualsAndReceiverEquals(getCurrentUsername(), username)
                .concatWith(chatRepository.findAllBySenderEqualsAndReceiverEquals(username, getCurrentUsername()))
                .sort((obj1, obj2) -> obj1.getDateTime().compareTo(obj2.getDateTime()));
    }

    @Override
    public Mono<Integer> countUnreadMessages() {
        return chatRepository.countAllByIdNotNullAndReceiverEqualsAndReadEquals(getCurrentUsername(), false);
    }

    @Override
    public Flux<ChatMessage> markMessagesWithUserAsRead(String username) {
        return chatRepository
                .findAllByIdNotNullAndReceiverEqualsAndSenderEqualsAndReadEquals(getCurrentUsername(), username, false)
                .map(chatMessage -> { chatMessage.setRead(true); return chatMessage;}).flatMap(chatRepository::save);
    }

    @Override
    public List<ChatMessageItem> getMessagesList() {
        Flux<ChatMessage> messagesList =
                chatRepository.findAllByReceiverEqualsOrSenderEqualsOrderByDateTimeDesc(
                        getCurrentUsername(), getCurrentUsername());

        List<ChatMessage> filteredList = messagesList.collectList().block().stream()
                .filter(distinctByKeys(ChatMessage::getReceiver, ChatMessage::getSender))
                .collect(Collectors.toList());

        List<ChatMessage> toDelete = new ArrayList<>();

        for (ChatMessage message : filteredList) {
            for (ChatMessage messageInner : filteredList) {
                if (message.getSender().equals(messageInner.getReceiver())
                        && message.getReceiver().equals(messageInner.getSender())) {
                    if (message.getDateTime().isBefore(messageInner.getDateTime())) {
                        toDelete.add(message);
                    } else if (messageInner.getDateTime().isBefore(message.getDateTime())) {
                        toDelete.add(messageInner);
                    }
                }
            }
        }

        filteredList.removeAll(toDelete);

        for (ChatMessage message : filteredList) {
            if(message.getSender().equals(getCurrentUsername())) {
                message.setSender(message.getReceiver());
            }
        }

        List<ChatMessageItem> returnList = new ArrayList<>();

        boolean anyMessageUnRead = chatRepository.
                existsByIdNotNullAndReadEqualsAndReceiverEquals(false, getCurrentUsername()).block();

        for(ChatMessage message : filteredList){
            returnList.add(new ChatMessageItem(
                    message.getId(),
                    message.getSender(),
                    message.getDateTime(),
                    message.getContent(),
                    anyMessageUnRead,
                    true
            ));
        }

        return returnList;
    }

    private static <T> Predicate<T> distinctByKeys(Function<? super T, ?>... keyExtractors) {
        final Map<List<?>, Boolean> seen = new ConcurrentHashMap<>();

        return t -> {
            final List<?> keys = Arrays.stream(keyExtractors)
                    .map(ke -> ke.apply(t))
                    .collect(Collectors.toList());

            return seen.putIfAbsent(keys, Boolean.TRUE) == null;
        };
    }
}
