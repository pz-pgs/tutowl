package pz.pgs.streamapp.controller;

import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.resource.UsersResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pz.pgs.streamapp.controller.dto.UserDto;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UsersController {

    private final UsersResource resource;

    @GetMapping("/")
    Flux<UserDto> getAll(
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "first", defaultValue = "0") Integer firstResult,
            @RequestParam(value = "max", defaultValue = "15") Integer maxResults
    ) {
        return Flux.fromIterable(
                resource.search(search, firstResult, maxResults)
        )
                .map(usr ->
                        new UserDto(
                                usr.getFirstName(),
                                usr.getLastName(),
                                usr.getUsername()
                        )
                );
    }
}


