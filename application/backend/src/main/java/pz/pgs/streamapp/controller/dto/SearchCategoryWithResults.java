package pz.pgs.streamapp.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;

@Data
@AllArgsConstructor
public class SearchCategoryWithResults {

    @JsonProperty("id")
    private int categoryId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("count")
    private Long count;

    @JsonProperty("list")
    private Collection<SearchResult> list;
}
