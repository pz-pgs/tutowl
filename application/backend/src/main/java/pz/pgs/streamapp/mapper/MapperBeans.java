package pz.pgs.streamapp.mapper;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperBeans {

    @Bean
    public RoomMapper roomMapper(){
        return RoomMapper.INSTANCE;
    }

    @Bean
    public RoomCategoryMapper roomCategoryMapper(){
        return RoomCategoryMapper.INSTANCE;
    }

    @Bean
    public ChatMapper chatMapper() { return ChatMapper.INSTANCE; }

    @Bean
    public NotificationMapper notificationMapper() { return NotificationMapper.INSTANCE; }

    @Bean
    public UserMapper userMapper() { return UserMapper.INSTANCE; }
}
