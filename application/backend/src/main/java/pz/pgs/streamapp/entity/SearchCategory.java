package pz.pgs.streamapp.entity;

import javax.validation.constraints.NotNull;

public enum SearchCategory {
    CATEGORIES(0),
    PEOPLE(1),
    ROOMS(2);

    private final int id;
    private final String name;

    SearchCategory(int id){
        this.id = id;
        this.name = this.name();
    }

    @NotNull
    public int getId() {
        return id;
    }

    @NotNull
    public String getName(){
        return name;
    }
}