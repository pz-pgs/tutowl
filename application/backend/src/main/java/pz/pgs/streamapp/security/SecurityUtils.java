package pz.pgs.streamapp.security;

import org.keycloak.KeycloakPrincipal;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * DO NOT USE inside Reactive Streams
 */
public final class SecurityUtils {

    public static String getCurrentUsername() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal == null) {
            throw new AuthorizationServiceException("User not authorized!");
        }
        if (principal instanceof KeycloakPrincipal) {
            return ((KeycloakPrincipal) principal).getName();
        } else if (principal instanceof String) {
            return (String) principal;
        } else if (principal instanceof UserDetails) {
            return ((UserDetails) principal).getUsername();
        } else {
            throw new AuthorizationServiceException("Incorrect authentication principal type!");
        }

    }

    public static boolean hasCurrentUserRole(String role) {
        return getRoles()
                .stream()
                .map(String::toLowerCase)
                .collect(Collectors.toList())
                .contains(
                        // Role here in collection looks exactly like in Keycloak service
                        // they are not mapped to Springs 'ROLE_' convention
                        role
                                .replace("ROLE_", "")
                                .toLowerCase()
                );
    }

    private static Collection<String> getRoles() {
        return getKeycloakPrincipal()
                .getKeycloakSecurityContext()
                .getToken()
                .getRealmAccess()
                .getRoles();
    }

    private static KeycloakPrincipal getKeycloakPrincipal() {
        return (KeycloakPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    private SecurityUtils() {
        throw new IllegalStateException("Utility class");
    }
}
