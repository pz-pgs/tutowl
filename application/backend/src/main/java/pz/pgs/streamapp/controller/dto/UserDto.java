package pz.pgs.streamapp.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.lang.Nullable;

@Data
@AllArgsConstructor
public class UserDto {

    @Nullable
    @JsonProperty("first_name")
    String firstName;

    @Nullable
    @JsonProperty("last_name")
    String lastName;

    @Nullable
    @JsonProperty("username")
    String username;
}
