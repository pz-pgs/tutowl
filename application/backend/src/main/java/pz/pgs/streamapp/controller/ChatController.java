package pz.pgs.streamapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.*;

import pz.pgs.streamapp.controller.dto.ChatMessageItem;
import pz.pgs.streamapp.controller.dto.ChatMessageRequest;
import pz.pgs.streamapp.entity.chat.ChatMessage;
import pz.pgs.streamapp.service.ChatService;
import pz.pgs.streamapp.utils.Logger;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static pz.pgs.streamapp.security.SecurityUtils.getCurrentUsername;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/chat")
public class ChatController {
    private static final Logger LOGGER = new Logger();

    private final ChatService chatService;

    @MessageMapping("/sendPrivateMessage")
    public Mono<ChatMessage> sendPrivateMessage(@Payload ChatMessageRequest chatMessageRequest) {
        LOGGER.info("Sending private message with content: {}.", chatMessageRequest.getContent());

        return chatService.sendPrivateMessage(chatMessageRequest);
    }

    @GetMapping(value="/messages", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ChatMessageItem> getMessagesList() {
        LOGGER.info("Getting messages list for user {}.", getCurrentUsername());

        return chatService.getMessagesList();
    }

    @GetMapping(value="/history", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<ChatMessage> getChatHistoryWithUser(@RequestParam(value = "username") String username) {
        LOGGER.info("Getting chat history for users {} and {}.", username, getCurrentUsername());

        return chatService.getChatHistoryWithUser(username);
    }

    @GetMapping(value="/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Integer> countUnreadMessages() {
        LOGGER.info("Counting unread messages by user {}.", getCurrentUsername());

        return chatService.countUnreadMessages();
    }

    @GetMapping(value="/read", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<ChatMessage> markMessagesWithUserAsRead(@RequestParam(value = "username") String username) {
        LOGGER.info("Marking messages as read by user {} with user {}.", getCurrentUsername(), username);

        return chatService.markMessagesWithUserAsRead(username);
    }
}