package pz.pgs.streamapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pz.pgs.streamapp.controller.dto.CreateRoomRequest;
import pz.pgs.streamapp.controller.dto.JoinRoomRequest;
import pz.pgs.streamapp.controller.dto.RoomBrief;
import pz.pgs.streamapp.controller.dto.RoomCalendar;
import pz.pgs.streamapp.controller.dto.RoomCategoryWithRoomsDto;
import pz.pgs.streamapp.controller.dto.RoomPreview;
import pz.pgs.streamapp.controller.error.exception.RoomAccessDeniedException;
import pz.pgs.streamapp.controller.error.exception.RoomDeletionException;
import pz.pgs.streamapp.controller.error.exception.RoomNotFoundException;
import pz.pgs.streamapp.entity.room.RoomCategory;
import pz.pgs.streamapp.mapper.RoomMapper;
import pz.pgs.streamapp.repository.RoomSort;
import pz.pgs.streamapp.service.RoomService;
import pz.pgs.streamapp.utils.Logger;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;

import static pz.pgs.streamapp.security.KeycloakRoles.ROLE_ADMIN;
import static pz.pgs.streamapp.security.KeycloakRoles.ROLE_STUDENT;
import static pz.pgs.streamapp.security.KeycloakRoles.ROLE_TEACHER;
import static pz.pgs.streamapp.security.SecurityUtils.getCurrentUsername;
import static pz.pgs.streamapp.security.SecurityUtils.hasCurrentUserRole;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/room")
public class RoomController {

    private static final Logger LOGGER = new Logger();

    private final RoomService roomService;
    private final RoomMapper roomMapper = RoomMapper.INSTANCE;

    /**
     * Returns available rooms.
     *
     * @param page   number of requested page starting from 0. Default: 0
     * @param size   number of entities per page. Default 10
     * @param sortBy type of sorting. Default: sort by name
     */
    @Secured(ROLE_STUDENT)
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<RoomPreview> getPagedListOfRooms(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,
            @RequestParam(value = "sort_by", required = false) RoomSort sortBy
    ) {
        LOGGER.info("Fetching list of pages: size: {}, page: {}.", size, page);

        return roomService
                .getPagedListOfRooms(page, size, sortBy)
                .map(roomMapper::roomToPreview);
    }

    @Secured({ROLE_STUDENT})
    @GetMapping(value = "/")
    public Flux<RoomCategoryWithRoomsDto> getCategorizedRooms(
            @RequestParam(value = "size", defaultValue = "10") int size
    ) {
        return roomService
                .getCategorizedRooms(size);
    }

    @Secured({ROLE_STUDENT})
    @GetMapping(value = "/calendar")
    public Flux<RoomCalendar> getCalendarEvents() {
        return roomService
                .getCalendarEvents()
                .map(roomMapper::roomToCalendar);
    }

    /**
     * Creates and saves new room. Issuer is automatically added as participant.
     *
     * @param request data required to create room
     * @return HttpStatus 201 - id of newly created room
     */
    @ResponseStatus(HttpStatus.CREATED)
    @Secured(ROLE_TEACHER)
    @PostMapping("/")
    public Mono<RoomPreview> createRoom(
            @RequestBody @Valid CreateRoomRequest request
    ) {
        LOGGER.info("Creating new room by user {} with name: {}.", getCurrentUsername(), request.getName());

        return roomService
                .createRoom(request, getCurrentUsername())
                .map(roomMapper::roomToPreview);
    }

    /**
     * Returns details of requested Room
     *
     * @param roomId id of requested Room
     * @return HttpStatus 200 - data of Room
     * @throws RoomNotFoundException HttpStatus 404 - if requested room does not exist
     */
    @Secured(ROLE_STUDENT)
    @GetMapping("/{roomId}")
    public Mono<RoomPreview> getRoomDetails(
            @PathVariable("roomId") @NotNull String roomId
    ) {
        LOGGER.info("Fetching room details with id {} by user with name: {}.",
                roomId, getCurrentUsername());

        return roomService
                .getRoomDetails(roomId)
                .map(roomMapper::roomToPreview);
    }

    /**
     * Deletes given room. Issuer must be an Admin or owner of this room.
     *
     * @param roomId id of room to be deleted
     * @return Void HttpStatus 204 - No Content on successful deletion
     * @throws RoomDeletionException HttpStatus 400 - When user does not own room or is not an Admin
     * @throws RoomNotFoundException HttpStatus 404 - When given roomId was not found
     */
    @Secured(ROLE_TEACHER)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{roomId}")
    public Mono<Void> deleteRoom(
            @PathVariable("roomId") @NotNull String roomId
    ) {
        LOGGER.info("Deleting room with id {} by user with name {}.", roomId, getCurrentUsername());

        // Not so reactive.. because we have no ReactiveContext
        boolean isAdmin = hasCurrentUserRole(ROLE_ADMIN);
        String currentUsername = getCurrentUsername();
        return roomService.deleteRoom(roomId, currentUsername, isAdmin);
    }

    /**
     * Updates Room with given information
     *
     * @param roomId      id of room to be updated
     * @param roomRequest data to be applied
     * @return HttpStatus 200 - if Room has been updated
     * @throws RoomNotFoundException HttpStatus 404 - when given roomId was not found or issuer is not an owner of that Room
     */
    @Secured(ROLE_STUDENT)
    @PutMapping("/{roomId}")
    public Mono<RoomPreview> updateRoom(
            @PathVariable @NotNull String roomId,
            @RequestBody @Valid CreateRoomRequest roomRequest
    ) {
        LOGGER.info("Updating room with id {} by user with name {}.", roomId, getCurrentUsername());

        return roomService
                .update(roomId, roomRequest, getCurrentUsername())
                .map(roomMapper::roomToPreview);
    }

    /**
     * Returns list of participants usernames
     *
     * @param roomId id of room to be updated
     * @return HttpStatus 200 - list of usernames
     * @throws RoomNotFoundException HttpStatus 404 - when given roomId was not found
     */
    @Secured(ROLE_STUDENT)
    @GetMapping("/{roomId}/participants")
    public Mono<Collection<String>> getParticipants(
            @PathVariable @NotNull String roomId
    ) {
        LOGGER.info("Fetching participants from room with id {} by user with name {}.", roomId, getCurrentUsername());

        return roomService
                .getParticipants(roomId);
    }

    /**
     * Join given room
     *
     * @param roomId id of room to be updated
     * @return HttpStatus 200 - RoomPreview with added issuer
     * @throws RoomNotFoundException     HttpStatus 404 - when given roomId was not found
     * @throws RoomAccessDeniedException HttpStatus 403 - when given provided incorrect password for private room
     */
    @Secured(ROLE_STUDENT)
    @PostMapping("/{roomId}/join")
    public Mono<RoomPreview> joinRoom(
            @PathVariable @NotNull String roomId,
            @RequestBody @Valid JoinRoomRequest request
    ) {
        LOGGER.info("Joining room with id {} by user with name {}.", roomId, getCurrentUsername());

        return roomService
                .joinRoom(roomId, request, getCurrentUsername())
                .map(roomMapper::roomToPreview);
    }

    /**
     * Leave given room
     *
     * @param roomId id of room to be updated
     * @return HttpStatus 200 - empty response
     * @throws RoomNotFoundException HttpStatus 404 - when given roomId was not found
     */
    @Secured(ROLE_STUDENT)
    @PostMapping("/{roomId}/leave")
    public Mono<Void> leaveRoom(
            @PathVariable @NotNull String roomId
    ) {
        LOGGER.info("Leaving room with id {} by user with name {}.", roomId, getCurrentUsername());

        return roomService
                .leaveRoom(roomId, getCurrentUsername());
    }

    /**
     * Check if Room with given name exists
     *
     * @param roomName name to be checked
     * @return true - if Room exists or
     * false - if Room does not exist
     */
    @Secured(ROLE_TEACHER)
    @GetMapping("/{roomName}/exist")
    public Mono<Boolean> checkIfNameExists(
            @PathVariable String roomName
    ) {
        LOGGER.info("Checking if room with name {} exists by user with name {}.", roomName, getCurrentUsername());

        return roomService
                .checkIfRoomExists(roomName);
    }

    /**
     * Returns available categories.
     */
    @Secured(ROLE_STUDENT)
    @GetMapping(value = "/category", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<RoomCategory> getListOfCategories() {
        LOGGER.info("Fetching list of available room categories by user with name {}.", getCurrentUsername());

        return roomService
                .getListOfCategories();
    }

    /**
     * Returns all room within given category
     *
     * @param categoryName name of the category
     * @return list of rooms in given category
     */
    @Secured(ROLE_STUDENT)
    @GetMapping(value = "/category/{categoryName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<RoomPreview> getListOfRoomsInGivenCategory(
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "10") int size,
            @RequestParam(value = "sort_by", required = false) RoomSort sortBy,
            @PathVariable("categoryName") @NotNull String categoryName
    ) {
        LOGGER.info("Fetching list of available room in {} category by user with name {}.",
                categoryName, getCurrentUsername());

        return roomService
                .getListOfRoomsInGivenCategory(page, size, sortBy, categoryName)
                .map(roomMapper::roomToPreview);
    }


    /**
     * Returns list of events within given range of hours. By default 5.
     */
    @Secured(ROLE_STUDENT)
    @GetMapping(value = "/range")
    public Flux<RoomBrief> getListOfEventsWithinRange(
            @NumberFormat @RequestParam(value = "start", defaultValue = "2") int start,
            @NumberFormat @RequestParam(value = "end", defaultValue = "2") int end
    ) {
        LOGGER.info("Fetching list of available room within range start:{}, end: {}, user: {}.", start, end, getCurrentUsername());

        return roomService
                .getByRange(start, end)
                .map(roomMapper::roomToBrief);
    }
}
