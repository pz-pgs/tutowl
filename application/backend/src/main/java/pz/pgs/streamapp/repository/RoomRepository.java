package pz.pgs.streamapp.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import pz.pgs.streamapp.entity.room.Room;
import pz.pgs.streamapp.entity.room.RoomCategory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Repository
public interface RoomRepository extends ReactiveMongoRepository<Room, String> {

    Flux<Room> findAllByIdNotNull();

    Flux<Room> findAllByIdNotNull(Pageable pageable);

    Flux<Room> findAllByIdNotNullAndCategory(Pageable page, RoomCategory category);

    // TODO map category in service to enum
    Flux<Room> findAllByIdNotNullAndCategory(Pageable page, String category);

    Mono<Long> countAllByIdNotNullAndCategory(RoomCategory category);

    Mono<Room> findByIdAndOwnerUsername(
            @NotNull String id,
            @NotNull String username
    );

    @SuppressWarnings("SpringDataRepositoryMethodReturnTypeInspection")
    Mono<Boolean> existsRoomByName(String name);

    Flux<Room> findAllByIdNotNullAndEventStartDateTimeAfterAndEventEndDateTimeBefore(Instant start, Instant end);

    Flux<Room> findAllByIdNotNullAndNameContainingIgnoreCaseOrDescriptionContainingIgnoreCaseOrCategoryIsContainingIgnoreCase(
            Pageable pageable, String name, String description, String category);

    Mono<Long> countAllByIdNotNullAndNameContainingIgnoreCaseOrDescriptionContainingIgnoreCaseOrCategoryIsContainingIgnoreCase(
            String name, String description, String category);
}