package pz.pgs.streamapp.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import pz.pgs.streamapp.controller.dto.RoomCategoryWithRoomsDto;
import pz.pgs.streamapp.entity.room.RoomCategory;

@Mapper
public interface RoomCategoryMapper {

    RoomCategoryMapper INSTANCE = Mappers.getMapper(RoomCategoryMapper.class);

//    @Mapping(source = "name", target = "name")
//    @Mapping(source = "id", target = "categoryId")
//    @Mapping(target = "availableRooms", ignore = true)
//    @Mapping(target = "roomsList", ignore = true)
//    RoomCategoryWithRoomsDto categoryToDto(RoomCategory roomCategory);
}
