package pz.pgs.streamapp.service;

import pz.pgs.streamapp.controller.dto.SearchCategoryWithResults;
import reactor.core.publisher.Flux;

public interface SearchService {
    Flux<SearchCategoryWithResults> searchWithGivenQuery(String searchQuery);
}
