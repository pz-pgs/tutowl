package pz.pgs.streamapp.entity.notification;

import javax.validation.constraints.NotNull;

public enum NotificationType {
    ROOM_CREATED(1),
    ROOM_DELETED(2);

    private final int id;
    private final String name;

    NotificationType(int id){
        this.id = id;
        this.name = this.name();
    }

    @NotNull
    public int getId() {
        return id;
    }

    @NotNull
    public String getName(){
        return name;
    }
}
