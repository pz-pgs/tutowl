package pz.pgs.streamapp.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import pz.pgs.streamapp.entity.notification.Notification;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface NotificationRepository extends ReactiveMongoRepository<Notification, String> {
    Flux<Notification> findAllByIdNotNullAndAuthorNotOrderByDateTimeDesc(String author);
    Mono<Boolean> existsByIdNotNullAndReadFalseAndAuthorNot(String author);
}
