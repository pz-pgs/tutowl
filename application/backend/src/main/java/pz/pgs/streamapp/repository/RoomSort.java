package pz.pgs.streamapp.repository;

public enum RoomSort {

    NAME("name"),
    CATEGORY("category"),
    START_DATE("eventStartDate");

    private final String variableNameToSortBy;

    RoomSort(String variableNameToSortBy){
        this.variableNameToSortBy = variableNameToSortBy;
    }

    public String getVariableNameToSortBy(){
        return variableNameToSortBy;
    }
}
