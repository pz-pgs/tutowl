package pz.pgs.streamapp.controller.error.exception;

import org.springframework.http.HttpStatus;

public class RoomDeletionException extends DomainException {

    public RoomDeletionException(){
        super(HttpStatus.BAD_REQUEST, "You do not own this room or have admin authority.");
    }
}
