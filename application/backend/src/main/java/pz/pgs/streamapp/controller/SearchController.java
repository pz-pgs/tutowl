package pz.pgs.streamapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pz.pgs.streamapp.controller.dto.SearchCategoryWithResults;
import pz.pgs.streamapp.service.SearchService;
import pz.pgs.streamapp.utils.Logger;
import reactor.core.publisher.Flux;

import static pz.pgs.streamapp.security.KeycloakRoles.ROLE_STUDENT;
import static pz.pgs.streamapp.security.SecurityUtils.getCurrentUsername;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/search")
public class SearchController {

    private static final Logger LOGGER = new Logger();

    private final SearchService searchService;

    @Secured(ROLE_STUDENT)
    @GetMapping()
    public Flux<SearchCategoryWithResults> searchWithGivenQuery(
            @RequestParam(value = "searchQuery") String searchQuery) {
        LOGGER.info("Searching by user '{}' with query '{}'", getCurrentUsername(), searchQuery);

        return searchService.searchWithGivenQuery(searchQuery);
    }
}
