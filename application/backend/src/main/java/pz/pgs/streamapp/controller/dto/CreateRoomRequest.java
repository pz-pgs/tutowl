package pz.pgs.streamapp.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.lang.Nullable;
import pz.pgs.streamapp.entity.room.RoomCategory;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.time.OffsetDateTime;

import static pz.pgs.streamapp.entity.ValidationConstants.MAX_ROOM_NAME_LENGTH;
import static pz.pgs.streamapp.entity.ValidationConstants.MIN_ROOM_NAME_LENGTH;

@Data
public class CreateRoomRequest {

    @NotNull
    @NotBlank
    @Size(
            min = MIN_ROOM_NAME_LENGTH,
            max = MAX_ROOM_NAME_LENGTH
    )
    @JsonProperty("name")
    private String name;

    @Nullable
    @JsonProperty("description")
    private String description;

    @NotNull
    @JsonProperty("is_public")
    private boolean isPublic;

    @Nullable
    @JsonProperty("password")
    private String password;

    @NotNull
    @JsonProperty("category")
    private RoomCategory category;

    /** Application by default use UTC across application.
     *
     * You can provide dates with offset: e.g. 2020-04-20T13:00:00+01:00
     * Which will be correctly translated to UTC: e.g. 2020-04T12:00:00+00:00
     * Application also returns UTC, so the frontend needs to convert it to
     * client's local zone.
     * */
    @NotNull
    @JsonProperty("event_start_date")
    private OffsetDateTime eventStartDateTime;

    @NotNull
    @JsonProperty("event_end_date")
    private OffsetDateTime eventEndDateTime;
}
