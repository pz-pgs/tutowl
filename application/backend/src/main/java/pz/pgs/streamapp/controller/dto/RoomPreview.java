package pz.pgs.streamapp.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import pz.pgs.streamapp.entity.room.RoomCategory;

import java.time.OffsetDateTime;
import java.util.Collection;

@Data
public class RoomPreview {

    @JsonProperty("id")
    private String id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("owner")
    private String ownerUsername;

    @JsonProperty("participants")
    private Collection<String> participants;

    @JsonProperty("is_public")
    private boolean isPublic;

    @JsonProperty("category")
    private RoomCategory category;

    @JsonProperty("event_start_time")
    private OffsetDateTime eventStartDateTime;

    @JsonProperty("event_end_time")
    private OffsetDateTime eventEndDateTime;
}
