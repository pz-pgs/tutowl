package pz.pgs.streamapp.service;

import org.springframework.http.ResponseEntity;
import pz.pgs.streamapp.controller.dto.LeaveSessionRequest;
import pz.pgs.streamapp.controller.dto.SessionRequest;

public interface OpenViduService {
    ResponseEntity<?> getToken(SessionRequest sessionRequest);

    ResponseEntity<?> removeUser(LeaveSessionRequest leaveSessionRequest);
}
