package pz.pgs.streamapp.service;

import pz.pgs.streamapp.controller.dto.NotificationItem;
import pz.pgs.streamapp.entity.notification.Notification;
import pz.pgs.streamapp.entity.notification.NotificationType;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface NotificationsService {
    Flux<NotificationItem> getNotificationsList();

    Notification sendNotification(String author, NotificationType notificationType);

    Mono<Boolean> checkIfAnyNewNotifications();

}
