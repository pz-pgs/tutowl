package pz.pgs.streamapp.service.implementation;

import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pz.pgs.streamapp.controller.dto.SearchCategoryWithResults;
import pz.pgs.streamapp.controller.dto.SearchResult;
import pz.pgs.streamapp.entity.SearchCategory;
import pz.pgs.streamapp.entity.room.RoomCategory;
import pz.pgs.streamapp.mapper.RoomMapper;
import pz.pgs.streamapp.mapper.UserMapper;
import pz.pgs.streamapp.repository.RoomRepository;
import pz.pgs.streamapp.repository.RoomSort;
import pz.pgs.streamapp.service.SearchService;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import static pz.pgs.streamapp.security.SecurityUtils.getCurrentUsername;

@Service
@RequiredArgsConstructor
public class SearchServiceImpl implements SearchService {

    private final RoomRepository roomRepository;
    private final RoomMapper roomMapper;
    private final UserMapper userMapper;
    private final UsersResource resource;

    @Override
    public Flux<SearchCategoryWithResults> searchWithGivenQuery(String searchQuery) {
        return Flux.fromArray(SearchCategory.values())
                .map(searchCategory ->
                        new SearchCategoryWithResults(
                                searchCategory.getId(),
                                searchCategory.getName(),
                                getResultsCount(searchCategory, searchQuery),
                                getResults(searchCategory, searchQuery)
                        )
                );
    }

    private Long getResultsCount(SearchCategory searchCategory, String searchQuery) {
        switch (searchCategory){
            case CATEGORIES:
                return Long.valueOf(Arrays.stream(RoomCategory.values())
                        .filter(roomCategory -> roomCategory.getName().toLowerCase().contains(searchQuery.toLowerCase()))
                        .collect(Collectors.toList()).size());
            case PEOPLE:
                return Long.valueOf(resource.search(searchQuery, 0, 15)
                        .stream().filter(user -> !user.getUsername().equals(getCurrentUsername()))
                        .map(userMapper::userRepresentationToSearchResult).collect(Collectors.toList()).size());
            case ROOMS:
                return roomRepository.
                        countAllByIdNotNullAndNameContainingIgnoreCaseOrDescriptionContainingIgnoreCaseOrCategoryIsContainingIgnoreCase(
                                searchQuery, searchQuery, searchQuery).blockOptional().orElse(0L);
            default:
                return 0L;
        }
    }

    private Collection<SearchResult> getResults(SearchCategory searchCategory, String searchQuery) {
        switch (searchCategory){
            case CATEGORIES:
                return Arrays.stream(RoomCategory.values())
                        .filter(roomCategory -> roomCategory.getName().toLowerCase().contains(searchQuery.toLowerCase()))
                        .map(roomMapper::roomRoomCategoryToSearchResult)
                        .collect(Collectors.toList());
            case PEOPLE:
                return resource.search(searchQuery, 0, 15)
                        .stream().filter(user -> !user.getUsername().equals(getCurrentUsername()))
                        .map(userMapper::userRepresentationToSearchResult).collect(Collectors.toList());
            case ROOMS:
                Sort sort = Sort.by(RoomSort.START_DATE.getVariableNameToSortBy()).ascending();
                PageRequest pageRequest = PageRequest.of(0, 10, sort);

                return roomRepository
                        .findAllByIdNotNullAndNameContainingIgnoreCaseOrDescriptionContainingIgnoreCaseOrCategoryIsContainingIgnoreCase(
                                pageRequest, searchQuery, searchQuery, searchQuery)
                        .map(roomMapper::roomToSearchResult)
                        .collectList()
                        .blockOptional()
                        .orElse(new ArrayList<>());
            default:
                return new ArrayList<>();
        }
    }
}
