package pz.pgs.streamapp.controller.error.exception;

import org.springframework.http.HttpStatus;

public class RoomAccessDeniedException extends DomainException {

    public RoomAccessDeniedException(){
        super(HttpStatus.FORBIDDEN, "Wrong password.");
    }
}
