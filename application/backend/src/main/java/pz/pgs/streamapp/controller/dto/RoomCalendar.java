package pz.pgs.streamapp.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import pz.pgs.streamapp.entity.room.RoomCategory;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;

@Data
public class RoomCalendar {
    @JsonProperty("id")
    private String id;

    @JsonProperty("title")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("owner")
    private String ownerUsername;

    @JsonProperty("start")
    private OffsetDateTime eventStartDateTime;

    @JsonProperty("category")
    private RoomCategory category;

    @JsonProperty("end")
    private OffsetDateTime eventEndDateTime;

    @JsonProperty("participants")
    private Collection<String> participants;

    @JsonProperty("backgroundColor")
    private String backgroundColor = returnRandomColor();

    @JsonProperty("textColor")
    private String textColor = returnTextColor();

    private String returnTextColor() {
        return "#ffffff";
    }

    private String returnRandomColor() {
        return "rgba("
                + returnRandomInt(35, 55) + ","
                + returnRandomInt(60, 80) + ","
                + returnRandomInt(90, 110) + ")";
    }

    private int returnRandomInt(int start, int end) {
        return ThreadLocalRandom.current().nextInt(start, end + 1);
    }
}
