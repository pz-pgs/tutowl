package pz.pgs.streamapp.entity.room;

import javax.validation.constraints.NotNull;

public enum RoomCategory {
    CHEMISTRY(1),
    GEOGRAPHY(2),
    MATH(3),
    PHYSICS(4);

    private final int id;
    private final String name;

    RoomCategory(int id){
        this.id = id;
        this.name = this.name();
    }

    @NotNull
    public int getId() {
        return id;
    }

    @NotNull
    public String getName(){
        return name;
    }
}
