package pz.pgs.streamapp.service;

import pz.pgs.streamapp.controller.dto.ChatMessageRequest;
import pz.pgs.streamapp.controller.dto.ChatMessageItem;
import pz.pgs.streamapp.entity.chat.ChatMessage;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ChatService {
    Mono<ChatMessage> sendPrivateMessage(ChatMessageRequest chatMessageRequest);
    Flux<ChatMessage> getChatHistoryWithUser(String username);
    List<ChatMessageItem> getMessagesList();
    Mono<Integer> countUnreadMessages();
    Flux<ChatMessage> markMessagesWithUserAsRead(String username);
}
