package pz.pgs.streamapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pz.pgs.streamapp.controller.dto.NotificationItem;
import pz.pgs.streamapp.service.NotificationsService;
import reactor.core.publisher.Flux;
import pz.pgs.streamapp.utils.Logger;
import reactor.core.publisher.Mono;

import static pz.pgs.streamapp.security.SecurityUtils.getCurrentUsername;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/notifications")
public class NotificationsController {

    private static final Logger LOGGER = new Logger();

    private final NotificationsService notificationsService;

    @GetMapping(value="/", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<NotificationItem> getNotificationsList() {
        LOGGER.info("Getting notifications list for user {}.", getCurrentUsername());

        return notificationsService.getNotificationsList();
    }

    @GetMapping(value="/new")
    public Mono<Boolean> checkIfAnyNewNotifications() {
        LOGGER.info("Checking if any new notifications for user {}.", getCurrentUsername());

        return notificationsService.checkIfAnyNewNotifications();
    }
}
