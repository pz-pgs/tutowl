package pz.pgs.streamapp.entity.room;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.lang.Nullable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashSet;

import static pz.pgs.streamapp.entity.ValidationConstants.MAX_ROOM_NAME_LENGTH;
import static pz.pgs.streamapp.entity.ValidationConstants.MIN_ROOM_NAME_LENGTH;
import static pz.pgs.streamapp.entity.ValidationConstants.PASSWORD_HASH_LENGTH;

@Document("rooms")
@TypeAlias("Room")
@Data
@NoArgsConstructor
public class Room {

    @NotNull
    @NotBlank
    @Size(
            min = MIN_ROOM_NAME_LENGTH,
            max = MAX_ROOM_NAME_LENGTH
    )
    @Field("name")
    private String name;

    @Nullable
    @Field("desc")
    private String description;

    @NotNull
    @Field("pub")
    private boolean isPublic;

    @Nullable
    @Size(
            min = PASSWORD_HASH_LENGTH,
            max = PASSWORD_HASH_LENGTH
    )
    @Field("pass")
    private String password;

    @NotNull
    @Field("partc")
    private HashSet<String> participants;

    @NotNull
    @Field("own")
    private String ownerUsername;

    @NotNull
    @Field("cat")
    private RoomCategory category;

    @NotNull
    @Field("event_start_date")
    private Instant eventStartDateTime;

    @NotNull
    @Field("event_end_date")
    private Instant eventEndDateTime;

    private String id;

    @CreatedDate
    @Field("cr_at")
    private Instant createdAt;

    public void addParticipant(String username) {
        if(participants == null)
            participants = new HashSet<>();

        participants.add(username);
    }

    public void removeParticipant(String username){
        if(participants != null)
            participants.remove(username);
    }

    public boolean isOwnedBy(String user) {
        return this.ownerUsername.equalsIgnoreCase(user);
    }

    public boolean isFinished() {
        return Instant.now().isAfter(eventEndDateTime);
    }
}
