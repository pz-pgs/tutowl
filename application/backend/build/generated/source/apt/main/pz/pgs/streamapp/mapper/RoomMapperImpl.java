package pz.pgs.streamapp.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import javax.annotation.processing.Generated;
import org.mapstruct.factory.Mappers;
import pz.pgs.streamapp.controller.dto.CreateRoomRequest;
import pz.pgs.streamapp.controller.dto.RoomBrief;
import pz.pgs.streamapp.controller.dto.RoomCalendar;
import pz.pgs.streamapp.controller.dto.RoomPreview;
import pz.pgs.streamapp.entity.room.Room;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2020-05-27T22:37:06+0200",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.7 (Ubuntu)"
)
public class RoomMapperImpl implements RoomMapper {

    private final TimeMappings timeMappings = Mappers.getMapper( TimeMappings.class );

    @Override
    public Room createRequestToRoom(CreateRoomRequest request) {
        if ( request == null ) {
            return null;
        }

        Room room = new Room();

        room.setName( request.getName() );
        room.setDescription( request.getDescription() );
        room.setPublic( request.isPublic() );
        room.setPassword( request.getPassword() );
        room.setCategory( request.getCategory() );
        room.setEventStartDateTime( timeMappings.offsetDateTimeToInstant( request.getEventStartDateTime() ) );
        room.setEventEndDateTime( timeMappings.offsetDateTimeToInstant( request.getEventEndDateTime() ) );

        return room;
    }

    @Override
    public RoomPreview roomToPreview(Room room) {
        if ( room == null ) {
            return null;
        }

        RoomPreview roomPreview = new RoomPreview();

        roomPreview.setId( room.getId() );
        roomPreview.setName( room.getName() );
        roomPreview.setDescription( room.getDescription() );
        roomPreview.setOwnerUsername( room.getOwnerUsername() );
        HashSet<String> hashSet = room.getParticipants();
        if ( hashSet != null ) {
            roomPreview.setParticipants( new ArrayList<String>( hashSet ) );
        }
        roomPreview.setPublic( room.isPublic() );
        roomPreview.setCategory( room.getCategory() );
        roomPreview.setEventStartDateTime( timeMappings.instantToOffsetDateTime( room.getEventStartDateTime() ) );
        roomPreview.setEventEndDateTime( timeMappings.instantToOffsetDateTime( room.getEventEndDateTime() ) );

        return roomPreview;
    }

    @Override
    public RoomBrief roomToBrief(Room room) {
        if ( room == null ) {
            return null;
        }

        RoomBrief roomBrief = new RoomBrief();

        roomBrief.setId( room.getId() );
        roomBrief.setName( room.getName() );
        roomBrief.setDescription( room.getDescription() );
        roomBrief.setCategory( room.getCategory() );
        roomBrief.setEventStartDateTime( timeMappings.instantToOffsetDateTime( room.getEventStartDateTime() ) );
        roomBrief.setEventEndDateTime( timeMappings.instantToOffsetDateTime( room.getEventEndDateTime() ) );

        addIsFinished( room, roomBrief );

        return roomBrief;
    }

    @Override
    public RoomCalendar roomToCalendar(Room room) {
        if ( room == null ) {
            return null;
        }

        RoomCalendar roomCalendar = new RoomCalendar();

        roomCalendar.setId( room.getId() );
        roomCalendar.setName( room.getName() );
        roomCalendar.setDescription( room.getDescription() );
        roomCalendar.setOwnerUsername( room.getOwnerUsername() );
        roomCalendar.setEventStartDateTime( timeMappings.instantToOffsetDateTime( room.getEventStartDateTime() ) );
        roomCalendar.setCategory( room.getCategory() );
        roomCalendar.setEventEndDateTime( timeMappings.instantToOffsetDateTime( room.getEventEndDateTime() ) );
        HashSet<String> hashSet = room.getParticipants();
        if ( hashSet != null ) {
            roomCalendar.setParticipants( new ArrayList<String>( hashSet ) );
        }

        return roomCalendar;
    }
}
